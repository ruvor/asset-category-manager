;(function () {

angular
    .module("assetCategoryManager")
    .directive("categoriesSelector", categoriesSelectorDirectiveFactory);

categoriesSelectorDirectiveFactory.$inject = [
    "$rootScope",
    "$http",
    "filterFilter",
    "$q",
    "configuration",
    "commonUtils",
    "$rootElement",
];
function categoriesSelectorDirectiveFactory($rootScope, $http, filterFilter, $q,
        configuration, commonUtils, $rootElement) {

    function getNewCategoryIds(newCategories) {
        return newCategories.map(function (category) {
            return category.id;
        });
    }

    var newCategoryIds; // ИДы категорий, которые намечено добавить
    function markCategoriesSelectedRecursive(selectedCategoryIds,
            doomedCategoryIds, newCategories, categories, isInDeep) {
        if (!isInDeep) {
            // Извлекаются только на первом уровне рекурсии
            newCategoryIds = getNewCategoryIds(newCategories);
        }
        for (var i = 0; categories && i < categories.length; i++) {
            var category = categories[i];
            if (selectedCategoryIds.indexOf(category.id) >= 0 &&
                    doomedCategoryIds.indexOf(category.id) < 0 ||
                    newCategoryIds.indexOf(category.id) >= 0) {
                category.selected = true;
            }
            else {
                category.selected = false;
            }
            if (category.categories) {
                markCategoriesSelectedRecursive(selectedCategoryIds,
                        doomedCategoryIds, newCategories, category.categories, true);
            }
        }
    }

    // Переводит текстовые ключи для некоторых директив
    function translateDirectives(scope) {
        scope.translations = {
            placeholders: {
                search: configuration.translations["placeholders.search"],
            },
        };
    }

    function search(scope, selectedCategoryIds, doomedCategoryIds, newCategories,
            notHidden, str) {
        // Если числится выполняемый запрос, то отменить его,
        // запросить у сервера список категорий с заданием отменятеля
        scope.isLoading = true;
        if (scope.cancellationToken) {
            scope.cancellationToken.resolve();
        }
        scope.cancellationToken = $q.defer();
        $http.get(configuration.serviceUrlPrefix + "/category/search", {
            params: {
                keyword: str,
                languageId : configuration.currentLang
            },
            withCredentials: true,
            timeout: scope.cancellationToken.promise,
        }).then(function successCallback(response) {
            var categoriesList = response.data;
            categoriesList = filterFilter(categoriesList, notHidden);
            markCategoriesSelectedRecursive(selectedCategoryIds,
                    doomedCategoryIds, newCategories, categoriesList);
            scope.categoriesList = categoriesList;
        }, function errorCallback(response) {
            // В виду циклического характера работы по мере ввода (с планомерными отменами и повторными
            // запросами и возобновлениями cancellationToken) невозможна обработка ошибок по аналогии
            // с прочими в приложении, но в данном месте эта обработка не критична
        }).finally(function () {
            scope.cancellationToken = null;
            scope.isLoading = false;
        });
    }

    function handleVocabulariesRequest(scope, selectedCategoryIds,
            notHidden, response) {
        var vocabularies = response.data;
        commonUtils.prepareVocabularies(vocabularies);
        for (var i = 0; i < vocabularies.length; i++) {
            var vocabulary = vocabularies[i];
            vocabulary.categories = filterFilter(vocabulary.categories, notHidden)
            commonUtils.prepareCategoriesRecursive(vocabulary.categories);
            if (scope.isMultiple) {
                markCategoriesSelectedRecursive(selectedCategoryIds,
                        [], [], vocabulary.categories);
            }
            vocabulary.collapsed = false;
        }
        vocabularies = filterFilter(vocabularies, function (vocabulary, index, array) {
            return vocabulary.categories.length;
        });
        scope.vocabularies = vocabularies;
    }

    function handleCategoriesRequest(parentCategory, selectedCategoryIds, doomedCategoryIds,
            newCategories, notHidden, response) {
        var subCategories = response.data.categories;
        subCategories = filterFilter(subCategories, notHidden);
        if (subCategories.length) {
            commonUtils.prepareCategoriesRecursive(subCategories);
            markCategoriesSelectedRecursive(selectedCategoryIds,
                    doomedCategoryIds, newCategories, subCategories);
            parentCategory.categories = subCategories;
            parentCategory.collapsed = false;
        }
        else {
            // Вариант, при котором единственной дочерней категорией разворачиваемой категории
            // является скрываемая категория
            parentCategory.hasChildren = false;
        }
    }

    function onCategoryClick(scope, category, selectedCategoryIds,
            doomedCategoryIds, newCategories, $event) {
        if (scope.isMultiple) {
            if ($event.target.tagName === "SPAN") {
                category.selected = !category.selected;
            }
            var index;
            if (category.selected) {
                index = doomedCategoryIds.indexOf(category.id);
                if (index >= 0) {
                    doomedCategoryIds.splice(index, 1);
                }
                if (selectedCategoryIds.indexOf(category.id) < 0) {
                    newCategories.push(category);
                }
            }
            else {
                if (selectedCategoryIds.indexOf(category.id) >= 0) {
                    doomedCategoryIds.push(category.id);
                }
                index = getNewCategoryIds(newCategories).indexOf(category.id);
                if (index >= 0) {
                    newCategories.splice(index, 1);
                }
            }
        }
        else {
            if ($event.target.tagName === "SPAN") {
                $rootScope.radioSelectedId = category.id;
            }
            newCategories.splice(0, newCategories.length);
            if (selectedCategoryIds.indexOf(category.id) < 0) {
                newCategories.push(category);
            }
        }
    }

    function onSelectCategories(scope, ngModel, doomedCategoryIds, newCategories) {
        var newCategory;
        if (scope.isMultiple) {
            var oldSelectedCategories = scope.selectedCategories;
            var newSelectedCategories = [];
            if (oldSelectedCategories !== undefined) {
                // Удаление из списка выбранных категорий, с которых было снято выделение
                for (var i = 0; i < oldSelectedCategories.length; i++) {
                    var selectedCategory = oldSelectedCategories[i];
                    if (doomedCategoryIds.indexOf(selectedCategory.id) < 0) {
                        newSelectedCategories.push(selectedCategory);
                    }
                }
            }
            // Добавление в список выбранных категорий, которые были вновь выбраны в диалоге
            for (var j = 0; j < newCategories.length; j++) {
                newCategory = newCategories[j];
                newSelectedCategories.push({
                    id: newCategory.id,
                    name: newCategory.name,
                });
            }
            scope.selectedCategories = newSelectedCategories;
            ngModel.$setViewValue(scope.selectedCategories);
        }
        else {
            if (newCategories.length) {
                newCategory = newCategories[0];
                scope.selectedCategories = [{
                    id: newCategory.id,
                    name: newCategory.name,
                }];
                ngModel.$setViewValue(scope.selectedCategories[0]);
            }
        }
        if (ngModel.$pristine) {
            ngModel.$setDirty();
        }
        scope.vocabularies = undefined; // Для освобождения памяти
        scope.isLoading = true; // Чтобы не промелькнуло сообщение об ошибке
        scope.showDialog = false;
    }

    return {
        scope: {
            hiddenCategory: "<",
        },

        restrict: "AE",

        require: 'ngModel',

        templateUrl: configuration.baseUrl + "/categories-selector.directive.html",

        link: function (scope, element, attrs, ngModel) {
            var dialogElement = element.find(".acm-cs-dlg");
            var selectedCategoryIds; // ИДы категорий, которые являются выбранными
            var doomedCategoryIds; // ИДы категорий, которые намечено удалить
            var newCategories; // Категории, которые намечено добавить
            var globalGroupId, groupId, serviceUrlPrefix, globalScopeAvail, siteScopeAvail, currentLang;

            function notHidden(category, index, array) {
                if (scope.hiddenCategory === undefined) return true;
                return category.id !== scope.hiddenCategory.id;
            }

            scope.isMultiple = Object.keys(attrs).indexOf("multiple") >= 0;
            scope.cancellationToken = null;
            // Необходимо размещение свойства скопа для хранения поисковой строки в объектном контейнере из-за
            // наследования скопа в шаблоне диалога вследствие трансклюда в директиве диалога. Иначе изменение
            // свойства не будет продетектировано в верхнеуровневом скопе директивы categoriesSelector.
            scope.searchStrContainer = {
                searchStr: "",
            };

            scope.showSelect = function () {
                return !scope.selectedCategories || scope.selectedCategories.length === 0 || !scope.isMultiple;
            };

            scope.showAdd = function () {
                return scope.isMultiple && scope.selectedCategories && scope.selectedCategories.length > 0;
            };

            scope.openDialog = function () {
                scope.searchStrContainer.searchStr = "";
                doomedCategoryIds = [];
                newCategories = [];
                if (scope.selectedCategories && scope.selectedCategories.length) {
                    selectedCategoryIds = scope.selectedCategories.map(function (category) {
                        return category.id;
                    });
                    $rootScope.radioSelectedId = selectedCategoryIds[0];
                }
                else {
                    selectedCategoryIds = [];
                    $rootScope.radioSelectedId = undefined;
                }
                scope.isLoading = true;
                scope.showDialog = true;
                // Запросить на сервере первый уровень словарей
                scope.cancellationToken = $q.defer();
                var groupIds = [];
                if (globalScopeAvail) {
                    groupIds.push(globalGroupId);
                }
                if (siteScopeAvail) {
                    groupIds.push(groupId);
                }
                $http.get(serviceUrlPrefix + "/vocabulary/group-category-trees", {
                    params: {
                        groupId : groupIds,
                        languageId : currentLang,
                    },
                    withCredentials: true,
                    timeout: scope.cancellationToken.promise,
                }).then(function successCallback(response) {
                    handleVocabulariesRequest(scope, selectedCategoryIds, notHidden, response);
                }, function errorCallback(response) {
                    commonUtils.handleCancellableHttpError(response, scope.cancellationToken);
                }).finally(function () {
                    scope.isLoading = false;
                });
            };

            scope.isCategoryPending = commonUtils.isCategoryPending;

            // Разворачивает категорию
            scope.expandCategory = function (category) {
                if (scope.isCategoryPending(category)) {
                    // Нужно получить с сервера список подкатегорий
                    category.isLoading = true;
                    $http.get(serviceUrlPrefix + "/category/child-categories", {
                        params: {
                            categoryId : category.id,
                            languageId : currentLang,
                        },
                        withCredentials: true,
                    }).then(function successCallback(response) {
                        handleCategoriesRequest(category, selectedCategoryIds,
                                doomedCategoryIds, newCategories, notHidden, response);
                    }).finally(function () {
                        category.isLoading = false;
                    });
                }
                else {
                    category.collapsed = false;
                }
            };

            scope.clickCategory = function (category, $event) {
                onCategoryClick(scope, category, selectedCategoryIds,
                        doomedCategoryIds, newCategories, $event);
            };

            scope.selectCategories = function () {
                // В выбиратель (scope.selectedCategories) добавляются просто категории,
                // без информации о пространственной структуре, только id и name
                onSelectCategories(scope, ngModel, doomedCategoryIds, newCategories);
            };

            scope.unselect = function (index) {
                if (scope.isMultiple) {
                    scope.selectedCategories.splice(index, 1);
                    ngModel.$validate();
                }
                else {
                    scope.selectedCategories = [];
                    ngModel.$setViewValue(undefined);
                }
                if (ngModel.$pristine) {
                    ngModel.$setDirty();
                }
            };

            // Применение конфигурации
            globalGroupId = configuration.globalGroupId;
            groupId = configuration.groupId;
            serviceUrlPrefix = configuration.serviceUrlPrefix;
            globalScopeAvail = configuration.globalScopeAvail;
            siteScopeAvail = configuration.siteScopeAvail;
            currentLang = configuration.currentLang;
            scope.baseUrl = configuration.baseUrl;

            ngModel.$isEmpty = function (value) {
                return !value || scope.isMultiple && !value.length;
            };

            ngModel.$render = function () {
                if (scope.isMultiple) {
                    scope.selectedCategories = ngModel.$viewValue;
                }
                else {
                    if (ngModel.$viewValue) {
                        scope.selectedCategories = [ngModel.$viewValue];
                    }
                    else {
                        scope.selectedCategories = [];
                    }
                }
            };

            scope.$watch("searchStrContainer.searchStr", function (newValue, oldValue) {
                if (newValue === oldValue) return;
                if (newValue) {
                    search(scope, selectedCategoryIds, doomedCategoryIds,
                            newCategories, notHidden, newValue)
                }
                else {
                    scope.categoriesList = undefined;
                    for (var i = 0; i < scope.vocabularies.length; i++) {
                        var vocabulary = scope.vocabularies[i];
                        markCategoriesSelectedRecursive(selectedCategoryIds,
                                doomedCategoryIds, newCategories, vocabulary.categories);
                    }
                }
            });

            translateDirectives(scope);

            // Поскольку приходится вызывать модальный диалог выбирателя изнутри другого модального диалога,
            // а некоторые особенности стилизации диалогов исключают размещение в DOMе элементов диалогов
            // внутри друг друга, следует вынести элемент диалога выбирателя наружу
            dialogElement.appendTo($rootElement);
            scope.$on("$destroy", function () {
                dialogElement.remove();
            });
        },
    };
}

})();
