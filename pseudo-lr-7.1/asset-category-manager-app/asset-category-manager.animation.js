;(function () {

angular
    .module("assetCategoryManager")
    .animation(".vocabulary-contents", slideAnimationFactory)
    .animation(".asset-category-manager-tree", slideAnimationFactory);

function slideAnimationFactory() {
    return {
        addClass: function(element, className, doneFn) {
            $(element).slideUp(100, doneFn);
        },
        removeClass: function(element, className, doneFn) {
            var $element = $(element);
            $element.hide(); // Без этого не выполняется начальная анимация развёртывания
            $element.slideDown(100, doneFn);
        }
    };
}

})();
