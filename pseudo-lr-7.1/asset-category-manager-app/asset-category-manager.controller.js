;(function () {

angular
    .module("assetCategoryManager")
    .controller("AssetCategoryManagerController", AssetCategoryManagerController);

AssetCategoryManagerController.$inject = [
    "$scope",
    "$http",
    "$timeout",
    "filterFilter",
    "$q",
    "configuration",
    "commonUtils",
    "showSuccess",
];
function AssetCategoryManagerController($scope, $http, $timeout, filterFilter, $q,
        configuration, commonUtils, showSuccess) {

    var exclusiveActionInProgress = false;
    var globalGroupId, groupId, plId, portletId, serviceUrlPrefix, currentLang;
    var messages;
    var DRAG_EXPAND_TIMEOUT = 500, dragExpandTimerPromise = null;
    var self = this; // Для доступа к данным контроллера в функциях без соответствующего контекста

    // Обработчик события начала бросания категории
    function onCategoryBeforeDrop(event) {
        var sourceParent = event.source.nodesScope.category ?
                event.source.nodesScope.category :
                event.source.nodesScope.vocabulary;
        var destinationCategory = event.dest.nodesScope.category;
        var destinationVocabulary = event.dest.nodesScope.vocabulary;
        var destinationParent = destinationCategory ?
                destinationCategory :
                destinationVocabulary;
        if (sourceParent === destinationParent && event.dest.index === event.source.index) {
            // Перетаскивания не произошло
            return false;
        }
        var droppedCategory = event.source.nodeScope.category;
        var resultIdsSequence = [];
        for (var i = 0; i < destinationParent.categories.length; i++) {
            var category = destinationParent.categories[i];
            if (category === droppedCategory) continue;
            resultIdsSequence.push(category.id);
        }
        resultIdsSequence.splice(event.dest.index, 0, droppedCategory.id);
        // Передать на сервер указание о перебазировании категории droppedCategory
        sourceParent.isLoading = true;
        destinationParent.isLoading = true;
        return $http.get(serviceUrlPrefix + "/category/action", {
            params: {
                actionType: "drop",
                droppedId: droppedCategory.id,
                droppedToCategoryId: destinationCategory ? destinationCategory.id : 0,
                droppedToVocabularyId: destinationVocabulary.id,
                categoriesIds: resultIdsSequence.toString(),
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            // A handler isn't needed here
        }, function errorCallback(response) {
            commonUtils.handleHttpError(response);
            sourceParent.isLoading = false;
            destinationParent.isLoading = false;
            return $q.reject();
        });
    }

    // Обработчик события окончания бросания категории
    function onCategoryDropped(event) {
        var sourceCategory = event.source.nodesScope.category;
        if (sourceCategory) {
            commonUtils.handleCategoryAfterSubDelete(sourceCategory);
            sourceCategory.isLoading = false;
        }
        else {
            var sourceVocabulary = event.source.nodesScope.vocabulary;
            commonUtils.handleVocabularyAfterSubDelete(sourceVocabulary);
            sourceVocabulary.isLoading = false;
        }
        var destinationCategory = event.dest.nodesScope.category;
        if (destinationCategory) {
            commonUtils.reloadCategory(self, destinationCategory);
        }
        else {
            var destinationVocabulary = event.dest.nodesScope.vocabulary;
            commonUtils.reloadVocabulary(self, destinationVocabulary);
        }
    }

    // Подготавливает диалог словаря к открытию
    function prepareVocabularyDialog(vocabulary, vocabularyData) {
        self.vocabularyDialog = {
            vocabulary: vocabulary,
            needApproval: false,
            isLoading: false,
        };
        if (vocabularyData) {
            self.vocabularyDialog.vocabularyData = {
                name: vocabularyData.name,
                description: vocabularyData.description,
                isPublic: vocabularyData.settings.public,
            };
        }
        else {
            self.vocabularyDialog.vocabularyData = {
                name: {},
                description: {},
                isGlobal: !self.siteScopeAvail,
                isPublic: false,
            };
        }
        self.vocabularyForm.$setPristine();
        self.currentInputsLang = self.languages[0];
    }

    // Подготавливает диалог категории к открытию
    function prepareCategoryDialog(category, rawCategoryData, rawParentCategoryData) {
        self.categoryDialog = {
            category: category,
            categoryData: rawCategoryData ? {
                name: rawCategoryData.name,
                description: rawCategoryData.description,
                responsibles: rawCategoryData.responsible,
                inheritedResponsibles: rawCategoryData.inheritedResponsible,
            } : {
                name: {},
                description: {},
            },
            isLoading: false,
        };
        if (rawParentCategoryData) {
            var parentResponsibles = rawParentCategoryData.responsible
                    .concat(rawParentCategoryData.inheritedResponsible)
                    .reduce(function (acc, responsible) {
                        if (acc.every(function (r) {
                            return r.id !== responsible.id;
                        })) {
                            acc.push(responsible);
                        }
                        return acc;
                    }, []);
            self.categoryDialog.categoryData.inheritedResponsibles = parentResponsibles;
        }
        self.categoryForm.$setPristine();
        self.currentInputsLang = self.languages[0];
    }

    // Формирует время архивации для диалога категории
    function makeCategoryExpirationTime() {
        var d = self.categoryDialog.expirationDate;
        var t = self.categoryDialog.expirationTime;
        return new Date(d.getFullYear(), d.getMonth(), d.getDate(), t.getHours(), t.getMinutes());
    }

    // Разворачивает/сворачивает словарь
    this.toggleVocabulary = function (vocabulary) {
        if (vocabulary.isLoading) return;
        if (vocabulary.collapsed) {
            if (commonUtils.isVocabularyPending(vocabulary)) {
                commonUtils.reloadVocabulary(self, vocabulary);
            }
            else {
                vocabulary.collapsed = false;
                commonUtils.saveState(self.vocabularies);
            }
        }
        else {
            vocabulary.collapsed = true;
            commonUtils.saveState(self.vocabularies);
        }
    };

    // Обрабатывает наведение курсора на словарь
    this.onVocabularyEnter = function (event, vocabulary) {
        if (event.buttons === 1 && vocabulary.collapsed) {
            dragExpandTimerPromise = $timeout(function () {
                self.toggleVocabulary(vocabulary);
                dragExpandTimerPromise = null;
            }, DRAG_EXPAND_TIMEOUT);
        }
    };

    // Обрабатывает уход курсора со словаря
    this.onVocabularyLeave = function () {
        if (dragExpandTimerPromise) {
            $timeout.cancel(dragExpandTimerPromise);
            dragExpandTimerPromise = null;
        }
    };

    // Проверяет возможность разворачивания всего дерева категорий словаря
    this.testAllCatsExpandability = function (vocabulary) {
        return vocabulary.categories.some(function (category) {
            return category.hasChildren || category.categories.length;
        });
    };

    // Разворачивает всё дерево категорий словаря
    this.expandAllCategories = function (vocabulary) {
        if (commonUtils.areTherePendingCategoriesRecursive(vocabulary.categories)) {
            // Если в этом дереве есть "отложенные" категории, то нужно запросить
            // по vocabularyId'у все категории словаря
            vocabulary.isLoading = true;
            $http.get(serviceUrlPrefix + "/vocabulary/category-tree", {
                params: {
                    vocabularyId: vocabulary.id,
                    expanded: true,
                    languageId : currentLang,
                },
                withCredentials: true,
            }).then(function successCallback(response) {
                var categories = response.data.categories;
                commonUtils.prepareCategoriesRecursive(categories);
                commonUtils.expandCategoriesRecursive(categories);
                vocabulary.categories = categories;
                commonUtils.saveState(self.vocabularies);
            }, function errorCallback(response) {
                commonUtils.handleHttpError(response);
            }).finally(function () {
                vocabulary.isLoading = false;
            });
        }
        else {
            commonUtils.expandCategoriesRecursive(vocabulary.categories);
            commonUtils.saveState(self.vocabularies);
        }
    };

    // Сворачивает всё дерево категорий словаря
    this.collapseAllCategories = function (vocabulary) {
        commonUtils.collapseCategoriesRecursive(vocabulary.categories);
        commonUtils.saveState(self.vocabularies);
    };

    // Разворачивает поддерево категории
    this.expandCategoryChildren = function (vocabulary, category) {
        if (category.categories.length === 0 || commonUtils.areTherePendingCategoriesRecursive(category.categories)) {
            // Если в этом поддереве есть "отложенные" категории, то нужно запросить по соответствующему vocabularyId'у
            // все категории словаря, поскольку в настоящий момент нет сервиса для запроса полной информации по
            // подкатегориям для категории
            category.isLoading = true;
            $http.get(serviceUrlPrefix + "/vocabulary/category-tree", {
                params: {
                    vocabularyId: vocabulary.id,
                    expanded: true,
                    languageId : currentLang,
                },
                withCredentials: true,
            }).then(function successCallback(response) {
                var categories = commonUtils.findCategoryById(response.data, category.id).categories;
                commonUtils.prepareCategoriesRecursive(categories);
                commonUtils.expandCategoriesRecursive(categories);
                category.categories = categories;
                category.collapsed = false;
                commonUtils.saveState(self.vocabularies);
            }, function errorCallback(response) {
                commonUtils.handleHttpError(response);
            }).finally(function () {
                category.isLoading = false;
            });
        }
        else {
            commonUtils.expandCategoriesRecursive(category.categories);
            category.collapsed = false;
            commonUtils.saveState(self.vocabularies);
        }
    };

    this.collapseCategoryChildren = function (category) {
        commonUtils.collapseCategoriesRecursive(category.categories);
        category.collapsed = true;
        commonUtils.saveState(self.vocabularies);
    };

    // Разворачивает категорию
    this.expandCategory = function (category) {
        if (self.isCategoryPending(category)) {
            // Нужно получить с сервера список подкатегорий
            commonUtils.reloadCategory(this, category);
        }
        else {
            // Категория уже полностью загружена
            category.collapsed = false;
            commonUtils.saveState(self.vocabularies);
        }
    };

    // Сворачивает категорию
    this.collapseCategory = function (category) {
        category.collapsed = true;
        commonUtils.saveState(self.vocabularies);
    };

    // Открывает диалог создания словаря
    this.openCreateVocabularyDialog = function () {
        prepareVocabularyDialog();
        this.showAddModVocabularyDialog = true;
    };

    // Открывает диалог изменения словаря
    this.openModifyVocabularyDialog = function (vocabulary) {
        if (exclusiveActionInProgress) return;
        // Получить с сервера подробности о словаре
        vocabulary.isLoading = exclusiveActionInProgress = true;
        $http.get(serviceUrlPrefix + "/vocabulary", {
            params: {
                vocabularyId: vocabulary.id,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            prepareVocabularyDialog(vocabulary, response.data);
            self.showAddModVocabularyDialog = true;
        }, function errorCallback(response) {
            commonUtils.handleHttpError(response);
        }).finally(function () {
            vocabulary.isLoading = exclusiveActionInProgress = false;
        });
    };

    // Отправляет на сервер создание/изменение словаря
    this.saveVocabulary = function () {
        this.vocabularyForm.$$controls.forEach(function (control) {
            control.$setDirty();
        });
        if (this.vocabularyForm.$invalid) return;
        if (!this.vocabularyDialog.needApproval && this.vocabularyDialog.vocabularyData.isPublic) {
            this.vocabularyDialog.needApproval = true;
            return;
        }
        // Отправка данных словаря на сервер
        this.cancellationToken = $q.defer();
        this.vocabularyDialog.isLoading = true;
        var data = {
            publicFlag: this.vocabularyDialog.vocabularyData.isPublic,
            title: this.vocabularyDialog.vocabularyData.name,
            description: this.vocabularyDialog.vocabularyData.description,
            portletId: portletId,
            plid: plId,
        };
        if (!this.vocabularyDialog.vocabulary) {
            data.groupId = this.vocabularyDialog.vocabularyData.isGlobal ? globalGroupId : groupId;
        }
        else {
            data.vocabularyId = this.vocabularyDialog.vocabulary.id;
        }
        $http.post(serviceUrlPrefix + "/vocabulary/actionForm", data, {
            params: {
                actionType: "save",
            },
            withCredentials: true,
            timeout: this.cancellationToken.promise,
        }).then(function successCallback(response) {
            var newVocabularyName = self.vocabularyDialog.vocabularyData.name[currentLang];
            if (!self.vocabularyDialog.vocabulary) {
                var newVocabulary = {
                    id: response.data.vocabularyId,
                    name: newVocabularyName,
                    hasChildren: false,
                    categories: [],
                    collapsed : false,
                };
                self.vocabularies.push(newVocabulary);
                showSuccess(messages.vocabularyCreated);
            }
            else {
                self.vocabularyDialog.vocabulary.name = newVocabularyName;
                showSuccess(messages.vocabularyModified);
            }
            self.showAddModVocabularyDialog = false;
        }, function errorCallback(response) {
            self.vocabularyDialog.isLoading = false;
            commonUtils.handleCancellableHttpError(response, self.cancellationToken);
        });
    };

    // Открывает диалог удаления словаря
    this.openDeleteVocabularyDialog = function (vocabulary) {
        if (exclusiveActionInProgress) return;
        // Получить с сервера количество категорий
        vocabulary.isLoading = exclusiveActionInProgress = true;
        $http.get(serviceUrlPrefix + "/vocabulary/action", {
            params: {
                actionType: "vocabularyRelations",
                vocabularyId: vocabulary.id,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            var categoriesCount = response.data.categoryCount;
            self.vocabularyDialog = {
                vocabulary: vocabulary,
                vocabularyEscapedName: Liferay.Util.escape(vocabulary.name),
                isLoading: false,
            };
            if (categoriesCount > 0) {
                self.vocabularyDialog.categoriesCount = categoriesCount;
                self.transferVocabularies = filterFilter(self.vocabularies, function (voc) {
                    return voc !== vocabulary;
                });
                self.vocabularyDialog.moveCategories = !!self.transferVocabularies.length;
                $scope.$applyAsync(function () {
                    // Требуется выполнение в отдельном digest-цикле для корректной проверки словарей
                    // на дублирование даже при том, что словарь, запланированный для переноса, не изменился
                    // с прошлого открытия диалога (благодаря отдельному циклу ангуляр продетектирует изменение
                    // свойства vocabularyToMoveTo и, соответственно, будет произведена проверка на дублирование)
                    self.vocabularyDialog.vocabularyToMoveTo = self.transferVocabularies[0];
                });
                self.showDelVocabularyDialog = true;
            }
            else {
                self.showDelSimpleVocabularyDialog = true;
            }
        }, function errorCallback(response) {
            commonUtils.handleHttpError(response);
        }).finally(function () {
            vocabulary.isLoading = exclusiveActionInProgress = false;
        });
    };

    // Отправляет на сервер удаление словаря
    this.deleteVocabulary = function (simple) {
        // Отправка на сервер
        this.cancellationToken = $q.defer();
        this.vocabularyDialog.isLoading = true;
        var params = {
            actionType: "deleteVocabulary",
            vocabularyId: this.vocabularyDialog.vocabulary.id,
            portletId: portletId,
            plid: plId,
        };
        if (simple) {
            params.actionType = "deleteVocabulary";
        }
        else {
            params.actionType = "deleteWithRelation";
            if (this.vocabularyDialog.vocabularyToMoveTo) {
                params.moveVocabularyId = this.vocabularyDialog.vocabularyToMoveTo.id;
            }
            params.categoryRelationAction = this.vocabularyDialog.moveCategories ? "move" : "delete";
        }
        $http.get(serviceUrlPrefix + "/vocabulary/action", {
            params: commonUtils.addThemeDisplay(params),
            withCredentials: true,
            timeout: this.cancellationToken.promise,
        }).then(function successCallback(response) {
            self.vocabularies.splice(self.vocabularies.indexOf(self.vocabularyDialog.vocabulary), 1);
            if (self.vocabularyDialog.moveCategories) {
                commonUtils.reloadVocabulary(self, self.vocabularyDialog.vocabularyToMoveTo);
            }
            commonUtils.saveState(self.vocabularies);
            self.showDelSimpleVocabularyDialog = false;
            self.showDelVocabularyDialog = false;
            showSuccess(messages.vocabularyDeleted);
        }, function errorCallback(response) {
            self.vocabularyDialog.isLoading = false;
            commonUtils.handleCancellableHttpError(response, self.cancellationToken);
        });
    };

    // Открывает диалог создания категории
    this.openCreateCategoryDialog = function (vocabulary, parentCategory) {
        if (!parentCategory) {
            // Создаётся категория верхнего уровня
            prepareCategoryDialog();
            this.categoryDialog.vocabulary = vocabulary;
            this.categoryDialog.parentCategory = parentCategory;
            this.showAddModCategoryDialog = true;
        } else {
            if (exclusiveActionInProgress) return;
            // Получить с сервера подробности о родительской категории
            parentCategory.isLoading = exclusiveActionInProgress = true;
            $http.get(serviceUrlPrefix + "/category", {
                params: {
                    categoryId: parentCategory.id,
                },
                withCredentials: true,
            }).then(function successCallback(response) {
                prepareCategoryDialog(null, null, response.data);
                self.categoryDialog.vocabulary = vocabulary;
                self.categoryDialog.parentCategory = parentCategory;
                self.showAddModCategoryDialog = true;
            }, function errorCallback(response) {
                commonUtils.handleHttpError(response);
            }).finally(function () {
                parentCategory.isLoading = exclusiveActionInProgress = false;
            });
        }
    };

    // Открывает диалог изменения категории
    this.openModifyCategoryDialog = function (category, vocabulary) {
        if (exclusiveActionInProgress) return;
        // Получить с сервера подробности о категории
        category.isLoading = exclusiveActionInProgress = true;
        $http.get(serviceUrlPrefix + "/category", {
            params: {
                categoryId: category.id,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            prepareCategoryDialog(category, response.data);
            self.categoryDialog.vocabulary = vocabulary;
            self.showAddModCategoryDialog = true;
        }, function errorCallback(response) {
            commonUtils.handleHttpError(response);
        }).finally(function () {
            category.isLoading = exclusiveActionInProgress = false;
        });
    };

    // Отправляет на сервер создание/изменение категории
    this.saveCategory = function () {
        this.categoryForm.$$controls.forEach(function (control) {
            control.$setDirty();
        });
        if (this.categoryForm.$invalid) return;
        // Отправка данных категории на сервер
        this.cancellationToken = $q.defer();
        this.categoryDialog.isLoading = true;
        var data = {
            vocabularyId: this.categoryDialog.vocabulary.id,
            title: this.categoryDialog.categoryData.name,
            description: this.categoryDialog.categoryData.description,
            responsibleId: this.categoryDialog.categoryData.responsibles ?
                    this.categoryDialog.categoryData.responsibles.map(function (r) {
                        return r.id;
                    }).join(',') : "",
        };
        if (!this.categoryDialog.category) {
            if (this.categoryDialog.parentCategory) {
                data.parentCategoryId = this.categoryDialog.parentCategory.id;
            }
        }
        else {
            data.categoryId = this.categoryDialog.category.id;
        }
        $http.post(serviceUrlPrefix + "/category/actionForm", data, {
            params: {
                actionType: "save",
            },
            withCredentials: true,
            timeout: this.cancellationToken.promise,
        }).then(function successCallback(response) {
            var newCategoryName = self.categoryDialog.categoryData.name[currentLang];
            if (!self.categoryDialog.category) {
                if ((self.categoryDialog.parentCategory && !self.isCategoryPending(self.categoryDialog.parentCategory)) ||
                        (!self.categoryDialog.parentCategory && !commonUtils.isVocabularyPending(self.categoryDialog.vocabulary))) {
                    // Если новая категория добавляется в родительскую, которая не является "отложенной",
                    // или она добавляется напрямую в словарь, который не является "отложенным"
                    var newCategory = {
                        id: response.data.categoryId,
                        name: newCategoryName,
                        hasChildren: false,
                        categories: [],
                        collapsed : true,
                    };
                    if (self.categoryDialog.parentCategory) {
                        self.categoryDialog.parentCategory.categories.push(newCategory);
                        self.expandCategory(self.categoryDialog.parentCategory);
                    }
                    else {
                        self.categoryDialog.vocabulary.categories.push(newCategory);
                        if (self.categoryDialog.vocabulary.collapsed) {
                            self.toggleVocabulary(self.categoryDialog.vocabulary);
                        }
                    }
                }
                else {
                    // Если новая категория добавляется в родительскую, которая является "отложенной",
                    // или напрямую в словарь, который является "отложенным"
                    if (self.categoryDialog.parentCategory) {
                        self.expandCategory(self.categoryDialog.parentCategory);
                    }
                    else {
                        // Можно безусловно применить toggleVocabulary, потому что "отложенный" словарь всегда свёрнут
                        self.toggleVocabulary(self.categoryDialog.vocabulary);
                    }
                }
                showSuccess(messages.categoryCreated);
            }
            else {
                self.categoryDialog.category.name = newCategoryName;
                showSuccess(messages.categoryModified);
            }
            self.showAddModCategoryDialog = false;
        }, function errorCallback(response) {
            self.categoryDialog.isLoading = false;
            commonUtils.handleCancellableHttpError(response, self.cancellationToken);
        });
    };

    // Открывает диалог удаления категории
    this.openDeleteCategoryDialog = function (category, vocabulary, parentCategory) {
        if (exclusiveActionInProgress) return;
        // Спросить у сервера количество дочерних категорий и привязанных документов
        category.isLoading = exclusiveActionInProgress = true;
        $http.get(serviceUrlPrefix + "/category/action", {
            params: {
                actionType: "relations",
                categoryId : category.id,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            var subCategoriesCount = response.data.categoryCount;
            var documentsCount = response.data.documentCount;
            self.categoryDialog = {};
            self.categoryDialog.category = category;
            self.categoryDialog.categoryEscapedName = Liferay.Util.escape(category.name);
            self.categoryDialog.vocabulary = vocabulary;
            self.categoryDialog.parentCategory = parentCategory;
            self.categoryDialog.isLoading = false;
            if (subCategoriesCount > 0 || documentsCount > 0) {
                self.categoryDialog.subCategoriesCount = subCategoriesCount;
                self.categoryDialog.documentsCount = documentsCount;
                self.categoryDialog.moveObjects = false;
                self.categoryDialog.step = 0;
                self.deleteCategoryForm.$setPristine();
                self.showDelCategoryDialog = true;
            }
            else {
                self.showDelSimpleCategoryDialog = true;
            }
        }, function errorCallback(response) {
            commonUtils.handleHttpError(response);
        }).finally(function () {
            category.isLoading = exclusiveActionInProgress = false;
        });
    };

    // Отправляет на сервер удаление категории простое
    this.deleteCategorySimple = function () {
        this.cancellationToken = $q.defer();
        self.categoryDialog.isLoading = true;
        $http.get(serviceUrlPrefix + "/category/action", {
            params: {
                actionType: "delete",
                categoryId : self.categoryDialog.category.id,
            },
            withCredentials: true,
            timeout: this.cancellationToken.promise,
        }).then(function successCallback(response) {
            commonUtils.handleSuccessDeleteCategoryRequest(self);
        }, function errorCallback(response) {
            self.categoryDialog.isLoading = false;
            commonUtils.handleCancellableHttpError(response, self.cancellationToken);
        });
    };

    // Отправляет на сервер удаление категории расширенное
    this.deleteCategory = function () {
        switch (this.categoryDialog.step) {
            case 0:
                // Выбор между Удалить и Перенести и удалить
                this.deleteCategoryForm.$$controls.forEach(function (control) {
                    control.$setDirty();
                });
                if (this.deleteCategoryForm.$invalid) return;
                // Отправка на сервер
                this.cancellationToken = $q.defer();
                this.categoryDialog.isLoading = true;
                if (this.categoryDialog.moveObjects) {
                    // Выбран перенос дочерних категорий и связанных документов и затем удаление категории,
                    // нужно передать на сервер ИД категории и что надо переносить объекты
                    $http.get(serviceUrlPrefix + "/category/action", {
                        params: {
                            actionType: "deleteWithRelation",
                            categoryId : this.categoryDialog.category.id,
                            moveCategoryId: this.categoryDialog.categoryToMoveTo.id,
                            relationAction: "move",
                            // documentsWithOtherLinksAction: // Согласно спецификации API в этом случае этот параметр не нужен
                        },
                        withCredentials: true,
                        timeout: this.cancellationToken.promise,
                    }).then(function successCallback(response) {
                        commonUtils.handleSuccessDeleteCategoryRequest(self, self.categoryDialog.categoryToMoveTo.id);
                    }, function errorCallback(response) {
                        self.categoryDialog.isLoading = false;
                        commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                    });
                }
                else {
                    // Выбрано удаление категории вместе с дочерними и связанными документами, нужно передать ИД
                    // категории и спросить, есть ли в ней документы, связанные с несколькими категориями, сколько
                    $http.get(serviceUrlPrefix + "/category/action", {
                        params: {
                            actionType: "documentsWithOtherLink",
                            categoryId : this.categoryDialog.category.id
                        },
                        withCredentials: true,
                        timeout: this.cancellationToken.promise,
                    }).then(function successCallback(response) {
                        var crossLinkedCount = response.data.documentsWithOtherLinks.length;
                        if (crossLinkedCount) {
                            // В удаляемой категории есть документы, связанные с несколькими категориями
                            self.categoryDialog.crossLinkedCount = crossLinkedCount;
                            self.categoryDialog.deleteCrossLinked = false;
                            self.categoryDialog.step = 1;
                            self.categoryDialog.isLoading = false;
                        }
                        else {
                            // В удаляемой категории нет документов, связанных с несколькими категориями
                            self.cancellationToken = $q.defer();
                            self.categoryDialog.isLoading = true;
                            $http.get(serviceUrlPrefix + "/category/action", {
                                params: commonUtils.addThemeDisplay({
                                    actionType: "deleteWithRelation",
                                    categoryId : self.categoryDialog.category.id,
                                    relationAction: "go",
                                }),
                                withCredentials: true,
                                timeout: self.cancellationToken.promise,
                            }).then(function successCallback(response) {
                                commonUtils.handleSuccessDeleteCategoryRequest(self);
                            }, function errorCallback(response) {
                                self.categoryDialog.isLoading = false;
                                commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                            });
                        }
                    }, function errorCallback(response) {
                        self.categoryDialog.isLoading = false;
                        commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                    });
                }
                break;
            case 1:
                // Выбор удаления или пропуска документов, связанных с несколькими категориями,
                // передать ИД категории и что надо удалять объекты, связанные с этой категорией
                this.cancellationToken = $q.defer();
                this.categoryDialog.isLoading = true;
                $http.get(serviceUrlPrefix + "/category/action", {
                    params: commonUtils.addThemeDisplay({
                        actionType: "deleteWithRelation",
                        categoryId : this.categoryDialog.category.id,
                        relationAction: "go",
                        documentsWithOtherLinksAction: self.categoryDialog.deleteCrossLinked ? "go" : "skip",
                    }),
                    withCredentials: true,
                    timeout: this.cancellationToken.promise,
                }).then(function successCallback(response) {
                    commonUtils.handleSuccessDeleteCategoryRequest(self);
                }, function errorCallback(response) {
                    self.categoryDialog.isLoading = false;
                    commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                });
                break;
        }
    };

    // Открывает диалог архивации категории
    this.openArchiveCategoryDialog = function (category) {
        if (exclusiveActionInProgress) return;
        // Получить с сервера количество подкатегорий и документов
        category.isLoading = exclusiveActionInProgress = true;
        $http.get(serviceUrlPrefix + "/category/action", {
            params: {
                actionType: "relations",
                categoryId : category.id,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            var subCategoriesCount = response.data.categoryCount;
            var documentsCount = response.data.documentCount;
            self.categoryDialog = {};
            self.categoryDialog.category = category;
            self.categoryDialog.categoryEscapedName = Liferay.Util.escape(category.name);
            self.categoryDialog.isLoading = false;
            if (subCategoriesCount > 0 || documentsCount > 0) {
                self.categoryDialog.subCategoriesCount = subCategoriesCount;
                self.categoryDialog.documentsCount = documentsCount;
                self.categoryDialog.moveObjects = false;
                self.categoryDialog.step = 0;
                self.archiveCategoryForm.$setPristine();
                self.showArchCategoryDialog = true;
            }
            else {
                self.showArchSimpleCategoryDialog = true;
            }
        }, function errorCallback(response) {
            commonUtils.handleHttpError(response);
        }).finally(function () {
            category.isLoading = exclusiveActionInProgress = false;
        });
    };

    // Отправляет на сервер архивацию категории простую
    this.archiveCategorySimple = function () {
        this.cancellationToken = $q.defer();
        this.categoryDialog.isLoading = true;
        $http.get(serviceUrlPrefix + "/category/action", {
            params: {
                actionType: "archive",
                categoryId : this.categoryDialog.category.id
            },
            withCredentials: true,
            timeout: this.cancellationToken.promise,
        }).then(function successCallback(response) {
            commonUtils.handleSuccessArchiveCategoryRequest(self);
        }, function errorCallback(response) {
            self.categoryDialog.isLoading = false;
            commonUtils.handleCancellableHttpError(response, self.cancellationToken);
        });
    };

    // Отправляет на сервер архивацию категории расширенную
    this.archiveCategory = function () {
        switch (this.categoryDialog.step) {
            case 0:
                // Выбор между Заархивировать и Перенести и заархивировать
                this.archiveCategoryForm.$$controls.forEach(function (control) {
                    control.$setDirty();
                });
                if (this.archiveCategoryForm.$invalid) return;
                // Отправка на сервер
                this.cancellationToken = $q.defer();
                this.categoryDialog.isLoading = true;
                if (this.categoryDialog.moveObjects) {
                    // Выбран перенос дочерних категорий и связанных документов и затем архивирование категории,
                    // нужно передать на сервер ИД категории и что надо переносить объекты
                    $http.get(serviceUrlPrefix + "/category/action", {
                        params: {
                            actionType: "archiveWithRelation",
                            categoryId : this.categoryDialog.category.id,
                            moveCategoryId: this.categoryDialog.categoryToMoveTo.id,
                            relationAction: "move",
                        },
                        withCredentials: true,
                        timeout: this.cancellationToken.promise,
                    }).then(function successCallback(response) {
                        commonUtils.handleSuccessArchiveCategoryRequest(self, self.categoryDialog.categoryToMoveTo.id);
                    }, function errorCallback(response) {
                        self.categoryDialog.isLoading = false;
                        commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                    });
                }
                else {
                    // Выбрано архивирование категории вместе с дочерними и связанными документами, нужно передать ИД
                    // категории и спросить, есть ли в ней документы, связанные с несколькими категориями, сколько
                    $http.get(serviceUrlPrefix + "/category/action", {
                        params: {
                            actionType: "documentsWithOtherLink",
                            categoryId : this.categoryDialog.category.id,
                        },
                        withCredentials: true,
                        timeout: this.cancellationToken.promise,
                    }).then(function successCallback(response) {
                        var crossLinkedCount = response.data.documentsWithOtherLinks.length;
                        if (crossLinkedCount) {
                            // В архивируемой категории есть документы, связанные с несколькими категориями
                            self.categoryDialog.crossLinkedCount = crossLinkedCount;
                            self.categoryDialog.archiveCrossLinked = false;
                            self.categoryDialog.step = 1;
                            self.categoryDialog.isLoading = false;
                        }
                        else {
                            // В архивируемой категории нет документов, связанных с несколькими категориями
                            self.cancellationToken = $q.defer();
                            self.categoryDialog.isLoading = true;
                            $http.get(serviceUrlPrefix + "/category/action", {
                                params: commonUtils.addThemeDisplay({
                                    actionType: "archiveWithRelation",
                                    categoryId : self.categoryDialog.category.id,
                                    relationAction: "go",
                                }),
                                withCredentials: true,
                                timeout: self.cancellationToken.promise,
                            }).then(function successCallback(response) {
                                commonUtils.handleSuccessArchiveCategoryRequest(self);
                            }, function errorCallback(response) {
                                self.categoryDialog.isLoading = false;
                                commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                            });
                        }
                    }, function errorCallback(response) {
                        self.categoryDialog.isLoading = false;
                        commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                    });
                }
                break;
            case 1:
                // Выбор архивирования или пропуска документов, связанных с несколькими категориями,
                // передать ИД категории и что надо архивировать объекты, связанные с этой категорией
                this.cancellationToken = $q.defer();
                this.categoryDialog.isLoading = true;
                $http.get(serviceUrlPrefix + "/category/action", {
                    params: commonUtils.addThemeDisplay({
                        actionType: "archiveWithRelation",
                        categoryId : this.categoryDialog.category.id,
                        relationAction: "go",
                        documentsWithOtherLinksAction: self.categoryDialog.archiveCrossLinked ? "go" : "skip",
                    }),
                    withCredentials: true,
                    timeout: this.cancellationToken.promise,
                }).then(function successCallback(response) {
                    commonUtils.handleSuccessArchiveCategoryRequest(self);
                }, function errorCallback(response) {
                    self.categoryDialog.isLoading = false;
                    commonUtils.handleCancellableHttpError(response, self.cancellationToken);
                });
                break;
        }
    };

    // Открывает диалог разархивации категории
    this.openUnarchiveCategoryDialog = function (category) {
        if (exclusiveActionInProgress) return;
        // Получить с сервера количество подкатегорий и документов
        category.isLoading = exclusiveActionInProgress = true;
        $http.get(serviceUrlPrefix + "/category/action", {
            params: {
                actionType: "relations",
                categoryId : category.id,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            var subCategoriesCount = response.data.categoryCount;
            var documentsCount = response.data.documentCount;
            self.categoryDialog = {};
            self.categoryDialog.category = category;
            self.categoryDialog.categoryEscapedName = Liferay.Util.escape(category.name);
            self.categoryDialog.isLoading = false;
            if (subCategoriesCount > 0 || documentsCount > 0) {
                /* self.categoryDialog.step = 0;
                self.unarchiveCategoryForm.$setPristine(); */ // INKNOW-1895
                self.showUnarchCategoryDialog = true;
            }
            else {
                self.showUnarchSimpleCategoryDialog = true;
            }
        }, function errorCallback(response) {
            commonUtils.handleHttpError(response);
        }).finally(function () {
            category.isLoading = exclusiveActionInProgress = false;
        });
    };

    // Отправляет на сервер разархивацию категории
    this.unarchiveCategory = function (simple) {
        /* if (!simple) {
            this.unarchiveCategoryForm.$$controls.forEach(function (control) {
                control.$setDirty();
            });
            if (this.unarchiveCategoryForm.$invalid) return;
        } */ // INKNOW-1895
        // Отправка на сервер запроса на разархивацию
        this.cancellationToken = $q.defer();
        this.categoryDialog.isLoading = true;
        var params = {
            categoryId : this.categoryDialog.category.id,
        };
        if (simple) {
            params.actionType = "unarchive";
        }
        else {
            params.actionType = "unarchiveWithRelation";
            /* if (!this.categoryDialog.neverExpire) {
                params.expirationDate = makeCategoryExpirationTime().getTime();
            } */ // INKNOW-1895
        }
        $http.get(serviceUrlPrefix + "/category/action", {
            params: commonUtils.addThemeDisplay(params),
            withCredentials: true,
            timeout: this.cancellationToken.promise,
        }).then(function successCallback(response) {
            commonUtils.handleSuccessUnarchiveCategoryRequest(self);
        }, function errorCallback(response) {
            self.categoryDialog.isLoading = false;
            commonUtils.handleCancellableHttpError(response, self.cancellationToken);
        });
    };

    // Применение конфигурации
    globalGroupId = configuration.globalGroupId;
    groupId = configuration.groupId;
    plId = configuration.plid;
    portletId = configuration.portletId;
    serviceUrlPrefix = configuration.serviceUrlPrefix;
    currentLang = configuration.currentLang;
    this.defaultLang = configuration.defaultLang;
    this.cancellationToken = null;
    this.dndEnabled = configuration.dndEnabled && configuration.editingEnabled;
    this.editingEnabled = configuration.editingEnabled;
    this.responsiblesEnabled = configuration.responsiblesEnabled;
    this.baseUrl = configuration.baseUrl;
    this.vocabularies = configuration.vocabularies;
    this.globalScopeAvail = configuration.globalScopeAvail;
    this.siteScopeAvail = configuration.siteScopeAvail;
    this.languages = new Array(configuration.defaultLang)
            .concat(configuration.languages.filter(function (lang) {
                return lang !== configuration.defaultLang;
            }));

    // Задание обработчиков событий и параметров деревьев категорий
    this.treeOptions = {
        beforeDrop: onCategoryBeforeDrop,
        dropped: onCategoryDropped,
    };

    $scope.$watch("acmCtrl.vocabularyDialog.vocabularyToMoveTo", function (newValue, oldValue) {
        if (newValue === oldValue) return;
        if (newValue) {
            self.vocabularyDialog.duplicatesWarning = false;
            $http.get(serviceUrlPrefix + "/vocabulary/action", {
                params: {
                    actionType: "vocabularyMoveForDuplication",
                    vocabularyId: self.vocabularyDialog.vocabulary.id,
                    moveVocabularyId: self.vocabularyDialog.vocabularyToMoveTo.id,
                },
                withCredentials: true,
            }).then(function successCallback(response) {
                self.vocabularyDialog.duplicatesWarning = response.data.categoryDuplication;
            });
        }
    });

    $scope.$watch("acmCtrl.categoryDialog.categoryToMoveTo", function (newValue, oldValue) {
        if (newValue === oldValue) return;
        if (newValue) {
            self.categoryDialog.duplicatesWarning = false;
            $http.get(serviceUrlPrefix + "/category/action", {
                params: {
                    actionType: "duplication",
                    categoryId: self.categoryDialog.category.id,
                    moveCategoryId: self.categoryDialog.categoryToMoveTo.id,
                },
                withCredentials: true,
            }).then(function successCallback(response) {
                self.categoryDialog.duplicatesWarning = response.data.categoryDuplication;
            });
        }
        else {
            self.categoryDialog.duplicatesWarning = false;
        }
    });

    // Помещение в контроллер переводов для некоторых директив
    this.translations = {
        placeholders: {
            vocabularyEmpty: configuration.translations["placeholders.vocabulary-empty"],
            categoryName: configuration.translations["placeholders.category-name"],
            categoryDescription: configuration.translations["placeholders.category-description"],
        },
    };
    // Группировка сообщений для удобства последующего доступа к ним
    messages = {
        vocabularyCreated: configuration.translations["messages.vocabulary-created"],
        vocabularyModified: configuration.translations["messages.vocabulary-modified"],
        vocabularyDeleted: configuration.translations["messages.vocabulary-deleted"],
        categoryCreated: configuration.translations["messages.category-created"],
        categoryModified: configuration.translations["messages.category-modified"],
    };
    this.isCategoryPending = commonUtils.isCategoryPending;
    commonUtils.prepareVocabularies(this.vocabularies);
    commonUtils.restoreState(this);
}

})();
