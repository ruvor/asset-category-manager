;(function () {

CommonUtils.$inject = ["$http", "configuration", "showSuccess", "showError"];
function CommonUtils($http, configuration, showSuccess, showError) {
    var self = this;

    // Выводит сообщение об ошибке согласно ответу сервера
    function showHttpError(response) {
        var lead, message;
        if (!response.data) {
            lead = configuration.translations["errors.common-network-error"];
            message = "";
        }
        else if (typeof response.data === "object") {
            lead = response.data.title;
            message = response.data.detail;
            if (response.data.reason) {
                console.error('HTTP error reason: ' + response.data.reason);
            }
        }
        else {
            lead = response.data;
            message = "";
        }
        showError(lead, message);
    }

    // Рекурсивно сохраняет в строке идентификаторы развёрнутых категорий
    function collectExpandedCategoryIdsRecursively(categories, expandedCategoryIds) {
        for (var i = 0; categories && i < categories.length; i++) {
            var category = categories[i];
            if (!category.collapsed) {
                expandedCategoryIds.push(category.id);
                collectExpandedCategoryIdsRecursively(category.categories, expandedCategoryIds);
            }
        }
    }

    // Рекурсивно находит категорию по ИДу, возвращая переданную категорию или
    // одну из дочерних (или более глубокого порядка)
    function findCategoryByIdRecursive(parentCategory, id, path) {
        if (parentCategory.id === id) {
            return parentCategory;
        }
        if (parentCategory.categories) {
            for (var i = 0; i < parentCategory.categories.length; i++) {
                var category = parentCategory.categories[i];
                var foundCategory = findCategoryByIdRecursive(category, id, path);
                if (foundCategory) {
                    path.push(category);
                    return foundCategory;
                }
            }
        }
        return null;
    }

    // Ищет категорию в текущем дереве по ИДу, если находит, то разворачивает
    // все её родительские категории и словарь, перезагружает и разворачивает её
    function exhibitCategoryById(ctrl, id) {
        var path = [];
        for (var i = 0; i < ctrl.vocabularies.length; i++) {
            var vocabulary = ctrl.vocabularies[i];
            for (var j = 0; j < vocabulary.categories.length; j++) {
                var category = vocabulary.categories[j];
                var foundCategory = findCategoryByIdRecursive(category, id, path);
                if (foundCategory) {
                    path.push(category);
                    break;
                }
            }
            if (foundCategory) {
                path.push(vocabulary);
                break;
            }
        }
        if (foundCategory) {
            self.reloadCategory(ctrl, foundCategory);
            for (var k = 1; k < path.length; k++) {
                path[k].collapsed = false;
            }
        }
    }

    // Заполняет у словарей свойство categories, если его не было,
    // это необходимо для корректной работы дерева, а также помечает
    // словари свёрнутыми
    this.prepareVocabularies = function (vocabularies) {
        for (var i = 0; i < vocabularies.length; i++) {
            var vocabulary = vocabularies[i];
            if (!vocabulary.categories) {
                vocabulary.categories = [];
            }
            vocabulary.collapsed = true;
        }
    };

    // Рекурсивно заполняет у категорий свойство categories, если его не было,
    // это необходимо для корректной работы дерева, а также сворачивает
    // пустые категории и разворачивает непустые
    this.prepareCategoriesRecursive = function (categories) {
        for (var i = 0; i < categories.length; i++) {
            var category = categories[i];
            if (category.categories) {
                // Если с сервера приходит заполненное свойство categories,
                // то по соглашению оно не может быть пустым, такие категории
                // нужно развернуть
                category.collapsed = false;
                this.prepareCategoriesRecursive(category.categories);
            }
            else {
                category.categories = [];
                // Нужно свернуть категорию для корректной индикации состояния
                category.collapsed = true;
            }
            category.archived = category.properties ? category.properties.archived : false;
        }
    };

    // Проверяет, является ли словарь "отложенным", т. е. имеет категории,
    // но данных по ним ещё нет
    this.isVocabularyPending = function (vocabulary) {
        return vocabulary.hasChildren && !vocabulary.categories.length;
    }

    // Проверяет, является ли категория "отложенной", т. е. имеет подкатегории,
    // но данных по ним ещё нет
    this.isCategoryPending = function (category) {
        return category.hasChildren && !category.categories.length;
    };

    // Перезагружает словарь, разворачивает его, сохраняет состояние развёрнутости
    this.reloadVocabulary = function (ctrl, vocabulary) {
        vocabulary.isLoading = true;
        $http.get(configuration.serviceUrlPrefix + "/vocabulary/category-tree", {
            params: {
                vocabularyId: vocabulary.id,
                languageId : configuration.currentLang,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            var categories = response.data.categories;
            self.prepareCategoriesRecursive(categories);
            vocabulary.categories = categories;
            vocabulary.collapsed = false;
            self.saveState(ctrl.vocabularies);
        }, function errorCallback(response) {
            self.handleHttpError(response);
        }).finally(function () {
            vocabulary.isLoading = false;
        });
    };

    // Перезагружает категорию, разворачивает её, сохраняет состояние развёрнутости
    this.reloadCategory = function (ctrl, category) {
        category.isLoading = true;
        $http.get(configuration.serviceUrlPrefix + "/category/child-categories", {
            params: {
                categoryId : category.id,
                languageId : configuration.currentLang,
            },
            withCredentials: true,
        }).then(function successCallback(response) {
            var subCategories = response.data.categories;
            if (subCategories) {
                // Проверка необходима, потому что перезагрузка категории может быть
                // запрошена даже для категории без дочерних (например, после удаления
                // категории с переносом данных)
                self.prepareCategoriesRecursive(subCategories);
                category.categories = subCategories;
                category.collapsed = false;
            } else {
                category.categories = [];
                category.collapsed = true;
            }
            self.saveState(ctrl.vocabularies);
        }, function errorCallback(response) {
            self.handleHttpError(response);
        }).finally(function () {
            category.isLoading = false;
        });
    };

    // Обрабатывает ошибку промиса $http и выводит сообщение
    this.handleHttpError = function (response) {
        showHttpError(response);
    };

    // Обрабатывает ошибку промиса $http и выводит сообщение, если ошибка не
    // вызвана отменой
    this.handleCancellableHttpError = function (response, cancellationToken) {
        if (cancellationToken) {
            // Ошибка не вызвана отменой запроса по закрытию диалога
            showHttpError(response);
        }
        else {
            // Токен отмены был сброшен для отмены HTTP-запроса; строго говоря,
            // это не есть ошибка, но в соответствии с логикой работы сервиса
            // $http срабатывает обработчик ошибки промиса; здесь ничего не нужно делать
        }
    };

    // Сохраняет состояние деревьев категорий словарей
    this.saveState = function (vocabularies) {
        var expandedVocabularyIds = [];
        var expandedCategoryIds = [];
        for (var i = 0; i < vocabularies.length; i++) {
            var vocabulary = vocabularies[i];
            if (!vocabulary.collapsed) {
                expandedVocabularyIds.push(vocabulary.id);
                collectExpandedCategoryIdsRecursively(vocabulary.categories, expandedCategoryIds);
            }
        }
        try {
            var state = expandedVocabularyIds + "|" + expandedCategoryIds;
            if (state === "|") {
                localStorage.removeItem(configuration.stateStorageKey);
            }
            else {
                localStorage[configuration.stateStorageKey] = state;
            }
        }
        catch (e) {
        }
    };

    // Восстанавливает состояние деревьев категорий словарей в соответствии с ранее
    // сохранённым, запрашивая необходимую информацию у сервера
    this.restoreState = function (ctrl) {
        var expandedVocabularyIdsStr = "";
        var expandedCategoryIdsStr = "";
        try {
            var state = localStorage[configuration.stateStorageKey];
            if (angular.isString(state)) {
                var stateParts = state.split("|");
                expandedVocabularyIdsStr = stateParts[0];
                expandedCategoryIdsStr = stateParts[1];
            }
        }
        catch (e) {
        }
        if (expandedVocabularyIdsStr) {
            expandedVocabularyIdsStr = "," + expandedVocabularyIdsStr + ",";
        }
        if (expandedCategoryIdsStr) {
            expandedCategoryIdsStr = "," + expandedCategoryIdsStr + ",";
        }
        if (expandedCategoryIdsStr || expandedVocabularyIdsStr) {
            // Есть сохранённые ИДы словарей и категорий, которые надо развернуть
            var updatingVocabularies = {};
            for (var i = 0; i < ctrl.vocabularies.length; i++) {
                // Перебор всех словарей
                var vocabulary = ctrl.vocabularies[i];
                if (expandedVocabularyIdsStr.indexOf("," + vocabulary.id + ",") > -1) {
                    // Текущий перебираемый словарь подлежит разворачиванию
                    if (!vocabulary.hasChildren) {
                        // но уже известно, что у этого словаря нет категорий, его можно сразу развернуть
                        vocabulary.collapsed = false;
                    }
                    else {
                        // Перед разворотом нужно запросить список категорий
                        vocabulary.isLoading = true;
                        updatingVocabularies[vocabulary.id] = vocabulary;
                    }
                }
            }
            if (!Object.keys(updatingVocabularies).length)
            {
                // Например, ранее открывавшиеся словари теперь не имеют категорий или
                // самих словарей уже нету
                this.saveState(ctrl.vocabularies); // Для возможной очистки от старых ИДов
                return;
            }
            // Запрос на сервер с передачей этих ИДов
            var params = {
                vocabularyId: Object.keys(updatingVocabularies),
                languageId : configuration.currentLang,
            };
            if (expandedCategoryIdsStr) {
                params.parentCategoryId = expandedCategoryIdsStr.slice(1, -1).split(",");
            }
            $http.get(configuration.serviceUrlPrefix + "/vocabulary/category-trees", {
                params: params,
                withCredentials: true,
            }).then(function successCallback(response) {
                var vocabularies = response.data;
                self.prepareVocabularies(vocabularies);
                for (var i = 0; i < vocabularies.length; i++) {
                    var vocabulary = vocabularies[i];
                    var updatingVocabulary = updatingVocabularies[vocabulary.id];
                    if (!updatingVocabulary) {
                        // Если сервер вернул информацию по незапрошенному словарю
                        continue;
                    }
                    self.prepareCategoriesRecursive(vocabulary.categories);
                    updatingVocabulary.categories = vocabulary.categories;
                    updatingVocabulary.collapsed = false;
                }
                self.saveState(ctrl.vocabularies); // Для возможной очистки от старых ИДов
            }).finally(function () {
                for (var i = 0; i < ctrl.vocabularies.length; i++) {
                    ctrl.vocabularies[i].isLoading = false;
                }
            });
        }
    };

    // Находит в словаре категорию по ИДу
    this.findCategoryById = function (vocabulary, categoryId) {
        var result = null;
        for (var i = 0; i < vocabulary.categories.length; i++) {
            var category = vocabulary.categories[i];
            result = findCategoryByIdRecursive(category, categoryId, []);
            if (result !== null) break;
        }
        return result;
    };

    // Рекурсивно проверяет, есть ли среди переданных категорий "отложенные"
    this.areTherePendingCategoriesRecursive = function (categories) {
        for (var i = 0; i < categories.length; i++) {
            var category = categories[i];
            if (self.isCategoryPending(category)) {
                return true;
            }
            if (this.areTherePendingCategoriesRecursive(category.categories)) {
                return true;
            }
        }
        return false;
    };

    // Рекурсивно разворачивает категории, имеющие дочерние категории
    this.expandCategoriesRecursive = function (categories) {
        for (var i = 0; categories && i < categories.length; i++) {
            var category = categories[i];
            if (category.categories.length) {
                category.collapsed = false;
                this.expandCategoriesRecursive(category.categories);
            }
            else {
                category.collapsed = true;
            }
        }
    };

    // Рекурсивно помечает категории свёрнутыми
    this.collapseCategoriesRecursive = function (categories) {
        for (var i = 0; categories && i < categories.length; i++) {
            var category = categories[i];
            category.collapsed = true;
            this.collapseCategoriesRecursive(category.categories);
        }
    };

    // Удаляет категорию из дерева
    this.removeCategoryFromTree = function (ctrl, category, vocabulary, parentCategory) {
        if (parentCategory) {
            parentCategory.categories.splice(parentCategory.categories.indexOf(category), 1);
            self.handleCategoryAfterSubDelete(parentCategory);
        }
        else {
            vocabulary.categories.splice(vocabulary.categories.indexOf(category), 1);
            self.handleVocabularyAfterSubDelete(vocabulary);
        }
        self.saveState(ctrl.vocabularies);
    };

    // Выставляет корректные свойства словаря после удаления подкатегории
    this.handleVocabularyAfterSubDelete = function (vocabulary) {
        if (!vocabulary.categories.length) {
            vocabulary.hasChildren = false;
        }
    };

    // Выставляет корректные свойства категории после удаления подкатегории
    this.handleCategoryAfterSubDelete = function (category) {
        if (!category.categories.length) {
            category.hasChildren = false;
            category.collapsed = true;
        }
    };

    // Обрабатывает успешный запрос на удаление категории
    this.handleSuccessDeleteCategoryRequest = function (ctrl, exhibitCategoryId) {
        self.removeCategoryFromTree(ctrl, ctrl.categoryDialog.category,
                ctrl.categoryDialog.vocabulary, ctrl.categoryDialog.parentCategory);
        if (exhibitCategoryId) {
            exhibitCategoryById(ctrl, ctrl.categoryDialog.categoryToMoveTo.id);
            self.saveState(ctrl.vocabularies);
        }
        ctrl.showDelCategoryDialog = false;
        ctrl.showDelSimpleCategoryDialog = false;
        showSuccess(configuration.translations["messages.category-deleted"]);
    };

    // Обрабатывает успешный запрос на архивацию категории
    this.handleSuccessArchiveCategoryRequest = function (ctrl, exhibitCategoryId) {
        ctrl.categoryDialog.category.archived = true;
        if (ctrl.categoryDialog.category.categories.length) {
            self.reloadCategory(ctrl, ctrl.categoryDialog.category);
        }
        if (exhibitCategoryId) {
            exhibitCategoryById(ctrl, ctrl.categoryDialog.categoryToMoveTo.id);
            self.saveState(ctrl.vocabularies);
        }
        ctrl.showArchCategoryDialog = false;
        ctrl.showArchSimpleCategoryDialog = false;
        showSuccess(configuration.translations["messages.category-archived"]);
    };

    // Обрабатывает успешный запрос на разархивацию категории
    this.handleSuccessUnarchiveCategoryRequest = function (ctrl) {
        ctrl.categoryDialog.category.archived = false;
        if (ctrl.categoryDialog.category.categories.length) {
            self.reloadCategory(ctrl, ctrl.categoryDialog.category);
        }
        ctrl.showUnarchCategoryDialog = false;
        ctrl.showUnarchSimpleCategoryDialog = false;
        showSuccess(configuration.translations["messages.category-unarchived"]);
    };

    // Добавляет в коллекцию параметров запроса компоненты объекта ThemeDisplay
    this.addThemeDisplay = function (params) {
        var supplementedParams = Object.assign({}, params);
        supplementedParams.assetCategoryManagerThemeDisplay = JSON.stringify({
            portalURL: Liferay.ThemeDisplay.getPortalURL(),
            siteGroupId: Liferay.ThemeDisplay.getSiteGroupId(),
            languageId: Liferay.ThemeDisplay.getLanguageId(),
        });
        return supplementedParams;
    };
}

angular
    .module("assetCategoryManager")
    .service("commonUtils", CommonUtils);

})();
