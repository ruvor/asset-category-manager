angular
    .module("assetCategoryManager", [
        "ngAnimate",
        "pascalprecht.translate",
        "simpleConfigurator",
        "ui.tree",
        "liferay7UITools",
        "simpleAutocompl",
    ]);
