;(function () {

angular
    .module("assetCategoryManager")
    .directive("usersSelector", usersSelectorDirectiveFactory);

usersSelectorDirectiveFactory.$inject = ["$http", "$q", "configuration", "filterFilter"];
function usersSelectorDirectiveFactory($http, $q, configuration, filterFilter) {
    return {
        scope: {
            readonly: '<',
        },

        restrict: "AE",

        require: 'ngModel',

        templateUrl: configuration.baseUrl + "/users-selector.directive.html",

        link: function (scope, element, attrs, ngModel) {
            var serviceUrlPrefix, currentLang;

            scope.userNotFoundMessage = configuration.translations["errors.user-not-found"];

            scope.getUsers = function (str) {
                return !str ? [] : $q(function(resolve, reject) {
                    $http.get(serviceUrlPrefix + "/user/search", {
                        params: {
                            keyword: str,
                            languageId : currentLang
                        },
                        withCredentials: true,
                    }).then(function successCallback(response) {
                        var selectedUsersIds = scope.selectedUsers ?
                                scope.selectedUsers.map(function (selectedUser) {
                                    return selectedUser.id;
                                }) : [];
                        resolve(filterFilter(response.data, function (user) {
                            return selectedUsersIds.indexOf(user.id) < 0;
                        }));
                    }, function errorCallback(response) {
                        reject();
                    });
                });
            };

            scope.unselect = function (index) {
                scope.selectedUsers.splice(index, 1);
                ngModel.$validate();
            };

            // Применение конфигурации
            serviceUrlPrefix = configuration.serviceUrlPrefix;
            currentLang = configuration.currentLang;

            ngModel.$render = function () {
                scope.selectedUsers = ngModel.$viewValue;
            };

            scope.$watch("user", function (newValue, oldValue) {
                if (newValue === oldValue) return;
                if (newValue) {
                    if (!scope.selectedUsers) {
                        scope.selectedUsers = [];
                        ngModel.$setViewValue(scope.selectedUsers);
                    }
                    scope.selectedUsers.push(newValue);
                    scope.user = undefined;
                }
            });
        },
    };
}

})();
