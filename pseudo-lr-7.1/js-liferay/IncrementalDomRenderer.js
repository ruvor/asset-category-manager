'use strict';

Liferay.Loader.define("frontend-js-metal-web$metal-incremental-dom@2.16.5/lib/IncrementalDomRenderer", ['module', 'exports', 'require', './incremental-dom', './changes', './data', './children/children', './render/patch', './render/render', 'frontend-js-metal-web$metal-component'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	require('./incremental-dom');

	var _changes = require('./changes');

	var _data = require('./data');

	var _children = require('./children/children');

	var _patch2 = require('./render/patch');

	var _render = require('./render/render');

	var _metalComponent = require("frontend-js-metal-web$metal-component");

	function _toConsumableArray(arr) {
		if (Array.isArray(arr)) {
			for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
				arr2[i] = arr[i];
			}return arr2;
		} else {
			return Array.from(arr);
		}
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	var IncrementalDomRenderer = function (_ComponentRenderer$co) {
		_inherits(IncrementalDomRenderer, _ComponentRenderer$co);

		function IncrementalDomRenderer() {
			_classCallCheck(this, IncrementalDomRenderer);

			return _possibleConstructorReturn(this, (IncrementalDomRenderer.__proto__ || Object.getPrototypeOf(IncrementalDomRenderer)).apply(this, arguments));
		}

		_createClass(IncrementalDomRenderer, [{
			key: 'buildShouldUpdateArgs',

			/**
    * Returns an array with the args that should be passed to the component's
    * `shouldUpdate` method. This can be overridden by sub classes to change
    * what the method should receive.
    * @param {Object} changes
    * @return {!Array}
    */
			value: function buildShouldUpdateArgs(changes) {
				return [changes.props];
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'dispose',
			value: function dispose(component) {
				var data = (0, _data.getData)(component);
				var ref = data.config.ref;
				var owner = data.owner;
				if (owner && owner.components && owner.components[ref] === component) {
					delete owner.components[ref];
				}

				if (data.childComponents) {
					for (var i = 0; i < data.childComponents.length; i++) {
						var child = data.childComponents[i];
						if (!child.isDisposed()) {
							if (!child.portalElement) {
								child.element = null;
							}
							child.dispose();
						}
					}
				}

				(0, _data.clearData)(component);
			}

			/**
    * Generates a key for the element currently being rendered in the given
    * component. By default, just returns the original key. Sub classes can
    * override this to change the behavior.
    * @param {!Component} component
    * @param {string} key
    * @return {?string}
    */

		}, {
			key: 'generateKey',
			value: function generateKey(component, key) {
				return key;
			}

			/**
    * Get the component's config data.
    * @param {!Component} component
    * @return {!Object}
    */

		}, {
			key: 'getConfig',
			value: function getConfig(component) {
				return (0, _data.getData)(component).config;
			}

			/**
    * Get the component's incremental dom renderer data.
    * @param {!Component} component
    * @return {!Object}
    */

		}, {
			key: 'getData',
			value: function getData(component) {
				return (0, _data.getData)(component);
			}

			/**
    * Gets the component that triggered the current patch operation.
    * @return {Component}
    */

		}, {
			key: 'getPatchingComponent',
			value: function getPatchingComponent() {
				return (0, _patch2.getPatchingComponent)();
			}

			/**
    * Handles a node having just been rendered. Sub classes should override this
    * for custom behavior.
    */

		}, {
			key: 'handleNodeRendered',
			value: function handleNodeRendered() {}

			/**
    * Checks if the given object is an incremental dom node.
    * @param {!Object} node
    * @return {boolean}
    */

		}, {
			key: 'isIncDomNode',
			value: function isIncDomNode(node) {
				return !!(0, _children.getOwner)(node);
			}

			/**
    * Calls incremental dom's patch function to render the component.
    * @param {!Component} component
    */

		}, {
			key: 'patch',
			value: function patch(component) {
				(0, _patch2.patch)(component);
			}

			/**
    * Renders the renderer's component for the first time, patching its element
    * through incremental dom function calls. If the first arg is a function
    * instead of a component instance, creates and renders this function, which
    * can either be a simple incremental dom function or a component constructor.
    * @param {!Component|function()} component Can be a component instance, a
    *     simple incremental dom function or a component constructor.
    * @param {Object|Element=} dataOrElement Optional config data for the
    *     function, or parent for the rendered content.
    * @param {Element=} parent Optional parent for the rendered content.
    * @return {!Component} The rendered component's instance.
    */

		}, {
			key: 'render',
			value: function render(component, dataOrElement, parent) {
				if (component[_metalComponent.Component.COMPONENT_FLAG]) {
					this.patch(component);
				} else {
					return (0, _render.renderFunction)(this, component, dataOrElement, parent);
				}
			}

			/**
    * Renders the given child node via its owner renderer.
    * @param {!Object} child
    */

		}, {
			key: 'renderChild',
			value: function renderChild(child) {
				(0, _render.renderChild)(child);
			}

			/**
    * Calls functions from `IncrementalDOM` to build the component element's
    * content. Can be overriden by subclasses (for integration with template
    * engines for example).
    * @param {!Component} component
    */

		}, {
			key: 'renderIncDom',
			value: function renderIncDom(component) {
				if (component.render) {
					component.render();
				} else {
					IncrementalDOM.elementVoid('div');
				}
			}

			/**
    * Runs the incremental dom functions for rendering this component, without
    * calling `patch`. This function needs to be called inside a `patch`.
    * @param {!Component} component
    */

		}, {
			key: 'renderInsidePatch',
			value: function renderInsidePatch(component) {
				var changes = (0, _changes.getChanges)(component);

				var shouldRender = !component.wasRendered || this.shouldUpdate(component, changes) || IncrementalDOM.currentPointer() !== component.element;
				if (shouldRender) {
					this.willUpdate_(component, changes);

					(0, _render.render)(component);
				} else if (component.element) {
					this.skipRender();
				}
			}

			/**
    * Sets up this component to be used by this renderer.
    * @param {!Component} component
    */

		}, {
			key: 'setUp',
			value: function setUp(component) {
				component.context = {};
				component.components = {};
				component.refs = {};

				var data = (0, _data.getData)(component);
				data.config = component.getInitialConfig();
				(0, _changes.trackChanges)(component);
			}

			/**
    * Checks if the component should be updated with the current state changes.
    * @param {!Component} component
    * @param {Object} changes
    * @return {boolean}
    */

		}, {
			key: 'shouldUpdate',
			value: function shouldUpdate(component, changes) {
				if (!changes) {
					return false;
				}
				if (component.shouldUpdate) {
					return component.shouldUpdate.apply(component, _toConsumableArray(this.buildShouldUpdateArgs(changes))); // eslint-disable-line
				}
				return true;
			}

			/**
    * Skips the next disposal of children components, by clearing the array as
    * if there were no children rendered the last time. This can be useful for
    * allowing components to be reused by other parent components in separate
    * render update cycles.
    * @param {!Component} component
    */

		}, {
			key: 'skipNextChildrenDisposal',
			value: function skipNextChildrenDisposal(component) {
				(0, _data.getData)(component).childComponents = null;
			}

			/**
    * Skips rendering the current node.
    */

		}, {
			key: 'skipRender',
			value: function skipRender() {
				IncrementalDOM.skipNode();
			}

			/**
    * Updates the renderer's component when state changes, patching its element
    * through incremental dom function calls.
    * @param {!Component} component
    * @param {Object} data
    */

		}, {
			key: 'update',
			value: function update(component, data) {
				var changes = (0, _changes.getChanges)(component);
				if (data.forceUpdate || this.shouldUpdate(component, changes)) {
					this.willUpdate_(component, changes);
					this.patch(component);
				}
			}

			/**
    * Invokes component's "willUpdate" lifecycle method if applicable.
    * @param {!Component} component
    * @param {Object} changes
    */

		}, {
			key: 'willUpdate_',
			value: function willUpdate_(component, changes) {
				if (!component.wasRendered || !changes) {
					return;
				}
				component.informWillUpdate.apply(component, _toConsumableArray(this.buildShouldUpdateArgs(changes)));
			}
		}]);

		return IncrementalDomRenderer;
	}(_metalComponent.ComponentRenderer.constructor);

	var renderer = new IncrementalDomRenderer();

	// Name of this renderer. Renderers should provide this as a way to identify
	// them via a simple string (when calling enableCompatibilityMode to add
	// support to old features for specific renderers for example).
	renderer.RENDERER_NAME = 'incremental-dom';

	exports.default = renderer;
});
//# sourceMappingURL=IncrementalDomRenderer.js.map