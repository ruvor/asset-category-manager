'use strict';

Liferay.Loader.define('frontend-taglib-clay$clay-alert@2.1.12/lib/ClayAlertBase.soy', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.templates = exports.ClayAlertBase = undefined;

  var _metalComponent = require('frontend-js-metal-web$metal-component');

  var _metalComponent2 = _interopRequireDefault(_metalComponent);

  var _metalSoy = require('frontend-js-metal-web$metal-soy');

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  } /* jshint ignore:start */

  var templates;
  goog.loadModule(function (exports) {
    var soy = goog.require('soy');
    var soydata = goog.require('soydata');
    // This file was automatically generated from ClayAlertBase.soy.
    // Please don't edit this file by hand.

    /**
     * @fileoverview Templates in namespace ClayAlertBase.
     * @public
     */

    goog.module('ClayAlertBase.incrementaldom');

    goog.require('goog.soy.data.SanitizedContent');
    var incrementalDom = goog.require('incrementaldom');
    goog.require('soy.asserts');
    var soyIdom = goog.require('soy.idom');

    var $templateAlias2 = _metalSoy2.default.getTemplate('ClayButton.incrementaldom', 'render');

    var $templateAlias1 = _metalSoy2.default.getTemplate('ClayIcon.incrementaldom', 'render');

    /**
     * @param {$render.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $render = function $render(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      /** @type {!goog.soy.data.SanitizedContent|function()|string} */
      var message = soy.asserts.assertType(goog.isFunction(opt_data.message) || goog.isString(opt_data.message) || opt_data.message instanceof goog.soy.data.SanitizedContent, 'message', opt_data.message, '!goog.soy.data.SanitizedContent|function()|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var spritemap = soy.asserts.assertType(goog.isString(opt_data.spritemap) || opt_data.spritemap instanceof goog.soy.data.SanitizedContent, 'spritemap', opt_data.spritemap, '!goog.soy.data.SanitizedContent|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var title = soy.asserts.assertType(goog.isString(opt_data.title) || opt_data.title instanceof goog.soy.data.SanitizedContent, 'title', opt_data.title, '!goog.soy.data.SanitizedContent|string');
      /** @type {*|null|undefined} */
      var _handleCloseClick = opt_data._handleCloseClick;
      /** @type {boolean|null|undefined} */
      var _visible = soy.asserts.assertType(opt_data._visible == null || goog.isBoolean(opt_data._visible) || opt_data._visible === 1 || opt_data._visible === 0, '_visible', opt_data._visible, 'boolean|null|undefined');
      /** @type {*|null|undefined} */
      var autoClose = opt_data.autoClose;
      /** @type {boolean|null|undefined} */
      var closeable = soy.asserts.assertType(opt_data.closeable == null || goog.isBoolean(opt_data.closeable) || opt_data.closeable === 1 || opt_data.closeable === 0, 'closeable', opt_data.closeable, 'boolean|null|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var elementClasses = soy.asserts.assertType(opt_data.elementClasses == null || goog.isString(opt_data.elementClasses) || opt_data.elementClasses instanceof goog.soy.data.SanitizedContent, 'elementClasses', opt_data.elementClasses, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var id = soy.asserts.assertType(opt_data.id == null || goog.isString(opt_data.id) || opt_data.id instanceof goog.soy.data.SanitizedContent, 'id', opt_data.id, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var style = soy.asserts.assertType(opt_data.style == null || goog.isString(opt_data.style) || opt_data.style instanceof goog.soy.data.SanitizedContent, 'style', opt_data.style, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var type = soy.asserts.assertType(opt_data.type == null || goog.isString(opt_data.type) || opt_data.type instanceof goog.soy.data.SanitizedContent, 'type', opt_data.type, '!goog.soy.data.SanitizedContent|null|string|undefined');
      var attributes__soy68 = function attributes__soy68() {
        incrementalDom.attr('class', 'alert alert-' + (style != null ? style : 'info') + ' fade' + (closeable ? ' alert-dismissible' : '') + (elementClasses ? ' ' + elementClasses : '') + (type == 'stripe' ? ' alert-fluid' : '') + (_visible != false ? ' show' : ''));
        if (autoClose && (type == 'stripe' || type == 'toast') && _visible) {
          incrementalDom.attr('data-onmouseover', '_handleMouseOver');
          incrementalDom.attr('data-onmouseout', '_handleMouseOut');
        }
        if (id) {
          incrementalDom.attr('id', id);
        }
        incrementalDom.attr('role', 'alert');
      };
      var content__soy79 = function content__soy79() {
        $content({ _handleCloseClick: _handleCloseClick, closeable: closeable, message: message, spritemap: spritemap, style: style, title: title }, opt_ijData);
      };
      incrementalDom.elementOpenStart('div');
      attributes__soy68();
      incrementalDom.elementOpenEnd();
      if (type == 'stripe') {
        incrementalDom.elementOpenStart('div');
        incrementalDom.attr('class', 'container-fluid container-fluid-max-xl');
        incrementalDom.elementOpenEnd();
        content__soy79();
        incrementalDom.elementClose('div');
      } else {
        content__soy79();
      }
      incrementalDom.elementClose('div');
    };
    exports.render = $render;
    /**
     * @typedef {{
     *  message: (!goog.soy.data.SanitizedContent|function()|string),
     *  spritemap: (!goog.soy.data.SanitizedContent|string),
     *  title: (!goog.soy.data.SanitizedContent|string),
     *  _handleCloseClick: (*|null|undefined),
     *  _visible: (boolean|null|undefined),
     *  autoClose: (*|null|undefined),
     *  closeable: (boolean|null|undefined),
     *  elementClasses: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  id: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  style: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  type: (!goog.soy.data.SanitizedContent|null|string|undefined),
     * }}
     */
    $render.Params;
    if (goog.DEBUG) {
      $render.soyTemplateName = 'ClayAlertBase.render';
    }

    /**
     * @param {$content.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $content = function $content(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      /** @type {!goog.soy.data.SanitizedContent|function()|string} */
      var message = soy.asserts.assertType(goog.isFunction(opt_data.message) || goog.isString(opt_data.message) || opt_data.message instanceof goog.soy.data.SanitizedContent, 'message', opt_data.message, '!goog.soy.data.SanitizedContent|function()|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var spritemap = soy.asserts.assertType(goog.isString(opt_data.spritemap) || opt_data.spritemap instanceof goog.soy.data.SanitizedContent, 'spritemap', opt_data.spritemap, '!goog.soy.data.SanitizedContent|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var title = soy.asserts.assertType(goog.isString(opt_data.title) || opt_data.title instanceof goog.soy.data.SanitizedContent, 'title', opt_data.title, '!goog.soy.data.SanitizedContent|string');
      /** @type {*|null|undefined} */
      var _handleCloseClick = opt_data._handleCloseClick;
      /** @type {boolean|null|undefined} */
      var closeable = soy.asserts.assertType(opt_data.closeable == null || goog.isBoolean(opt_data.closeable) || opt_data.closeable === 1 || opt_data.closeable === 0, 'closeable', opt_data.closeable, 'boolean|null|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var style = soy.asserts.assertType(opt_data.style == null || goog.isString(opt_data.style) || opt_data.style instanceof goog.soy.data.SanitizedContent, 'style', opt_data.style, '!goog.soy.data.SanitizedContent|null|string|undefined');
      if (closeable) {
        var msg_1_202442244688100878__soy541 = '';
        /** @desc The word 'Close' used as a verb */
        var MSG_EXTERNAL_202442244688100878 = 'Close';
MSG_EXTERNAL_202442244688100878 = MSG_EXTERNAL_202442244688100878.replace(/{(\d+)}/g, '\x01$1\x01')
        msg_1_202442244688100878__soy541 += MSG_EXTERNAL_202442244688100878;
        $templateAlias2({ ariaLabel: '' + msg_1_202442244688100878__soy541, elementClasses: 'close', events: { click: _handleCloseClick }, icon: 'times', iconAlignment: 'right', spritemap: spritemap, style: 'unstyled', type: 'button' }, opt_ijData);
      }
      var icon__soy132 = '';
      var $tmp = style;
      switch (goog.isObject($tmp) ? $tmp.toString() : $tmp) {
        case 'danger':
          icon__soy132 += 'exclamation-full';
          break;
        case 'success':
          icon__soy132 += 'check-circle-full';
          break;
        case 'warning':
          icon__soy132 += 'warning-full';
          break;
        default:
          icon__soy132 += 'info-circle';
      }
      incrementalDom.elementOpenStart('span');
      incrementalDom.attr('class', 'alert-indicator');
      incrementalDom.elementOpenEnd();
      $templateAlias1({ spritemap: spritemap, symbol: icon__soy132 }, opt_ijData);
      incrementalDom.elementClose('span');
      incrementalDom.elementOpenStart('strong');
      incrementalDom.attr('class', 'lead');
      incrementalDom.elementOpenEnd();
      soyIdom.print(title);
      incrementalDom.elementClose('strong');
      soyIdom.print(message);
    };
    exports.content = $content;
    /**
     * @typedef {{
     *  message: (!goog.soy.data.SanitizedContent|function()|string),
     *  spritemap: (!goog.soy.data.SanitizedContent|string),
     *  title: (!goog.soy.data.SanitizedContent|string),
     *  _handleCloseClick: (*|null|undefined),
     *  closeable: (boolean|null|undefined),
     *  style: (!goog.soy.data.SanitizedContent|null|string|undefined),
     * }}
     */
    $content.Params;
    if (goog.DEBUG) {
      $content.soyTemplateName = 'ClayAlertBase.content';
    }

    exports.render.params = ["message", "spritemap", "title", "_handleCloseClick", "_visible", "autoClose", "closeable", "elementClasses", "id", "style", "type"];
    exports.render.types = { "message": "html|string", "spritemap": "string", "title": "string", "_handleCloseClick": "any", "_visible": "bool", "autoClose": "any", "closeable": "bool", "elementClasses": "string", "id": "string", "style": "string", "type": "string" };
    exports.content.params = ["message", "spritemap", "title", "_handleCloseClick", "closeable", "style"];
    exports.content.types = { "message": "html|string", "spritemap": "string", "title": "string", "_handleCloseClick": "any", "closeable": "bool", "style": "string" };
    exports.templates = templates = exports;
    return exports;
  });

  var ClayAlertBase = function (_Component) {
    _inherits(ClayAlertBase, _Component);

    function ClayAlertBase() {
      _classCallCheck(this, ClayAlertBase);

      return _possibleConstructorReturn(this, (ClayAlertBase.__proto__ || Object.getPrototypeOf(ClayAlertBase)).apply(this, arguments));
    }

    return ClayAlertBase;
  }(_metalComponent2.default);

  _metalSoy2.default.register(ClayAlertBase, templates);
  exports.ClayAlertBase = ClayAlertBase;
  exports.templates = templates;
  exports.default = templates;
  /* jshint ignore:end */
  //# sourceMappingURL=ClayAlertBase.soy.js.map
});
//# sourceMappingURL=ClayAlertBase.soy.js.map