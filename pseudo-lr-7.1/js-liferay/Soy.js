'use strict';

Liferay.Loader.define("frontend-js-metal-web$metal-soy@2.16.5/lib/Soy", ['module', 'exports', 'require', 'frontend-js-metal-web$metal-soy-bundle', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal', 'frontend-js-metal-web$metal-state', 'frontend-js-metal-web$metal-incremental-dom', './SoyAop'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.validators = exports.SoyAop = exports.Soy = exports.Config = undefined;

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _get = function get(object, property, receiver) {
		if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
			var parent = Object.getPrototypeOf(object);if (parent === null) {
				return undefined;
			} else {
				return get(parent, property, receiver);
			}
		} else if ("value" in desc) {
			return desc.value;
		} else {
			var getter = desc.get;if (getter === undefined) {
				return undefined;
			}return getter.call(receiver);
		}
	};

	require("frontend-js-metal-web$metal-soy-bundle");

	var _metalComponent = require("frontend-js-metal-web$metal-component");

	var _metal = require("frontend-js-metal-web$metal");

	var _metalState = require("frontend-js-metal-web$metal-state");

	var _metalIncrementalDom = require("frontend-js-metal-web$metal-incremental-dom");

	var _metalIncrementalDom2 = _interopRequireDefault(_metalIncrementalDom);

	var _SoyAop = require('./SoyAop');

	var _SoyAop2 = _interopRequireDefault(_SoyAop);

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	// The injected data that will be passed to soy templates.
	var ijData = {};

	/**
  * Soy Renderer
  */

	var Soy = function (_IncrementalDomRender) {
		_inherits(Soy, _IncrementalDomRender);

		function Soy() {
			_classCallCheck(this, Soy);

			return _possibleConstructorReturn(this, (Soy.__proto__ || Object.getPrototypeOf(Soy)).apply(this, arguments));
		}

		_createClass(Soy, [{
			key: 'getExtraDataConfig',

			/**
    * Adds the template params to the component's state, if they don't exist yet.
    * @param {!Component} component
    * @return {Object}
    */
			value: function getExtraDataConfig(component) {
				var elementTemplate = component.constructor.TEMPLATE;
				if (!(0, _metal.isFunction)(elementTemplate)) {
					return;
				}

				elementTemplate = _SoyAop2.default.getOriginalFn(elementTemplate);
				this.soyParamTypes_ = elementTemplate.types || {};

				var keys = elementTemplate.params || [];
				var configs = {};
				for (var i = 0; i < keys.length; i++) {
					if (!component[keys[i]]) {
						configs[keys[i]] = {};
					}
				}
				return configs;
			}

			/**
    * Copies the component's state to an object so it can be passed as it's
    * template call's data. The copying needs to be done because, if the component
    * itself is passed directly, some problems occur when soy tries to merge it
    * with other data, due to property getters and setters. This is safer.
    * Also calls the component's "prepareStateForRender" to let it change the
    * data passed to the template.
    * @param {!Component} component
    * @param {!Array<string>} params The params used by this template.
    * @return {!Object}
    * @protected
    */

		}, {
			key: 'buildTemplateData_',
			value: function buildTemplateData_(component, params) {
				var _this2 = this;

				var data = _metal.object.mixin({}, this.getConfig(component));
				component.getStateKeys().forEach(function (key) {
					var value = component[key];
					if (_this2.isHtmlParam_(component, key)) {
						value = soyRenderer_.toIncDom(value);
					}
					data[key] = value;
				});

				for (var i = 0; i < params.length; i++) {
					if (!data[params[i]] && (0, _metal.isFunction)(component[params[i]])) {
						data[params[i]] = component[params[i]].bind(component);
					}
				}

				if ((0, _metal.isFunction)(component.prepareStateForRender)) {
					return component.prepareStateForRender(data) || data;
				} else {
					return data;
				}
			}

			/**
    * Returns the requested template function. This function will be wrapped in
    * another though, just to defer the requirement of the template's module
    * being ready until the function is actually called.
    * @param {string} namespace The soy template's namespace.
    * @param {string} templateName The name of the template function.
    * @return {!function()}
    */

		}, {
			key: 'getTemplate',
			value: function getTemplate(namespace, templateName) {
				return function (data, ignored, ijData) {
					if (!goog.loadedModules_[namespace]) {
						throw new Error('No template with namespace "' + namespace + '" has been loaded yet.');
					}
					return goog.loadedModules_[namespace][templateName](data, ignored, ijData);
				};
			}

			/**
    * Handles an intercepted soy template call. If the call is for a component's
    * main template, then it will be replaced with a call that incremental dom
    * can use for both handling an instance of that component and rendering it.
    * @param {!function()} originalFn The original template function that was
    *     intercepted.
    * @param {Object} data The data the template was called with.
    * @protected
    */

		}, {
			key: 'handleInterceptedCall_',
			value: function handleInterceptedCall_(originalFn) {
				var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

				var args = [originalFn.componentCtor, null, []];
				for (var key in data) {
					if (Object.prototype.hasOwnProperty.call(data, key)) {
						args.push(key, data[key]);
					}
				}
				IncrementalDOM.elementVoid.apply(null, args);
			}

			/**
    * Checks if the given param type is html.
    * @param {!Component} component
    * @param {string} name
    * @protected
    * @return {boolean}
    */

		}, {
			key: 'isHtmlParam_',
			value: function isHtmlParam_(component, name) {
				var state = component.getDataManager().getStateInstance(component);
				if (state.getStateKeyConfig(name).isHtml) {
					return true;
				}

				var elementTemplate = _SoyAop2.default.getOriginalFn(component.constructor.TEMPLATE);
				var type = (elementTemplate.types || {})[name] || '';
				return type.split('|').indexOf('html') !== -1;
			}

			/**
    * Registers the given templates to be used by `Soy` for the specified
    * component constructor.
    * @param {!Function} componentCtor The constructor of the component that
    *     should use the given templates.
    * @param {!Object} templates Object containing soy template functions.
    * @param {string=} mainTemplate The name of the main template that should be
    *     used to render the component. Defaults to "render".
    */

		}, {
			key: 'register',
			value: function register(componentCtor, templates) {
				var mainTemplate = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'render';

				componentCtor.RENDERER = soyRenderer_;
				componentCtor.TEMPLATE = _SoyAop2.default.getOriginalFn(templates[mainTemplate]);
				componentCtor.TEMPLATE.componentCtor = componentCtor;
				_SoyAop2.default.registerForInterception(templates, mainTemplate);
				_metalComponent.ComponentRegistry.register(componentCtor);
			}

			/**
    * Overrides the default method from `IncrementalDomRenderer` so the component's
    * soy template can be used for rendering.
    * @param {!Component} component
    * @param {!Object} data Data passed to the component when rendering it.
    * @override
    */

		}, {
			key: 'renderIncDom',
			value: function renderIncDom(component) {
				var elementTemplate = component.constructor.TEMPLATE;
				if ((0, _metal.isFunction)(elementTemplate) && !component.render) {
					elementTemplate = _SoyAop2.default.getOriginalFn(elementTemplate);
					_SoyAop2.default.startInterception(this.handleInterceptedCall_);
					var data = this.buildTemplateData_(component, elementTemplate.params || []);
					elementTemplate(data, null, ijData);
					_SoyAop2.default.stopInterception();
				} else {
					_get(Soy.prototype.__proto__ || Object.getPrototypeOf(Soy.prototype), 'renderIncDom', this).call(this, component);
				}
			}

			/**
    * Sets the injected data object that should be passed to templates.
    * @param {Object} data
    */

		}, {
			key: 'setInjectedData',
			value: function setInjectedData(data) {
				ijData = data || {};
			}

			/**
    * Overrides the original `IncrementalDomRenderer` method so that only
    * state keys used by the main template can cause updates.
    * @param {!Component} component
    * @param {Object} changes
    * @return {boolean}
    */

		}, {
			key: 'shouldUpdate',
			value: function shouldUpdate(component, changes) {
				var should = _get(Soy.prototype.__proto__ || Object.getPrototypeOf(Soy.prototype), 'shouldUpdate', this).call(this, component, changes);
				if (!should || component.shouldUpdate) {
					return should;
				}

				var fn = component.constructor.TEMPLATE;
				var params = fn ? _SoyAop2.default.getOriginalFn(fn).params : [];
				for (var i = 0; i < params.length; i++) {
					if (changes.props[params[i]]) {
						return true;
					}
				}
				return false;
			}

			/**
    * Converts the given incremental dom function into an html string.
    * @param {!function()} incDomFn
    * @return {string}
    */

		}, {
			key: 'toHtmlString',
			value: function toHtmlString(incDomFn) {
				var element = document.createElement('div');
				IncrementalDOM.patch(element, incDomFn);
				return element.innerHTML;
			}

			/**
    * Converts the given html string into an incremental dom function.
    * @param {string|{contentKind: string, content: string}} value
    * @return {!function()}
    */

		}, {
			key: 'toIncDom',
			value: function toIncDom(value) {
				if ((0, _metal.isObject)(value) && (0, _metal.isString)(value.content) && value.contentKind === 'HTML') {
					value = value.content;
				}
				if ((0, _metal.isString)(value)) {
					value = _metalIncrementalDom.HTML2IncDom.buildFn(value);
				}
				return value;
			}
		}]);

		return Soy;
	}(_metalIncrementalDom2.default.constructor);

	var soyRenderer_ = new Soy();
	soyRenderer_.RENDERER_NAME = 'soy';

	exports.default = soyRenderer_;
	exports.Config = _metalState.Config;
	exports.Soy = soyRenderer_;
	exports.SoyAop = _SoyAop2.default;
	exports.validators = _metalState.validators;
});
//# sourceMappingURL=Soy.js.map