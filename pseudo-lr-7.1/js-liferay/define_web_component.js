'use strict';

Liferay.Loader.define('frontend-js-metal-web$metal-web-component@2.16.5/lib/define_web_component', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-state', 'frontend-js-metal-web$metal'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.defineWebComponent = defineWebComponent;

	var _metalState = require('frontend-js-metal-web$metal-state');

	var _metalState2 = _interopRequireDefault(_metalState);

	var _metal = require('frontend-js-metal-web$metal');

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	/**
  * Register a custom element for a given Metal component.
  *
  * @param {String} tagName The tag name to use for this custom element.
  * @param {!function()} Ctor Metal component constructor.
  * @return {void} Nothing.
  */
	function defineWebComponent(tagName, Ctor) {
		if (!('customElements' in window)) {
			return;
		}

		var observedAttributes = Object.keys(_metalState2.default.getStateStatic(Ctor));

		var props = (0, _metal.getStaticProperty)(Ctor, 'PROPS', _metalState.mergeState);

		var hasProps = (0, _metal.isObject)(props) && Object.keys(props).length;

		if (hasProps) {
			observedAttributes = Object.keys(props);
		}

		/**
   * Custom Element wrapper for Metal components.
   *
   * @constructor
   * @extends HTMLElement
   */
		function CustomElement() {
			return Reflect.construct(HTMLElement, [], CustomElement);
		}

		CustomElement.observedAttributes = observedAttributes;

		Object.setPrototypeOf(CustomElement.prototype, HTMLElement.prototype);
		Object.setPrototypeOf(CustomElement, HTMLElement);

		Object.assign(CustomElement.prototype, {
			/**
    * Handler for when new attribute values are passed to the custom
    * element.
    *
    * @memberof CustomElement
    * @param {!string} attrName name of the changed attribute.
    * @param {!string} oldVal previous value of the attribute.
    * @param {!string} newVal new value of the attribute
    */
			attributeChangedCallback: function attributeChangedCallback(attrName, oldVal, newVal) {
				if (!this.component) {
					return;
				}

				newVal = this.deserializeValue_(newVal);

				if (this.componentHasProps) {
					this.component.props[attrName] = newVal;
				} else {
					this.component[attrName] = newVal;
				}
			},

			/**
    * Handles the initial rendering of the Metal component. Invoked when
    * the custom element enters the document.
    *
    * @memberof CustomElement
    */
			connectedCallback: function connectedCallback() {
				var useShadowDOM = this.getAttribute('useShadowDOM') || false;
				var element = this;

				if (useShadowDOM) {
					element = this.attachShadow({
						mode: 'open'
					});
				}

				var opts = {};
				for (var i = 0, l = observedAttributes.length; i < l; i++) {
					var deserializedValue = this.deserializeValue_(this.getAttribute(observedAttributes[i]));

					if (deserializedValue) {
						opts[observedAttributes[i]] = deserializedValue;
					}
				}
				this.component = new Ctor(opts, element);
				this.componentHasProps = hasProps;
				this.componentEventHandler = this.emit.bind(this);

				this.component.on('*', this.componentEventHandler);
			},

			/**
    * Parses attribute value as JSON in case it is an Array or Object.
    *
    * @memberof CustomElement
    * @param {?} value attribute value that should be parsed.
    * @return {Object}
    */
			deserializeValue_: function deserializeValue_(value) {
				var retVal = void 0;

				try {
					retVal = JSON.parse(value);
				} catch (e) {}

				return retVal || value;
			},

			/**
    * Disposes the Metal component and detaches event listeners. Invoked
    * once the custom element exits the document.
    *
    * @memberof CustomElement
    */
			disconnectedCallback: function disconnectedCallback() {
				this.component.off('*', this.componentEventHandler);
				this.component.dispose();
			},

			/**
    * Proxy event handler that passes event payloads from Metal component
    * events to custom element events.
    *
    * @memberof CustomElement
    * @param {?} data data emitted from Metal component event
    */
			emit: function emit() {
				for (var _len = arguments.length, data = Array(_len), _key = 0; _key < _len; _key++) {
					data[_key] = arguments[_key];
				}

				var eventData = data.pop();
				var event = new CustomEvent(eventData.type, {
					detail: data
				});
				this.dispatchEvent(event);
			}
		});

		window.customElements.define(tagName, CustomElement);
	}

	exports.default = defineWebComponent;
});
//# sourceMappingURL=define_web_component.js.map