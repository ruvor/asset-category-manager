'use strict';

Liferay.Loader.define('frontend-js-spa-web@2.0.6/liferay/init.es', ['module', 'exports', 'require', './screen/ActionURLScreen.es', './app/App.es', 'frontend-js-metal-web$metal/lib/async/async', 'frontend-js-spa-web$senna/lib/globals/globals', './screen/RenderURLScreen.es', 'frontend-js-metal-web$metal-uri/lib/Uri', 'frontend-js-spa-web$senna/lib/utils/utils', 'frontend-js-metal-web$metal-dom'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _ActionURLScreen = require('./screen/ActionURLScreen.es');

	var _ActionURLScreen2 = _interopRequireDefault(_ActionURLScreen);

	var _App = require('./app/App.es');

	var _App2 = _interopRequireDefault(_App);

	var _async = require('frontend-js-metal-web$metal/lib/async/async');

	var _async2 = _interopRequireDefault(_async);

	var _globals = require('frontend-js-spa-web$senna/lib/globals/globals');

	var _globals2 = _interopRequireDefault(_globals);

	var _RenderURLScreen = require('./screen/RenderURLScreen.es');

	var _RenderURLScreen2 = _interopRequireDefault(_RenderURLScreen);

	var _Uri = require('frontend-js-metal-web$metal-uri/lib/Uri');

	var _Uri2 = _interopRequireDefault(_Uri);

	var _utils = require('frontend-js-spa-web$senna/lib/utils/utils');

	var _utils2 = _interopRequireDefault(_utils);

	var _metalDom = require('frontend-js-metal-web$metal-dom');

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	/**
  * Initializes a Senna App with routes that match both ActionURLs and RenderURLs.
  * It also overrides Liferay's default Liferay.Util.submitForm to makes sure
  * forms are properly submitted using SPA.
  * @return {!App} The Senna App initialized
  */

	var initSPA = function initSPA() {
		var app = new _App2.default();

		app.addRoutes([{
			handler: _ActionURLScreen2.default,
			path: function path(url) {
				var match = false;

				var uri = new _Uri2.default(url);

				var loginRedirect = new _Uri2.default(Liferay.SPA.loginRedirect);

				var host = loginRedirect.getHost() || window.location.host;

				if (app.isLinkSameOrigin_(host)) {
					match = uri.getParameterValue('p_p_lifecycle') === '1';
				}

				return match;
			}
		}, {
			handler: _RenderURLScreen2.default,
			path: function path(url) {
				var match = false;

				if ((url + '/').indexOf(themeDisplay.getPathMain() + '/') !== 0) {
					var excluded = Liferay.SPA.excludedPaths.some(function (excludedPath) {
						return url.indexOf(excludedPath) === 0;
					});

					if (!excluded) {
						var uri = new _Uri2.default(url);

						var lifecycle = uri.getParameterValue('p_p_lifecycle');

						match = lifecycle === '0' || !lifecycle;
					}
				}

				return match;
			}
		}]);

		Liferay.Util.submitForm = function (form) {
			_async2.default.nextTick(function () {
				var formElement = form.getDOM();
				var formSelector = 'form' + Liferay.SPA.navigationExceptionSelectors;
				var url = formElement.action;

				if ((0, _metalDom.match)(formElement, formSelector) && app.canNavigate(url) && formElement.method !== 'get' && !app.isInPortletBlacklist(formElement)) {
					Liferay.Util._submitLocked = false;

					_globals2.default.capturedFormElement = formElement;

					var buttonSelector = 'button:not([type]),button[type=submit],input[type=submit]';

					if ((0, _metalDom.match)(_globals2.default.document.activeElement, buttonSelector)) {
						_globals2.default.capturedFormButtonElement = _globals2.default.document.activeElement;
					} else {
						_globals2.default.capturedFormButtonElement = form.one(buttonSelector);
					}

					app.navigate(_utils2.default.getUrlPath(url));
				} else {
					formElement.submit();
				}
			});
		};

		Liferay.SPA.app = app;

		Liferay.fire('SPAReady');

		return app;
	};

	exports.default = {
		init: function init(callback) {
			var _this = this;

			if (_globals2.default.document.readyState == 'loading') {
				_globals2.default.document.addEventListener('DOMContentLoaded', function () {
					callback.call(_this, initSPA());
				});
			} else {
				callback.call(this, initSPA());
			}
		}
	};
});
//# sourceMappingURL=init.es.js.map