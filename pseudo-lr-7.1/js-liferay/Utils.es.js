'use strict';

Liferay.Loader.define("frontend-js-spa-web@2.0.6/liferay/util/Utils.es", ['module', 'exports', 'require'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	var MAX_TIMEOUT = Math.pow(2, 31) - 1;

	/**
  * Utils
  *
  * A collection of utilities used by this module
  */

	var Utils = function () {
		function Utils() {
			_classCallCheck(this, Utils);
		}

		_createClass(Utils, null, [{
			key: 'getMaxTimeout',

			/**
    * Returns the maximum number allowed by the `setTimeout` function
    * @return {!Number} The number
    */

			value: function getMaxTimeout() {
				return MAX_TIMEOUT;
			}

			/**
    * Given a portletId, returns the ID of the portlet's boundary DOM element
    * @param  {!String} portletId The portlet ID
    * @return {!String} The portlet boundary ID
    */

		}, {
			key: 'getPortletBoundaryId',
			value: function getPortletBoundaryId(portletId) {
				return 'p_p_id_' + portletId + '_';
			}

			/**
    * Given an array of portlet IDs, returns an array of portlet boundary IDs
    * @param  {!Array} The collection of portletIds
    * @return {!Array} The collection of portlet boundary IDs
    */

		}, {
			key: 'getPortletBoundaryIds',
			value: function getPortletBoundaryIds(portletIds) {
				return portletIds.map(function (portletId) {
					return Utils.getPortletBoundaryId(portletId);
				});
			}

			/**
    * Destroys all rendered portlets on the page
    */

		}, {
			key: 'resetAllPortlets',
			value: function resetAllPortlets() {
				Utils.getPortletBoundaryIds(Liferay.Portlet.list).forEach(function (value, index, collection) {
					var portlet = document.querySelector('#' + value);

					if (portlet) {
						Liferay.Portlet.destroy(portlet);

						portlet.portletProcessed = false;
					}
				});

				Liferay.Portlet.readyCounter = 0;

				Liferay.destroyComponents(function (component, destroyConfig) {
					return destroyConfig.destroyOnNavigate;
				});
			}
		}]);

		return Utils;
	}();

	exports.default = Utils;
});
//# sourceMappingURL=Utils.es.js.map