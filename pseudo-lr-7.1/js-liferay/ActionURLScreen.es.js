'use strict';

Liferay.Loader.define("frontend-js-spa-web@2.0.6/liferay/screen/ActionURLScreen.es", ['module', 'exports', 'require', './EventScreen.es', 'frontend-js-metal-web$metal-uri/lib/Uri', 'frontend-js-spa-web$senna/lib/utils/utils'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _get = function get(object, property, receiver) {
		if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
			var parent = Object.getPrototypeOf(object);if (parent === null) {
				return undefined;
			} else {
				return get(parent, property, receiver);
			}
		} else if ("value" in desc) {
			return desc.value;
		} else {
			var getter = desc.get;if (getter === undefined) {
				return undefined;
			}return getter.call(receiver);
		}
	};

	var _EventScreen2 = require('./EventScreen.es');

	var _EventScreen3 = _interopRequireDefault(_EventScreen2);

	var _Uri = require("frontend-js-metal-web$metal-uri/lib/Uri");

	var _Uri2 = _interopRequireDefault(_Uri);

	var _utils = require("frontend-js-spa-web$senna/lib/utils/utils");

	var _utils2 = _interopRequireDefault(_utils);

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	/**
  * ActionURLScreen
  *
  * Inherits from {@link EventScreen|EventScreen}. The screen used for all
  * requests made to ActionURLs.
  */

	var ActionURLScreen = function (_EventScreen) {
		_inherits(ActionURLScreen, _EventScreen);

		/**
   * @inheritDoc
   */

		function ActionURLScreen() {
			_classCallCheck(this, ActionURLScreen);

			var _this = _possibleConstructorReturn(this, (ActionURLScreen.__proto__ || Object.getPrototypeOf(ActionURLScreen)).call(this));

			_this.httpMethod = 'POST';
			return _this;
		}

		/**
   * @inheritDoc
   * Ensures that an action request (form submission) redirect's final
   * URL has the lifecycle RENDER `p_p_lifecycle=0`
   * @return {!String} The request path
   */

		_createClass(ActionURLScreen, [{
			key: 'getRequestPath',
			value: function getRequestPath() {
				var requestPath = null;

				var request = this.getRequest();

				if (request) {
					var uri = new _Uri2.default(_get(ActionURLScreen.prototype.__proto__ || Object.getPrototypeOf(ActionURLScreen.prototype), 'getRequestPath', this).call(this));

					if (uri.getParameterValue('p_p_lifecycle') === '1') {
						uri.setParameterValue('p_p_lifecycle', '0');
					}

					requestPath = _utils2.default.getUrlPath(uri.toString());
				}

				return requestPath;
			}
		}]);

		return ActionURLScreen;
	}(_EventScreen3.default);

	exports.default = ActionURLScreen;
});
//# sourceMappingURL=ActionURLScreen.es.js.map