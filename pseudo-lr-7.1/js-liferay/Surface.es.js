'use strict';

Liferay.Loader.define("frontend-js-spa-web@2.0.6/liferay/surface/Surface.es", ['module', 'exports', 'require', 'frontend-js-metal-web$metal/lib/core', 'frontend-js-metal-web$metal-dom/lib/dom', 'frontend-js-spa-web$senna/lib/surface/Surface'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _get = function get(object, property, receiver) {
		if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
			var parent = Object.getPrototypeOf(object);if (parent === null) {
				return undefined;
			} else {
				return get(parent, property, receiver);
			}
		} else if ("value" in desc) {
			return desc.value;
		} else {
			var getter = desc.get;if (getter === undefined) {
				return undefined;
			}return getter.call(receiver);
		}
	};

	var _core = require("frontend-js-metal-web$metal/lib/core");

	var _core2 = _interopRequireDefault(_core);

	var _dom = require("frontend-js-metal-web$metal-dom/lib/dom");

	var _dom2 = _interopRequireDefault(_dom);

	var _Surface2 = require("frontend-js-spa-web$senna/lib/surface/Surface");

	var _Surface3 = _interopRequireDefault(_Surface2);

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	/**
  * LiferaySurface
  *
  * Inherits from Senna's Surface and calls {@link https://github.com/liferay/liferay-portal/blob/7.1.x/modules/apps/frontend-js/frontend-js-web/src/main/resources/META-INF/resources/liferay/dom_task_runner.js|Liferay.DOMTaskRunner}
  * when content is added to a Surface
  */

	var LiferaySurface = function (_Surface) {
		_inherits(LiferaySurface, _Surface);

		function LiferaySurface() {
			_classCallCheck(this, LiferaySurface);

			return _possibleConstructorReturn(this, (LiferaySurface.__proto__ || Object.getPrototypeOf(LiferaySurface)).apply(this, arguments));
		}

		_createClass(LiferaySurface, [{
			key: 'addContent',

			/**
    * @inheritDoc
    */

			value: function addContent(screenId, content) {
				if (_core2.default.isString(content)) {
					content = _dom2.default.buildFragment(content);
				}

				Liferay.DOMTaskRunner.runTasks(content);

				return _get(LiferaySurface.prototype.__proto__ || Object.getPrototypeOf(LiferaySurface.prototype), 'addContent', this).call(this, screenId, content);
			}
		}]);

		return LiferaySurface;
	}(_Surface3.default);

	exports.default = LiferaySurface;
});
//# sourceMappingURL=Surface.es.js.map