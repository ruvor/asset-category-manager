'use strict';

Liferay.Loader.define("frontend-taglib-clay$clay-alert@2.1.12/lib/ClayStripe", ['module', 'exports', 'require', './ClayAlertBase', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-web-component', 'frontend-js-metal-web$metal-soy', 'frontend-js-metal-web$metal-state', './ClayStripe.soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ClayStripe = undefined;

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  require('./ClayAlertBase');

  var _metalComponent = require("frontend-js-metal-web$metal-component");

  var _metalComponent2 = _interopRequireDefault(_metalComponent);

  var _metalWebComponent = require("frontend-js-metal-web$metal-web-component");

  var _metalWebComponent2 = _interopRequireDefault(_metalWebComponent);

  var _metalSoy = require("frontend-js-metal-web$metal-soy");

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  var _metalState = require("frontend-js-metal-web$metal-state");

  var _ClayStripeSoy = require("./ClayStripe.soy");

  var _ClayStripeSoy2 = _interopRequireDefault(_ClayStripeSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  /**
   * Metal Clay Stripe component.
   * @extends Component
   */
  var ClayStripe = function (_Component) {
    _inherits(ClayStripe, _Component);

    function ClayStripe() {
      _classCallCheck(this, ClayStripe);

      return _possibleConstructorReturn(this, (ClayStripe.__proto__ || Object.getPrototypeOf(ClayStripe)).apply(this, arguments));
    }

    _createClass(ClayStripe, [{
      key: 'attached',

      /**
       * @inheritDoc
       */
      value: function attached() {
        this.addListener('hide', this._defaultHideStripe, true);
      }

      /**
       * Hides the alert and destroy it if proceed.
       * @private
       */

    }, {
      key: '_defaultHideStripe',
      value: function _defaultHideStripe() {
        if (this.destroyOnHide) {
          this.dispose();
        }
      }

      /**
       * Continues the propagation of the hide event
       * @return {Boolean} If the event has been prevented or not.
       */

    }, {
      key: '_handleHide',
      value: function _handleHide() {
        return !this.emit('hide');
      }
    }]);

    return ClayStripe;
  }(_metalComponent2.default);

  /**
   * State definition.
   * @static
   * @type {!Object}
   */

  ClayStripe.STATE = {
    /**
     * Flag to `true` to indicate whether the alert should be closed
     * automatically with the default time.
     * @default false
     * @instance
     * @memberof ClayStripe
     * @type {?(bool|number)}
     */
    autoClose: _metalState.Config.oneOfType([_metalState.Config.bool(), _metalState.Config.number()]).value(false),

    /**
     * Flag to indicate if the alert should be destroyen when close.
     * @default false
     * @instance
     * @memberof ClayStripe
     * @type {?bool}
     */
    destroyOnHide: _metalState.Config.bool().value(false),

    /**
     * CSS classes to be applied to the element.
     * @default undefined
     * @instance
     * @memberof ClayStripe
     * @type {?(string|undefined)}
     */
    elementClasses: _metalState.Config.string(),

    /**
     * Id to be applied to the element.
     * @default undefined
     * @instance
     * @memberof ClayStripe
     * @type {?(string|undefined)}
     */
    id: _metalState.Config.string(),

    /**
     * The message of alert
     * @default undefined
     * @instance
     * @memberof ClayStripe
     * @type {!html|string}
     */
    message: _metalState.Config.any().required(),

    /**
     * The path to the SVG spritemap file containing the icons.
     * @default undefined
     * @instance
     * @memberof ClayStripe
     * @type {!string}
     */
    spritemap: _metalState.Config.string().required(),

    /**
     * The style of alert
     * @default info
     * @instance
     * @memberof ClayStripe
     * @type {?string}
     */
    style: _metalState.Config.oneOf(['danger', 'info', 'success', 'warning']).value('info'),

    /**
     * The title of alert
     * @default undefined
     * @instance
     * @memberof ClayStripe
     * @type {!string}
     */
    title: _metalState.Config.string().required()
  };

  (0, _metalWebComponent2.default)('clay-stripe', ClayStripe);

  _metalSoy2.default.register(ClayStripe, _ClayStripeSoy2.default);

  exports.ClayStripe = ClayStripe;
  exports.default = ClayStripe;
  //# sourceMappingURL=ClayStripe.js.map
});
//# sourceMappingURL=ClayStripe.js.map