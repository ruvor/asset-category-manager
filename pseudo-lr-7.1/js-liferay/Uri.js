'use strict';

Liferay.Loader.define("frontend-js-metal-web$metal-uri@2.4.0/lib/Uri", ['module', 'exports', 'require', 'frontend-js-metal-web$metal', './parse', 'frontend-js-metal-web$metal-structs'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _slicedToArray = function () {
		function sliceIterator(arr, i) {
			var _arr = [];var _n = true;var _d = false;var _e = undefined;try {
				for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
					_arr.push(_s.value);if (i && _arr.length === i) break;
				}
			} catch (err) {
				_d = true;_e = err;
			} finally {
				try {
					if (!_n && _i["return"]) _i["return"]();
				} finally {
					if (_d) throw _e;
				}
			}return _arr;
		}return function (arr, i) {
			if (Array.isArray(arr)) {
				return arr;
			} else if (Symbol.iterator in Object(arr)) {
				return sliceIterator(arr, i);
			} else {
				throw new TypeError("Invalid attempt to destructure non-iterable instance");
			}
		};
	}();

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _metal = require("frontend-js-metal-web$metal");

	var _parse = require('./parse');

	var _parse2 = _interopRequireDefault(_parse);

	var _metalStructs = require("frontend-js-metal-web$metal-structs");

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	var parseFn_ = _parse2.default;

	var Uri = function () {

		/**
   * This class contains setters and getters for the parts of the URI.
   * The following figure displays an example URIs and their component parts.
   *
   *                                  path
   *	                             ┌───┴────┐
   *	  abc://example.com:123/path/data?key=value#fragid1
   *	  └┬┘   └────┬────┘ └┬┘           └───┬───┘ └──┬──┘
   * protocol  hostname  port            search    hash
   *          └──────┬───────┘
   *                host
   *
   * @param {*=} opt_uri Optional string URI to parse
   * @constructor
   */
		function Uri() {
			var opt_uri = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

			_classCallCheck(this, Uri);

			this.url = Uri.parse(this.maybeAddProtocolAndHostname_(opt_uri));
		}

		/**
   * Adds parameters to uri from a <code>MultiMap</code> as source.
   * @param {MultiMap} multimap The <code>MultiMap</code> containing the
   *   parameters.
   * @protected
   * @chainable
   */

		_createClass(Uri, [{
			key: 'addParametersFromMultiMap',
			value: function addParametersFromMultiMap(multimap) {
				var _this = this;

				multimap.names().forEach(function (name) {
					multimap.getAll(name).forEach(function (value) {
						_this.addParameterValue(name, value);
					});
				});
				return this;
			}

			/**
    * Adds the value of the named query parameters.
    * @param {string} key The parameter to set.
    * @param {*} value The new value. Will be explicitly casted to String.
    * @chainable
    */

		}, {
			key: 'addParameterValue',
			value: function addParameterValue(name, value) {
				this.ensureQueryInitialized_();
				if ((0, _metal.isDef)(value)) {
					value = String(value);
				}
				this.query.add(name, value);
				return this;
			}

			/**
    * Adds the values of the named query parameter.
    * @param {string} key The parameter to set.
    * @param {*} value The new value.
    * @chainable
    */

		}, {
			key: 'addParameterValues',
			value: function addParameterValues(name, values) {
				var _this2 = this;

				values.forEach(function (value) {
					return _this2.addParameterValue(name, value);
				});
				return this;
			}

			/**
    * Ensures query internal map is initialized and synced with initial value
    * extracted from URI search part.
    * @protected
    */

		}, {
			key: 'ensureQueryInitialized_',
			value: function ensureQueryInitialized_() {
				var _this3 = this;

				if (this.query) {
					return;
				}
				this.query = new _metalStructs.MultiMap();
				var search = this.url.search;
				if (search) {
					search.substring(1).split('&').forEach(function (param) {
						var _param$split = param.split('='),
						    _param$split2 = _slicedToArray(_param$split, 2),
						    key = _param$split2[0],
						    value = _param$split2[1];

						if ((0, _metal.isDef)(value)) {
							value = Uri.urlDecode(value);
						}
						_this3.addParameterValue(key, value);
					});
				}
			}

			/**
    * Gets the hash part of uri.
    * @return {string}
    */

		}, {
			key: 'getHash',
			value: function getHash() {
				return this.url.hash || '';
			}

			/**
    * Gets the host part of uri. E.g. <code>[hostname]:[port]</code>.
    * @return {string}
    */

		}, {
			key: 'getHost',
			value: function getHost() {
				var host = this.getHostname();
				if (host) {
					var port = this.getPort();
					if (port && port !== '80') {
						host += ':' + port;
					}
				}
				return host;
			}

			/**
    * Gets the hostname part of uri without protocol and port.
    * @return {string}
    */

		}, {
			key: 'getHostname',
			value: function getHostname() {
				var hostname = this.url.hostname;
				if (hostname === Uri.HOSTNAME_PLACEHOLDER) {
					return '';
				}
				return hostname;
			}

			/**
    * Gets the origin part of uri. E.g. <code>http://[hostname]:[port]</code>.
    * @return {string}
    */

		}, {
			key: 'getOrigin',
			value: function getOrigin() {
				var host = this.getHost();
				if (host) {
					return this.getProtocol() + '//' + host;
				}
				return '';
			}

			/**
    * Returns the first value for a given parameter or undefined if the given
    * parameter name does not appear in the query string.
    * @param {string} paramName Unescaped parameter name.
    * @return {string|undefined} The first value for a given parameter or
    *   undefined if the given parameter name does not appear in the query
    *   string.
    */

		}, {
			key: 'getParameterValue',
			value: function getParameterValue(name) {
				this.ensureQueryInitialized_();
				return this.query.get(name);
			}

			/**
    * Returns the value<b>s</b> for a given parameter as a list of decoded
    * query parameter values.
    * @param {string} name The parameter to get values for.
    * @return {!Array<?>} The values for a given parameter as a list of decoded
    *   query parameter values.
    */

		}, {
			key: 'getParameterValues',
			value: function getParameterValues(name) {
				this.ensureQueryInitialized_();
				return this.query.getAll(name);
			}

			/**
    * Returns the name<b>s</b> of the parameters.
    * @return {!Array<string>} The names for the parameters as a list of
    *   strings.
    */

		}, {
			key: 'getParameterNames',
			value: function getParameterNames() {
				this.ensureQueryInitialized_();
				return this.query.names();
			}

			/**
    * Gets the function currently being used to parse URIs.
    * @return {!function()}
    */

		}, {
			key: 'getPathname',

			/**
    * Gets the pathname part of uri.
    * @return {string}
    */
			value: function getPathname() {
				return this.url.pathname;
			}

			/**
    * Gets the port number part of uri as string.
    * @return {string}
    */

		}, {
			key: 'getPort',
			value: function getPort() {
				return this.url.port;
			}

			/**
    * Gets the protocol part of uri. E.g. <code>http:</code>.
    * @return {string}
    */

		}, {
			key: 'getProtocol',
			value: function getProtocol() {
				return this.url.protocol;
			}

			/**
    * Gets the search part of uri. Search value is retrieved from query
    * parameters.
    * @return {string}
    */

		}, {
			key: 'getSearch',
			value: function getSearch() {
				var _this4 = this;

				var search = '';
				var querystring = '';
				this.getParameterNames().forEach(function (name) {
					_this4.getParameterValues(name).forEach(function (value) {
						querystring += name;
						if ((0, _metal.isDef)(value)) {
							querystring += '=' + encodeURIComponent(value);
						}
						querystring += '&';
					});
				});
				querystring = querystring.slice(0, -1);
				if (querystring) {
					search += '?' + querystring;
				}
				return search;
			}

			/**
    * Checks if uri contains the parameter.
    * @param {string} name
    * @return {boolean}
    */

		}, {
			key: 'hasParameter',
			value: function hasParameter(name) {
				this.ensureQueryInitialized_();
				return this.query.contains(name);
			}

			/**
    * Returns true if the default protocol (scheme) was added to the original Uri.
    * @return {boolean} True if a protocol (scheme) was added to the Url, false
    *   otherwise
    */

		}, {
			key: 'isUsingDefaultProtocol',
			value: function isUsingDefaultProtocol() {
				return this.usingDefaultProtocol_;
			}

			/**
    * Makes this URL unique by adding a random param to it. Useful for avoiding
    * cache.
    */

		}, {
			key: 'makeUnique',
			value: function makeUnique() {
				this.setParameterValue(Uri.RANDOM_PARAM, _metal.string.getRandomString());
				return this;
			}

			/**
    * Maybe adds protocol and a hostname placeholder on a partial URI if needed.
    * Relevant for compatibility with <code>URL</code> native object.
    * @param {string=} opt_uri
    * @return {string} URI with protocol and hostname placeholder.
    */

		}, {
			key: 'maybeAddProtocolAndHostname_',
			value: function maybeAddProtocolAndHostname_(opt_uri) {
				var url = opt_uri;
				if (opt_uri.indexOf('://') === -1 && opt_uri.indexOf('javascript:') !== 0) {
					// jshint ignore:line

					url = Uri.DEFAULT_PROTOCOL;
					this.usingDefaultProtocol_ = true;

					if (opt_uri[0] !== '/' || opt_uri[1] !== '/') {
						url += '//';
					}

					switch (opt_uri.charAt(0)) {
						case '.':
						case '?':
						case '#':
							url += Uri.HOSTNAME_PLACEHOLDER;
							url += '/';
							url += opt_uri;
							break;
						case '':
						case '/':
							if (opt_uri[1] !== '/') {
								url += Uri.HOSTNAME_PLACEHOLDER;
							}
							url += opt_uri;
							break;
						default:
							url += opt_uri;
					}
				} else {
					this.usingDefaultProtocol_ = false;
				}
				return url;
			}

			/**
    * Parses the given uri string into an object.
    * @param {*=} opt_uri Optional string URI to parse
    */

		}, {
			key: 'removeParameter',

			/**
    * Removes the named query parameter.
    * @param {string} name The parameter to remove.
    * @chainable
    */
			value: function removeParameter(name) {
				this.ensureQueryInitialized_();
				this.query.remove(name);
				return this;
			}

			/**
    * Removes uniqueness parameter of the uri.
    * @chainable
    */

		}, {
			key: 'removeUnique',
			value: function removeUnique() {
				this.removeParameter(Uri.RANDOM_PARAM);
				return this;
			}

			/**
    * Sets the hash.
    * @param {string} hash
    * @chainable
    */

		}, {
			key: 'setHash',
			value: function setHash(hash) {
				this.url.hash = hash;
				return this;
			}

			/**
    * Sets the hostname.
    * @param {string} hostname
    * @chainable
    */

		}, {
			key: 'setHostname',
			value: function setHostname(hostname) {
				this.url.hostname = hostname;
				return this;
			}

			/**
    * Sets the value of the named query parameters, clearing previous values
    * for that key.
    * @param {string} key The parameter to set.
    * @param {*} value The new value.
    * @chainable
    */

		}, {
			key: 'setParameterValue',
			value: function setParameterValue(name, value) {
				this.removeParameter(name);
				this.addParameterValue(name, value);
				return this;
			}

			/**
    * Sets the values of the named query parameters, clearing previous values
    * for that key.
    * @param {string} key The parameter to set.
    * @param {*} value The new value.
    * @chainable
    */

		}, {
			key: 'setParameterValues',
			value: function setParameterValues(name, values) {
				var _this5 = this;

				this.removeParameter(name);
				values.forEach(function (value) {
					return _this5.addParameterValue(name, value);
				});
				return this;
			}

			/**
    * Sets the pathname.
    * @param {string} pathname
    * @chainable
    */

		}, {
			key: 'setPathname',
			value: function setPathname(pathname) {
				this.url.pathname = pathname;
				return this;
			}

			/**
    * Sets the port number.
    * @param {*} port Port number.
    * @chainable
    */

		}, {
			key: 'setPort',
			value: function setPort(port) {
				this.url.port = port;
				return this;
			}

			/**
    * Sets the function that will be used for parsing the original string uri
    * into an object.
    * @param {!function()} parseFn
    */

		}, {
			key: 'setProtocol',

			/**
    * Sets the protocol. If missing <code>http:</code> is used as default.
    * @param {string} protocol
    * @chainable
    */
			value: function setProtocol(protocol) {
				this.url.protocol = protocol;
				if (this.url.protocol[this.url.protocol.length - 1] !== ':') {
					this.url.protocol += ':';
				}
				return this;
			}

			/**
    * @return {string} The string form of the url.
    * @override
    */

		}, {
			key: 'toString',
			value: function toString() {
				var href = '';
				var host = this.getHost();
				if (host) {
					href += this.getProtocol() + '//';
				}
				href += host + this.getPathname() + this.getSearch() + this.getHash();
				return href;
			}

			/**
    * Joins the given paths.
    * @param {string} basePath
    * @param {...string} ...paths Any number of paths to be joined with the base url.
    * @static
    */

		}], [{
			key: 'getParseFn',
			value: function getParseFn() {
				return parseFn_;
			}
		}, {
			key: 'parse',
			value: function parse(opt_uri) {
				return parseFn_(opt_uri);
			}
		}, {
			key: 'setParseFn',
			value: function setParseFn(parseFn) {
				parseFn_ = parseFn;
			}
		}, {
			key: 'joinPaths',
			value: function joinPaths(basePath) {
				for (var _len = arguments.length, paths = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					paths[_key - 1] = arguments[_key];
				}

				if (basePath.charAt(basePath.length - 1) === '/') {
					basePath = basePath.substring(0, basePath.length - 1);
				}
				paths = paths.map(function (path) {
					return path.charAt(0) === '/' ? path.substring(1) : path;
				});
				return [basePath].concat(paths).join('/').replace(/\/$/, '');
			}

			/**
    * URL-decodes the string. We need to specially handle '+'s because
    * the javascript library doesn't convert them to spaces.
    * @param {string} str The string to url decode.
    * @return {string} The decoded {@code str}.
    */

		}, {
			key: 'urlDecode',
			value: function urlDecode(str) {
				return decodeURIComponent(str.replace(/\+/g, ' '));
			}
		}]);

		return Uri;
	}();

	/**
  * Default protocol value.
  * @type {string}
  * @default http:
  * @static
  */

	var isSecure = function isSecure() {
		return typeof window !== 'undefined' && window.location && window.location.protocol && window.location.protocol.indexOf('https') === 0;
	};

	Uri.DEFAULT_PROTOCOL = isSecure() ? 'https:' : 'http:';

	/**
  * Hostname placeholder. Relevant to internal usage only.
  * @type {string}
  * @static
  */
	Uri.HOSTNAME_PLACEHOLDER = 'hostname' + Date.now();

	/**
  * Name used by the param generated by `makeUnique`.
  * @type {string}
  * @static
  */
	Uri.RANDOM_PARAM = 'zx';

	exports.default = Uri;
});
//# sourceMappingURL=Uri.js.map