'use strict';

Liferay.Loader.define("frontend-taglib-clay$clay-tooltip@2.1.12/lib/ClayTooltip", ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-soy', 'frontend-js-metal-web$metal-position', 'frontend-js-metal-web$metal-state', 'frontend-js-metal-web$metal-events', 'frontend-js-metal-web$metal', 'frontend-js-metal-web$metal-dom', './ClayTooltip.soy'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.ClayTooltip = undefined;

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _metalComponent = require("frontend-js-metal-web$metal-component");

	var _metalComponent2 = _interopRequireDefault(_metalComponent);

	var _metalSoy = require("frontend-js-metal-web$metal-soy");

	var _metalSoy2 = _interopRequireDefault(_metalSoy);

	var _metalPosition = require("frontend-js-metal-web$metal-position");

	var _metalState = require("frontend-js-metal-web$metal-state");

	var _metalEvents = require("frontend-js-metal-web$metal-events");

	var _metal = require("frontend-js-metal-web$metal");

	var _metalDom = require("frontend-js-metal-web$metal-dom");

	var _ClayTooltipSoy = require("./ClayTooltip.soy");

	var _ClayTooltipSoy2 = _interopRequireDefault(_ClayTooltipSoy);

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _defineProperty(obj, key, value) {
		if (key in obj) {
			Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
		} else {
			obj[key] = value;
		}return obj;
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	/**
  * Singleton enforcer class
  */
	var SingletonEnforcer = function SingletonEnforcer() {
		_classCallCheck(this, SingletonEnforcer);
	};

	/**
  * Implementation of ClayTooltip.
  * @extends Component
  */

	var ClayTooltip = function (_Component) {
		_inherits(ClayTooltip, _Component);

		/**
   * @inheritDoc
   */
		function ClayTooltip(enforcer, config, parentElement) {
			_classCallCheck(this, ClayTooltip);

			if (!enforcer) {
				throw new Error('Use ClayTooltip.init to initialize ClayTooltip');
			}
			return _possibleConstructorReturn(this, (ClayTooltip.__proto__ || Object.getPrototypeOf(ClayTooltip)).call(this, config, parentElement));
		}

		/**
   * Returns the instance for this ClayTooltip.
   * @memberof ClayTooltip
   * @param {Object=} config An object with the initial values for this
   *                         component's state.
   * @param {boolean|string|Element=} parentElement The element where the
   * component should be rendered. Can be given as a selector or an element.
   * If `false` is passed, the component won't be rendered automatically
   * after created.
   * @static
   * @return {ClayTooltip}
   */

		_createClass(ClayTooltip, [{
			key: 'created',

			/**
    * @inheritDoc
    */
			value: function created() {
				this._eventHandler = new _metalEvents.EventHandler();
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'attached',
			value: function attached() {
				this.addListener('transitionend', this._handleTransitionEnd, true);
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'rendered',
			value: function rendered() {
				if (this._target) {
					var alignedPosition = _metalPosition.Align.align(this.element, this._target, this.position);

					if (this.alignedPosition !== alignedPosition) {
						this.alignedPosition = alignedPosition;
					}
				}
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'detached',
			value: function detached() {
				this._eventHandler.removeAllListeners();
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'disposed',
			value: function disposed() {
				this._eventHandler.removeAllListeners();
			}

			/**
    * Handles mouseenter event.
    * @memberof ClayTooltip
    * @param {!Element} element
    * @return {!String}
    * @private
    */

		}, {
			key: '_getContent',
			value: function _getContent(element) {
				var titleAttribute = element.getAttribute('title');

				if (titleAttribute) {
					element.setAttribute('data-title', titleAttribute);
					element.setAttribute('data-restore-title', 'true');
					element.removeAttribute('title');
				} else if (element.tagName === 'svg') {
					var titleTag = element.querySelector('title');

					if (titleTag) {
						element.setAttribute('data-title', titleTag.innerHTML);
						element.setAttribute('data-restore-title', 'true');
						titleTag.remove();
					}
				}

				return element.getAttribute('data-title');
			}

			/**
    * Handles click event.
    * @memberof ClayTooltip
    * @param {!Event} event
    * @private
    */

		}, {
			key: '_handleMouseClick',
			value: function _handleMouseClick(event) {
				this._restoreTitle(event.delegateTarget);

				this._isTransitioning = true;
				this.visible = false;
			}

			/**
    * Handles mouseenter event.
    * @memberof ClayTooltip
    * @param {!Event} event
    * @private
    */

		}, {
			key: '_handleMouseEnter',
			value: function _handleMouseEnter(event) {
				var content = this._getContent(event.delegateTarget);
				this._target = event.delegateTarget;

				this._content = content;

				if (!this.visible) {
					this.element.style.display = 'block';
				}
				this._isTransitioning = true;
				this.visible = true;
			}

			/**
    * Handles tooltip element mouseenter event.
    * @memberof ClayTooltip
    * @private
    */

		}, {
			key: '_handleMouseEnterTooltip',
			value: function _handleMouseEnterTooltip() {
				if (this._isTransitioning) {
					this.visible = true;
				}
			}

			/**
    * Handles mouseleave events.
    * @memberof ClayTooltip
    * @param {!Event} event
    * @private
    */

		}, {
			key: '_handleMouseLeave',
			value: function _handleMouseLeave(event) {
				if (this.visible) {
					this._restoreTitle(event.delegateTarget);

					this._isTransitioning = true;
					this.visible = false;
				}
			}

			/**
    * Handles transionend event.
    * @memberof ClayTooltip
    * @private
    */

		}, {
			key: '_handleTransitionEnd',
			value: function _handleTransitionEnd() {
				this._isTransitioning = false;

				if (!this.visible) {
					this.element.style.display = 'none';
				}
			}

			/**
    * Restores the title attribute to an element
    * @memberof ClayTooltip
    * @param {Element} element
    * @private
    */

		}, {
			key: '_restoreTitle',
			value: function _restoreTitle(element) {
				var title = element.getAttribute('data-title');
				var restoreTitle = element.getAttribute('data-restore-title');

				if (title && restoreTitle === 'true') {
					if (element.tagName === 'svg') {
						var titleTag = document.createElement('title');
						titleTag.innerHTML = title;

						element.appendChild(titleTag);
					} else {
						element.setAttribute('title', title);
					}

					element.removeAttribute('data-restore-title');
				}
			}

			/**
    * The setter function for the `classMap` staet.
    * @memberof ClayTooltip
    * @param {Object} val
    * @return {!Object}
    * @private
    */

		}, {
			key: 'setterClassMapFn_',
			value: function setterClassMapFn_(val) {
				return _metal.object.mixin(this.valueClassMapFn_(), val);
			}

			/**
    * Sync selectors
    * @memberof ClayTooltip
    * @param {Array.<string>} newValue  The new value of `this.selectors`.
    * @param {Array.<string>} prevValue The previous value of `this.selectors`.
    * @private
    */

		}, {
			key: 'syncSelectors',
			value: function syncSelectors(newValue, prevValue) {
				if (newValue) {
					var prevSelectors = Array.isArray(prevValue) ? prevValue : [];

					var newSelectors = newValue.reduce(function (arr, val) {
						if (arr.indexOf(val) === -1) {
							arr.push(val);
						}
						return arr;
					}, prevSelectors);

					this._eventHandler.removeAllListeners();

					for (var i = 0, l = newSelectors.length; i < l; i++) {
						var selector = newSelectors[i];

						this._eventHandler.add(_metalDom.dom.delegate(document, 'blur', selector, this._handleMouseLeave.bind(this)), _metalDom.dom.delegate(document, 'click', selector, this._handleMouseClick.bind(this)), _metalDom.dom.delegate(document, 'focus', selector, this._handleMouseEnter.bind(this)), _metalDom.dom.delegate(document, 'mouseenter', selector, this._handleMouseEnter.bind(this)), _metalDom.dom.delegate(document, 'mouseleave', selector, this._handleMouseLeave.bind(this)));
					}
				}
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'syncVisible',
			value: function syncVisible() {}
			// This is needed to make fade transition work


			/**
    * Gets the default value for the `classMap` state.
    * @return {!Object}
    * @private
    */

		}, {
			key: 'valueClassMapFn_',
			value: function valueClassMapFn_() {
				var _ref;

				return _ref = {}, _defineProperty(_ref, _metalPosition.Align.TopLeft, 'clay-tooltip-top-left'), _defineProperty(_ref, _metalPosition.Align.TopCenter, 'clay-tooltip-top'), _defineProperty(_ref, _metalPosition.Align.TopRight, 'clay-tooltip-top-right'), _defineProperty(_ref, _metalPosition.Align.BottomLeft, 'clay-tooltip-bottom-left'), _defineProperty(_ref, _metalPosition.Align.BottomCenter, 'clay-tooltip-bottom'), _defineProperty(_ref, _metalPosition.Align.BottomRight, 'clay-tooltip-bottom-right'), _defineProperty(_ref, _metalPosition.Align.RightCenter, 'clay-tooltip-right'), _defineProperty(_ref, _metalPosition.Align.LeftCenter, 'clay-tooltip-left'), _ref;
			}
		}], [{
			key: 'init',
			value: function init(config, parentElement) {
				if (!this._instance) {
					this._instance = new ClayTooltip(new SingletonEnforcer(), config, parentElement);
				}
				return this._instance;
			}
		}]);

		return ClayTooltip;
	}(_metalComponent2.default);

	/**
  * State definition.
  * @static
  * @type {!Object}
  */

	ClayTooltip.STATE = {
		/**
   * Content of the tooltip.
   * @default undefined
   * @instance
   * @memberof ClayTooltip
   * @type {!(html|string)}
   */
		_content: _metalState.Config.any().value('').internal(),

		/**
   * The current position of the tooltip after being aligned via `Align.align`.
   * @default undefined
   * @type {number}
   * @instance
   * @memberof ClayTooltip
   */
		alignedPosition: {
			validator: _metalPosition.Align.isValidPosition
		},

		/**
   * Element classes.
   * @default undefined
   * @instance
   * @memberof ClayTooltip
   * @type {string}
   */
		elementClasses: _metalState.Config.string(),

		/**
   * A map from `Align` position constants to the CSS class that should be
   * added to the tooltip when it's aligned in that position.
   * @instance
   * @memberof ClayTooltip
   * @type {!Object}
   */
		classMap: {
			setter: 'setterClassMapFn_',
			validator: _metal.core.isObject,
			valueFn: 'valueClassMapFn_'
		},

		/**
   * Tooltip position
   * @default Align.Bottom
   * @instance
   * @memberof ClayTooltip
   * @type {!string}
   */
		position: {
			validator: _metalPosition.Align.isValidPosition,
			value: _metalPosition.Align.Bottom
		},

		/**
   * Selectors
   * @default ['[data-title]', '[title]']
   * @instance
   * @memberof ClayTooltip
   * @type {!Array.<string>}
   */
		selectors: _metalState.Config.array().value(['[data-title]', '[title]']),

		/**
   * Tooltip visible when show is called.
   * @default false
   * @instance
   * @memberof ClayTooltip
   * @private
   * @type {?bool}
   */
		visible: _metalState.Config.bool().value(false)
	};

	_metalSoy2.default.register(ClayTooltip, _ClayTooltipSoy2.default);

	exports.ClayTooltip = ClayTooltip;
	exports.default = ClayTooltip;
	//# sourceMappingURL=ClayTooltip.js.map
});
//# sourceMappingURL=ClayTooltip.js.map