'use strict';

Liferay.Loader.define('frontend-taglib-clay$clay-icon@2.1.12/lib/ClayIcon.soy', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.templates = exports.ClayIcon = undefined;

  var _metalComponent = require('frontend-js-metal-web$metal-component');

  var _metalComponent2 = _interopRequireDefault(_metalComponent);

  var _metalSoy = require('frontend-js-metal-web$metal-soy');

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  } /* jshint ignore:start */

  var templates;
  goog.loadModule(function (exports) {
    var soy = goog.require('soy');
    var soydata = goog.require('soydata');
    // This file was automatically generated from ClayIcon.soy.
    // Please don't edit this file by hand.

    /**
     * @fileoverview Templates in namespace ClayIcon.
     * @public
     */

    goog.module('ClayIcon.incrementaldom');

    goog.require('goog.soy.data.SanitizedContent');
    var incrementalDom = goog.require('incrementaldom');
    goog.require('soy.asserts');
    var soyIdom = goog.require('soy.idom');

    /**
     * @param {$render.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $render = function $render(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var spritemap = soy.asserts.assertType(goog.isString(opt_data.spritemap) || opt_data.spritemap instanceof goog.soy.data.SanitizedContent, 'spritemap', opt_data.spritemap, '!goog.soy.data.SanitizedContent|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var symbol = soy.asserts.assertType(goog.isString(opt_data.symbol) || opt_data.symbol instanceof goog.soy.data.SanitizedContent, 'symbol', opt_data.symbol, '!goog.soy.data.SanitizedContent|string');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var elementClasses = soy.asserts.assertType(opt_data.elementClasses == null || goog.isString(opt_data.elementClasses) || opt_data.elementClasses instanceof goog.soy.data.SanitizedContent, 'elementClasses', opt_data.elementClasses, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {boolean|null|undefined} */
      var focusable = soy.asserts.assertType(opt_data.focusable == null || goog.isBoolean(opt_data.focusable) || opt_data.focusable === 1 || opt_data.focusable === 0, 'focusable', opt_data.focusable, 'boolean|null|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var id = soy.asserts.assertType(opt_data.id == null || goog.isString(opt_data.id) || opt_data.id instanceof goog.soy.data.SanitizedContent, 'id', opt_data.id, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var title = soy.asserts.assertType(opt_data.title == null || goog.isString(opt_data.title) || opt_data.title instanceof goog.soy.data.SanitizedContent, 'title', opt_data.title, '!goog.soy.data.SanitizedContent|null|string|undefined');
      var attributes__soy31 = function attributes__soy31() {
        incrementalDom.attr('aria-hidden', 'true');
        incrementalDom.attr('class', 'lexicon-icon lexicon-icon-' + symbol + (elementClasses ? ' ' + elementClasses : ''));
        if (focusable) {
          incrementalDom.attr('focusable', 'true');
        } else {
          incrementalDom.attr('focusable', 'false');
        }
        if (id) {
          incrementalDom.attr('id', id);
        }
      };
      incrementalDom.elementOpenStart('svg');
      attributes__soy31();
      incrementalDom.elementOpenEnd();
      incrementalDom.elementOpen('title');
      soyIdom.print(title != null ? title : id != null ? id : symbol);
      incrementalDom.elementClose('title');
      incrementalDom.elementOpenStart('use');
      incrementalDom.attr('xlink:href', spritemap + '#' + symbol);
      incrementalDom.elementOpenEnd();
      incrementalDom.elementClose('use');
      incrementalDom.elementClose('svg');
    };
    exports.render = $render;
    /**
     * @typedef {{
     *  spritemap: (!goog.soy.data.SanitizedContent|string),
     *  symbol: (!goog.soy.data.SanitizedContent|string),
     *  elementClasses: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  focusable: (boolean|null|undefined),
     *  id: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  title: (!goog.soy.data.SanitizedContent|null|string|undefined),
     * }}
     */
    $render.Params;
    if (goog.DEBUG) {
      $render.soyTemplateName = 'ClayIcon.render';
    }

    exports.render.params = ["spritemap", "symbol", "elementClasses", "focusable", "id", "title"];
    exports.render.types = { "spritemap": "string", "symbol": "string", "elementClasses": "string", "focusable": "bool", "id": "string", "title": "string" };
    exports.templates = templates = exports;
    return exports;
  });

  var ClayIcon = function (_Component) {
    _inherits(ClayIcon, _Component);

    function ClayIcon() {
      _classCallCheck(this, ClayIcon);

      return _possibleConstructorReturn(this, (ClayIcon.__proto__ || Object.getPrototypeOf(ClayIcon)).apply(this, arguments));
    }

    return ClayIcon;
  }(_metalComponent2.default);

  _metalSoy2.default.register(ClayIcon, templates);
  exports.ClayIcon = ClayIcon;
  exports.templates = templates;
  exports.default = templates;
  /* jshint ignore:end */
  //# sourceMappingURL=ClayIcon.soy.js.map
});
//# sourceMappingURL=ClayIcon.soy.js.map