'use strict';

Liferay.Loader.define('frontend-taglib-clay$clay-tooltip@2.1.12/lib/ClayTooltip.soy', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.templates = exports.ClayTooltip = undefined;

  var _metalComponent = require('frontend-js-metal-web$metal-component');

  var _metalComponent2 = _interopRequireDefault(_metalComponent);

  var _metalSoy = require('frontend-js-metal-web$metal-soy');

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  } /* jshint ignore:start */

  var templates;
  goog.loadModule(function (exports) {
    var soy = goog.require('soy');
    var soydata = goog.require('soydata');
    // This file was automatically generated from ClayTooltip.soy.
    // Please don't edit this file by hand.

    /**
     * @fileoverview Templates in namespace ClayTooltip.
     * @public
     */

    goog.module('ClayTooltip.incrementaldom');

    goog.require('goog.soy.data.SanitizedContent');
    var incrementalDom = goog.require('incrementaldom');
    goog.require('soy.asserts');
    var soyIdom = goog.require('soy.idom');

    /**
     * @param {$render.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $render = function $render(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      opt_data = opt_data || {};
      /** @type {!goog.soy.data.SanitizedContent|function()|null|string|undefined} */
      var _content = soy.asserts.assertType(opt_data._content == null || goog.isFunction(opt_data._content) || goog.isString(opt_data._content) || opt_data._content instanceof goog.soy.data.SanitizedContent, '_content', opt_data._content, '!goog.soy.data.SanitizedContent|function()|null|string|undefined');
      /** @type {null|number|undefined} */
      var alignedPosition = soy.asserts.assertType(opt_data.alignedPosition == null || goog.isNumber(opt_data.alignedPosition), 'alignedPosition', opt_data.alignedPosition, 'null|number|undefined');
      /** @type {?} */
      var classMap = opt_data.classMap;
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var elementClasses = soy.asserts.assertType(opt_data.elementClasses == null || goog.isString(opt_data.elementClasses) || opt_data.elementClasses instanceof goog.soy.data.SanitizedContent, 'elementClasses', opt_data.elementClasses, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {null|number|undefined} */
      var position = soy.asserts.assertType(opt_data.position == null || goog.isNumber(opt_data.position), 'position', opt_data.position, 'null|number|undefined');
      /** @type {boolean|null|undefined} */
      var visible = soy.asserts.assertType(opt_data.visible == null || goog.isBoolean(opt_data.visible) || opt_data.visible === 1 || opt_data.visible === 0, 'visible', opt_data.visible, 'boolean|null|undefined');
      var classes__soy10 = classMap ? classMap : ['clay-tooltip-top-left', 'clay-tooltip-top', 'clay-tooltip-top-right', 'clay-tooltip-bottom-left', 'clay-tooltip-bottom', 'clay-tooltip-bottom-right', 'clay-tooltip-right', 'clay-tooltip-left'];
      var currentPosition__soy12 = alignedPosition != null ? alignedPosition : position;
      var positionClass__soy14 = currentPosition__soy12 != null ? classes__soy10[currentPosition__soy12] : 'clay-tooltip-bottom';
      var tooltipAttributes__soy33 = function tooltipAttributes__soy33() {
        incrementalDom.attr('class', 'tooltip fade ' + positionClass__soy14 + (elementClasses ? ' ' + elementClasses : '') + (visible ? ' show' : ''));
        incrementalDom.attr('data-onmouseenter', '_handleMouseEnterTooltip');
        incrementalDom.attr('data-onmouseleave', '_handleMouseLeave');
        if (!visible) {
          incrementalDom.attr('hidden', '');
        }
        incrementalDom.attr('role', 'tooltip');
      };
      incrementalDom.elementOpenStart('div');
      tooltipAttributes__soy33();
      incrementalDom.elementOpenEnd();
      incrementalDom.elementOpenStart('div');
      incrementalDom.attr('class', 'arrow');
      incrementalDom.elementOpenEnd();
      incrementalDom.elementClose('div');
      incrementalDom.elementOpenStart('div');
      incrementalDom.attr('class', 'tooltip-inner');
      incrementalDom.elementOpenEnd();
      soyIdom.print(_content);
      incrementalDom.elementClose('div');
      incrementalDom.elementClose('div');
    };
    exports.render = $render;
    /**
     * @typedef {{
     *  _content: (!goog.soy.data.SanitizedContent|function()|null|string|undefined),
     *  alignedPosition: (null|number|undefined),
     *  classMap: (?|undefined),
     *  elementClasses: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  position: (null|number|undefined),
     *  visible: (boolean|null|undefined),
     * }}
     */
    $render.Params;
    if (goog.DEBUG) {
      $render.soyTemplateName = 'ClayTooltip.render';
    }

    exports.render.params = ["_content", "alignedPosition", "classMap", "elementClasses", "position", "visible"];
    exports.render.types = { "_content": "html|string", "alignedPosition": "number", "classMap": "?", "elementClasses": "string", "position": "number", "visible": "bool" };
    exports.templates = templates = exports;
    return exports;
  });

  var ClayTooltip = function (_Component) {
    _inherits(ClayTooltip, _Component);

    function ClayTooltip() {
      _classCallCheck(this, ClayTooltip);

      return _possibleConstructorReturn(this, (ClayTooltip.__proto__ || Object.getPrototypeOf(ClayTooltip)).apply(this, arguments));
    }

    return ClayTooltip;
  }(_metalComponent2.default);

  _metalSoy2.default.register(ClayTooltip, templates);
  exports.ClayTooltip = ClayTooltip;
  exports.templates = templates;
  exports.default = templates;
  /* jshint ignore:end */
  //# sourceMappingURL=ClayTooltip.soy.js.map
});
//# sourceMappingURL=ClayTooltip.soy.js.map