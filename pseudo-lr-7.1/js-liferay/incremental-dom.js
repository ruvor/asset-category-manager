'use strict';

Liferay.Loader.define('frontend-js-metal-web$metal-incremental-dom@2.16.5/lib/incremental-dom', ['module', 'exports', 'require', 'frontend-js-metal-web$incremental-dom', 'frontend-js-metal-web$incremental-dom-string', 'frontend-js-metal-web$metal'], function (module, exports, require) {
	var define = undefined;
	var _incrementalDom = require('frontend-js-metal-web$incremental-dom');

	var IncrementalDOM = _interopRequireWildcard(_incrementalDom);

	var _incrementalDomString = require('frontend-js-metal-web$incremental-dom-string');

	var IncrementalDOMString = _interopRequireWildcard(_incrementalDomString);

	var _metal = require('frontend-js-metal-web$metal');

	function _interopRequireWildcard(obj) {
		if (obj && obj.__esModule) {
			return obj;
		} else {
			var newObj = {};if (obj != null) {
				for (var key in obj) {
					if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
				}
			}newObj.default = obj;return newObj;
		}
	}

	if ((0, _metal.isServerSide)()) {
		// Overrides global.IncrementalDOM virtual elements with incremental dom
		// string implementation for server side rendering. At the moment it does not
		// override for Node.js tests since tests are using jsdom to simulate the
		// browser.
		global.IncrementalDOM = IncrementalDOMString;
	} else {
		var scope = typeof exports !== 'undefined' && typeof global !== 'undefined' ? global : window;

		if (!scope.IncrementalDOM) {
			scope.IncrementalDOM = IncrementalDOM;
		}
	}
});
//# sourceMappingURL=incremental-dom.js.map