'use strict';

Liferay.Loader.define('frontend-js-metal-web$metal-uri@2.4.0/node', ['module', 'exports', 'require', './lib/Uri', 'frontend-js-node-shims$path', 'frontend-js-node-shims$url'], function (module, exports, require) {
	var define = undefined;
	var Uri = require('./lib/Uri').default;

	if (typeof URL === 'undefined' && typeof require !== 'undefined') {
		// If there is no "document", then this should be running in NodeJS or in ReactNative env and
		// in this case we should use the "url" NPM module as the parse function.
		// In ReactNative env "path" will be replaced with "path-browserify".

		var path = require('frontend-js-node-shims$path');
		var url = require('frontend-js-node-shims$url');

		Uri.setParseFn(function (urlStr) {
			var parsed = url.parse(urlStr);
			parsed.pathname = path.normalize(parsed.pathname);
			return parsed;
		});
	}

	module.exports = Uri;
});
//# sourceMappingURL=node.js.map