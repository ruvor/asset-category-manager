'use strict';

Liferay.Loader.define("frontend-taglib-clay$clay-alert@2.1.12/lib/ClayAlertBase", ['module', 'exports', 'require', 'frontend-taglib-clay$clay-button', 'frontend-taglib-clay$clay-icon', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-web-component', 'frontend-js-metal-web$metal-soy', 'frontend-js-metal-web$metal-state', 'frontend-js-metal-web$metal', './ClayAlertBase.soy'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.ClayAlertBase = undefined;

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	require("frontend-taglib-clay$clay-button");

	require("frontend-taglib-clay$clay-icon");

	var _metalComponent = require("frontend-js-metal-web$metal-component");

	var _metalComponent2 = _interopRequireDefault(_metalComponent);

	var _metalWebComponent = require("frontend-js-metal-web$metal-web-component");

	var _metalWebComponent2 = _interopRequireDefault(_metalWebComponent);

	var _metalSoy = require("frontend-js-metal-web$metal-soy");

	var _metalSoy2 = _interopRequireDefault(_metalSoy);

	var _metalState = require("frontend-js-metal-web$metal-state");

	var _metal = require("frontend-js-metal-web$metal");

	var _ClayAlertBaseSoy = require("./ClayAlertBase.soy");

	var _ClayAlertBaseSoy2 = _interopRequireDefault(_ClayAlertBaseSoy);

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	/**
  * Metal Clay Alert Base component.
  * @extends Component
  */
	var ClayAlertBase = function (_Component) {
		_inherits(ClayAlertBase, _Component);

		function ClayAlertBase() {
			_classCallCheck(this, ClayAlertBase);

			return _possibleConstructorReturn(this, (ClayAlertBase.__proto__ || Object.getPrototypeOf(ClayAlertBase)).apply(this, arguments));
		}

		_createClass(ClayAlertBase, [{
			key: 'attached',

			/**
    * @inheritDoc
    */
			value: function attached() {
				this.addListener('hide', this._defaultHideAlert, true);
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'disposed',
			value: function disposed() {
				if (this._timer) {
					clearTimeout(this._timer);
					this._timer = undefined;
				}
				this._timeToDisappear = undefined;
				this._startedTime = undefined;
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'rendered',
			value: function rendered(firstRender) {
				if (firstRender && !(0, _metal.isServerSide)()) {
					this._startTimer();
				}
			}

			/**
    * Hides the alert and destroy it if proceed.
    * @private
    */

		}, {
			key: '_defaultHideAlert',
			value: function _defaultHideAlert() {
				if (!this.isDisposed()) {
					this._delayTime = 0;
					this._visible = false;

					if (this._timer) {
						clearTimeout(this._timer);
					}

					if (this.destroyOnHide) {
						this.dispose();
					}
				}
			}

			/**
    * Handles onclick event for the close button in case of closeable alert.
    * @private
    */

		}, {
			key: '_handleCloseClick',
			value: function _handleCloseClick() {
				this.close();
			}

			/**
    * Handles mouseot event for the alert.
    * @private
    */

		}, {
			key: '_handleMouseOut',
			value: function _handleMouseOut() {
				this._resumeTimeout();
			}

			/**
    * Handles mouseover event for the alert.
    * @private
    */

		}, {
			key: '_handleMouseOver',
			value: function _handleMouseOver() {
				this._pauseTimeout();
			}

			/**
    * Pauses the closing delay time.
    * @private
    */

		}, {
			key: '_pauseTimeout',
			value: function _pauseTimeout() {
				if (this._timer) {
					clearTimeout(this._timer);
					this._timer = undefined;
					this._timeToDisappear -= new Date() - this._startedTime;
				}
			}

			/**
    * Resumes the closing delay time.
    * @private
    */

		}, {
			key: '_resumeTimeout',
			value: function _resumeTimeout() {
				var _this2 = this;

				if (this._timeToDisappear > 0) {
					this._startedTime = new Date();
					this._timer = setTimeout(function () {
						_this2.close();
					}, this._timeToDisappear);
				}
			}

			/**
    * Sets the delayTime if passed, if it does not set the default, and starts.
    * @private
    */

		}, {
			key: '_startTimer',
			value: function _startTimer() {
				if (this.autoClose) {
					this._timeToDisappear = this.autoClose * 1000;

					this._resumeTimeout();
				}
			}

			/**
    * Emits hide alert.
    * @private
    */

		}, {
			key: 'close',
			value: function close() {
				this.emit('hide');
			}
		}]);

		return ClayAlertBase;
	}(_metalComponent2.default);

	/**
  * State definition.
  * @static
  * @type {!Object}
  */

	ClayAlertBase.STATE = {
		/**
   * Flag to indicate the visibility of the alert
   * @default true
   * @instance
   * @memberof ClayAlertBase
   * @type {?bool}
   */
		_visible: _metalState.Config.bool().internal().value(true),

		/**
   * Set the duration time to auto close the alert.
   * @default undfined
   * @instance
   * @memberof ClayAlertBase
   * @type {?number}
   */
		autoClose: _metalState.Config.number(),

		/**
   * Flag to indicate if the alert is closeable.
   * @default false
   * @instance
   * @memberof ClayAlertBase
   * @type {?bool}
   */
		closeable: _metalState.Config.bool().value(false),

		/**
   * Flag to indicate if the alert should be destroyen when close.
   * @default false
   * @instance
   * @memberof ClayAlertBase
   * @type {?bool}
   */
		destroyOnHide: _metalState.Config.bool().value(false),

		/**
   * CSS classes to be applied to the element.
   * @default undefined
   * @instance
   * @memberof ClayAlert
   * @type {?(string|undefined)}
   */
		elementClasses: _metalState.Config.string(),

		/**
   * Id to be applied to the element.
   * @default undefined
   * @instance
   * @memberof ClayAlert
   * @type {?(string|undefined)}
   */
		id: _metalState.Config.string(),

		/**
   * The message of alert
   * @default undefined
   * @instance
   * @memberof ClayAlertBase
   * @type {!(html|string)}
   */
		message: _metalState.Config.any().required(),

		/**
   * The path to the SVG spritemap file containing the icons.
   * @default undefined
   * @instance
   * @memberof ClayAlertBase
   * @type {!string}
   */
		spritemap: _metalState.Config.string().required(),

		/**
   * The style of alert
   * @default info
   * @instance
   * @memberof ClayAlertBase
   * @type {?string}
   */
		style: _metalState.Config.oneOf(['danger', 'info', 'success', 'warning']).value('info'),

		/**
   * The title of alert
   * @default undefined
   * @instance
   * @memberof ClayAlertBase
   * @type {!string}
   */
		title: _metalState.Config.string().required(),

		/**
   * The type of alert
   * @default embedded
   * @instance
   * @memberof ClayAlertBase
   * @type {?string}
   */
		type: _metalState.Config.oneOf(['embedded', 'stripe', 'toast']).value('embedded')
	};

	(0, _metalWebComponent2.default)('clay-alert-base', ClayAlertBase);

	_metalSoy2.default.register(ClayAlertBase, _ClayAlertBaseSoy2.default);

	exports.ClayAlertBase = ClayAlertBase;
	exports.default = ClayAlertBase;
	//# sourceMappingURL=ClayAlertBase.js.map
});
//# sourceMappingURL=ClayAlertBase.js.map