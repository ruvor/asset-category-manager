'use strict';

Liferay.Loader.define('frontend-taglib-clay$clay-button@2.1.12/lib/ClayButton.soy', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.templates = exports.ClayButton = undefined;

  var _metalComponent = require('frontend-js-metal-web$metal-component');

  var _metalComponent2 = _interopRequireDefault(_metalComponent);

  var _metalSoy = require('frontend-js-metal-web$metal-soy');

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  } /* jshint ignore:start */

  var templates;
  goog.loadModule(function (exports) {
    var soy = goog.require('soy');
    var soydata = goog.require('soydata');
    // This file was automatically generated from ClayButton.soy.
    // Please don't edit this file by hand.

    /**
     * @fileoverview Templates in namespace ClayButton.
     * @public
     */

    goog.module('ClayButton.incrementaldom');

    goog.require('goog.soy.data.SanitizedContent');
    var incrementalDom = goog.require('incrementaldom');
    goog.require('soy.asserts');
    var soyIdom = goog.require('soy.idom');

    var $templateAlias1 = _metalSoy2.default.getTemplate('ClayIcon.incrementaldom', 'render');

    /**
     * @param {$render.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $render = function $render(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      opt_data = opt_data || {};
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var ariaLabel = soy.asserts.assertType(opt_data.ariaLabel == null || goog.isString(opt_data.ariaLabel) || opt_data.ariaLabel instanceof goog.soy.data.SanitizedContent, 'ariaLabel', opt_data.ariaLabel, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {boolean|null|undefined} */
      var block = soy.asserts.assertType(opt_data.block == null || goog.isBoolean(opt_data.block) || opt_data.block === 1 || opt_data.block === 0, 'block', opt_data.block, 'boolean|null|undefined');
      /** @type {boolean|null|undefined} */
      var disabled = soy.asserts.assertType(opt_data.disabled == null || goog.isBoolean(opt_data.disabled) || opt_data.disabled === 1 || opt_data.disabled === 0, 'disabled', opt_data.disabled, 'boolean|null|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var elementClasses = soy.asserts.assertType(opt_data.elementClasses == null || goog.isString(opt_data.elementClasses) || opt_data.elementClasses instanceof goog.soy.data.SanitizedContent, 'elementClasses', opt_data.elementClasses, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var icon = soy.asserts.assertType(opt_data.icon == null || goog.isString(opt_data.icon) || opt_data.icon instanceof goog.soy.data.SanitizedContent, 'icon', opt_data.icon, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var iconAlignment = soy.asserts.assertType(opt_data.iconAlignment == null || goog.isString(opt_data.iconAlignment) || opt_data.iconAlignment instanceof goog.soy.data.SanitizedContent, 'iconAlignment', opt_data.iconAlignment, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var id = soy.asserts.assertType(opt_data.id == null || goog.isString(opt_data.id) || opt_data.id instanceof goog.soy.data.SanitizedContent, 'id', opt_data.id, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|function()|null|string|undefined} */
      var label = soy.asserts.assertType(opt_data.label == null || goog.isFunction(opt_data.label) || goog.isString(opt_data.label) || opt_data.label instanceof goog.soy.data.SanitizedContent, 'label', opt_data.label, '!goog.soy.data.SanitizedContent|function()|null|string|undefined');
      /** @type {boolean|null|undefined} */
      var monospaced = soy.asserts.assertType(opt_data.monospaced == null || goog.isBoolean(opt_data.monospaced) || opt_data.monospaced === 1 || opt_data.monospaced === 0, 'monospaced', opt_data.monospaced, 'boolean|null|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var name = soy.asserts.assertType(opt_data.name == null || goog.isString(opt_data.name) || opt_data.name instanceof goog.soy.data.SanitizedContent, 'name', opt_data.name, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var size = soy.asserts.assertType(opt_data.size == null || goog.isString(opt_data.size) || opt_data.size instanceof goog.soy.data.SanitizedContent, 'size', opt_data.size, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var spritemap = soy.asserts.assertType(opt_data.spritemap == null || goog.isString(opt_data.spritemap) || opt_data.spritemap instanceof goog.soy.data.SanitizedContent, 'spritemap', opt_data.spritemap, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var style = soy.asserts.assertType(opt_data.style == null || goog.isString(opt_data.style) || opt_data.style instanceof goog.soy.data.SanitizedContent, 'style', opt_data.style, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var title = soy.asserts.assertType(opt_data.title == null || goog.isString(opt_data.title) || opt_data.title instanceof goog.soy.data.SanitizedContent, 'title', opt_data.title, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var type = soy.asserts.assertType(opt_data.type == null || goog.isString(opt_data.type) || opt_data.type instanceof goog.soy.data.SanitizedContent, 'type', opt_data.type, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var value = soy.asserts.assertType(opt_data.value == null || goog.isString(opt_data.value) || opt_data.value instanceof goog.soy.data.SanitizedContent, 'value', opt_data.value, '!goog.soy.data.SanitizedContent|null|string|undefined');
      var attributes__soy93 = function attributes__soy93() {
        incrementalDom.attr('class', 'btn' + (block ? ' btn-block' : '') + (elementClasses ? ' ' + elementClasses : '') + (monospaced ? ' btn-monospaced' : '') + (size ? ' btn-' + size : '') + (style ? ' btn-' + style : ' btn-primary'));
        if (ariaLabel) {
          incrementalDom.attr('aria-label', ariaLabel);
        } else if (label) {
          incrementalDom.attr('aria-label', label);
        } else if (icon) {
          incrementalDom.attr('aria-label', icon);
        }
        if (disabled) {
          incrementalDom.attr('disabled', 'disabled');
        }
        if (id) {
          incrementalDom.attr('id', id);
        }
        if (name) {
          incrementalDom.attr('name', name);
        }
        if (title) {
          incrementalDom.attr('title', title);
        }
        if (value) {
          incrementalDom.attr('value', value);
        }
        incrementalDom.attr('type', type);
      };
      incrementalDom.elementOpenStart('button');
      attributes__soy93();
      incrementalDom.elementOpenEnd();
      $content({ icon: icon, iconAlignment: iconAlignment != null ? iconAlignment : 'left', label: label, spritemap: spritemap }, opt_ijData);
      incrementalDom.elementClose('button');
    };
    exports.render = $render;
    /**
     * @typedef {{
     *  ariaLabel: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  block: (boolean|null|undefined),
     *  disabled: (boolean|null|undefined),
     *  elementClasses: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  icon: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  iconAlignment: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  id: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  label: (!goog.soy.data.SanitizedContent|function()|null|string|undefined),
     *  monospaced: (boolean|null|undefined),
     *  name: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  size: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  spritemap: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  style: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  title: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  type: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  value: (!goog.soy.data.SanitizedContent|null|string|undefined),
     * }}
     */
    $render.Params;
    if (goog.DEBUG) {
      $render.soyTemplateName = 'ClayButton.render';
    }

    /**
     * @param {$content.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $content = function $content(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      opt_data = opt_data || {};
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var icon = soy.asserts.assertType(opt_data.icon == null || goog.isString(opt_data.icon) || opt_data.icon instanceof goog.soy.data.SanitizedContent, 'icon', opt_data.icon, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var iconAlignment = soy.asserts.assertType(opt_data.iconAlignment == null || goog.isString(opt_data.iconAlignment) || opt_data.iconAlignment instanceof goog.soy.data.SanitizedContent, 'iconAlignment', opt_data.iconAlignment, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|function()|null|string|undefined} */
      var label = soy.asserts.assertType(opt_data.label == null || goog.isFunction(opt_data.label) || goog.isString(opt_data.label) || opt_data.label instanceof goog.soy.data.SanitizedContent, 'label', opt_data.label, '!goog.soy.data.SanitizedContent|function()|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var spritemap = soy.asserts.assertType(opt_data.spritemap == null || goog.isString(opt_data.spritemap) || opt_data.spritemap instanceof goog.soy.data.SanitizedContent, 'spritemap', opt_data.spritemap, '!goog.soy.data.SanitizedContent|null|string|undefined');
      var iconContent__soy120 = function iconContent__soy120() {
        if (icon && spritemap) {
          $icon({ icon: icon, iconAlignment: iconAlignment, label: label, spritemap: spritemap }, opt_ijData);
        }
      };
      if (iconContent__soy120 != '' && iconAlignment == 'left') {
        iconContent__soy120();
      }
      soyIdom.print(label != null ? label : '');
      if (iconContent__soy120 != '' && iconAlignment == 'right') {
        iconContent__soy120();
      }
    };
    exports.content = $content;
    /**
     * @typedef {{
     *  icon: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  iconAlignment: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  label: (!goog.soy.data.SanitizedContent|function()|null|string|undefined),
     *  spritemap: (!goog.soy.data.SanitizedContent|null|string|undefined),
     * }}
     */
    $content.Params;
    if (goog.DEBUG) {
      $content.soyTemplateName = 'ClayButton.content';
    }

    /**
     * @param {$icon.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $icon = function $icon(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var icon = soy.asserts.assertType(goog.isString(opt_data.icon) || opt_data.icon instanceof goog.soy.data.SanitizedContent, 'icon', opt_data.icon, '!goog.soy.data.SanitizedContent|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var spritemap = soy.asserts.assertType(goog.isString(opt_data.spritemap) || opt_data.spritemap instanceof goog.soy.data.SanitizedContent, 'spritemap', opt_data.spritemap, '!goog.soy.data.SanitizedContent|string');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var iconAlignment = soy.asserts.assertType(opt_data.iconAlignment == null || goog.isString(opt_data.iconAlignment) || opt_data.iconAlignment instanceof goog.soy.data.SanitizedContent, 'iconAlignment', opt_data.iconAlignment, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|function()|null|string|undefined} */
      var label = soy.asserts.assertType(opt_data.label == null || goog.isFunction(opt_data.label) || goog.isString(opt_data.label) || opt_data.label instanceof goog.soy.data.SanitizedContent, 'label', opt_data.label, '!goog.soy.data.SanitizedContent|function()|null|string|undefined');
      var iconContent__soy147 = function iconContent__soy147() {
        $templateAlias1({ spritemap: spritemap, symbol: icon }, opt_ijData);
      };
      if (label) {
        var iconWrapperClasses__soy159 = '';
        iconWrapperClasses__soy159 += 'inline-item';
        var $tmp;
        if (iconAlignment == 'left') {
          $tmp = ' inline-item-before';
        } else if (iconAlignment == 'right') {
          $tmp = ' inline-item-after';
        } else {
          $tmp = '';
        }
        iconWrapperClasses__soy159 += $tmp;
        incrementalDom.elementOpenStart('span');
        incrementalDom.attr('class', iconWrapperClasses__soy159);
        incrementalDom.elementOpenEnd();
        iconContent__soy147();
        incrementalDom.elementClose('span');
      } else {
        iconContent__soy147();
      }
    };
    exports.icon = $icon;
    /**
     * @typedef {{
     *  icon: (!goog.soy.data.SanitizedContent|string),
     *  spritemap: (!goog.soy.data.SanitizedContent|string),
     *  iconAlignment: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  label: (!goog.soy.data.SanitizedContent|function()|null|string|undefined),
     * }}
     */
    $icon.Params;
    if (goog.DEBUG) {
      $icon.soyTemplateName = 'ClayButton.icon';
    }

    exports.render.params = ["ariaLabel", "block", "disabled", "elementClasses", "icon", "iconAlignment", "id", "label", "monospaced", "name", "size", "spritemap", "style", "title", "type", "value"];
    exports.render.types = { "ariaLabel": "string", "block": "bool", "disabled": "bool", "elementClasses": "string", "icon": "string", "iconAlignment": "string", "id": "string", "label": "html|string", "monospaced": "bool", "name": "string", "size": "string", "spritemap": "string", "style": "string", "title": "string", "type": "string", "value": "string" };
    exports.content.params = ["icon", "iconAlignment", "label", "spritemap"];
    exports.content.types = { "icon": "string", "iconAlignment": "string", "label": "html|string", "spritemap": "string" };
    exports.icon.params = ["icon", "spritemap", "iconAlignment", "label"];
    exports.icon.types = { "icon": "string", "spritemap": "string", "iconAlignment": "string", "label": "html|string" };
    exports.templates = templates = exports;
    return exports;
  });

  var ClayButton = function (_Component) {
    _inherits(ClayButton, _Component);

    function ClayButton() {
      _classCallCheck(this, ClayButton);

      return _possibleConstructorReturn(this, (ClayButton.__proto__ || Object.getPrototypeOf(ClayButton)).apply(this, arguments));
    }

    return ClayButton;
  }(_metalComponent2.default);

  _metalSoy2.default.register(ClayButton, templates);
  exports.ClayButton = ClayButton;
  exports.templates = templates;
  exports.default = templates;
  /* jshint ignore:end */
  //# sourceMappingURL=ClayButton.soy.js.map
});
//# sourceMappingURL=ClayButton.soy.js.map