'use strict';

Liferay.Loader.define("frontend-taglib-clay$clay-component@2.1.12/lib/ClayComponent", ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-state', 'frontend-js-metal-web$metal-dom', 'frontend-js-metal-web$metal'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.ClayComponent = undefined;

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _get = function get(object, property, receiver) {
		if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
			var parent = Object.getPrototypeOf(object);if (parent === null) {
				return undefined;
			} else {
				return get(parent, property, receiver);
			}
		} else if ("value" in desc) {
			return desc.value;
		} else {
			var getter = desc.get;if (getter === undefined) {
				return undefined;
			}return getter.call(receiver);
		}
	};

	var _metalComponent = require("frontend-js-metal-web$metal-component");

	var _metalComponent2 = _interopRequireDefault(_metalComponent);

	var _metalState = require("frontend-js-metal-web$metal-state");

	var _metalDom = require("frontend-js-metal-web$metal-dom");

	var _metal = require("frontend-js-metal-web$metal");

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	/**
  * Clay Component.
  * @extends Component
  */
	var ClayComponent = function (_Component) {
		_inherits(ClayComponent, _Component);

		function ClayComponent() {
			_classCallCheck(this, ClayComponent);

			return _possibleConstructorReturn(this, (ClayComponent.__proto__ || Object.getPrototypeOf(ClayComponent)).apply(this, arguments));
		}

		_createClass(ClayComponent, [{
			key: 'attached',

			/**
    * @inheritDoc
    */
			value: function attached() {
				var _get2,
				    _this2 = this;

				for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
					args[_key] = arguments[_key];
				}

				(_get2 = _get(ClayComponent.prototype.__proto__ || Object.getPrototypeOf(ClayComponent.prototype), 'attached', this)).call.apply(_get2, [this].concat(args));

				if ((0, _metal.isServerSide)()) {
					return;
				}

				var getAttribute = this.element.getAttribute.bind(this.element);

				this.element.getAttribute = function (attributeName) {
					var attributeValue = getAttribute(attributeName);

					if (_this2.element && !attributeValue) {
						attributeValue = _metalDom.domData.get(_this2.element, attributeName);
					}

					return _metal.core.isDefAndNotNull(attributeValue) ? attributeValue : '';
				};

				for (var dataKey in this.data) {
					if (this.data.hasOwnProperty(dataKey)) {
						_metalDom.domData.get(this.element, 'data-' + dataKey, this.data[dataKey]);
					}
				}
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'rendered',
			value: function rendered() {
				var _get3;

				for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
					args[_key2] = arguments[_key2];
				}

				(_get3 = _get(ClayComponent.prototype.__proto__ || Object.getPrototypeOf(ClayComponent.prototype), 'rendered', this)).call.apply(_get3, [this].concat(args));

				for (var dataKey in this.data) {
					if (Object.prototype.hasOwnProperty.call(this.data, dataKey)) {
						this.element.setAttribute('data-' + dataKey, this.data[dataKey]);
					}
				}
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'buildFacade_',
			value: function buildFacade_(eventName, data, originalEvent) {
				if (this.getShouldUseFacade()) {
					var facade = {
						data: data,
						preventDefault: function preventDefault() {
							facade.preventedDefault = true;
							if (originalEvent && originalEvent.preventDefault && (!originalEvent.preventDefault || !originalEvent.defaultPrevented)) {
								originalEvent.preventDefault();
							}
						},
						stopInmediatePropagation: function stopInmediatePropagation() {
							if (originalEvent && originalEvent.stopInmediatePropagation) {
								originalEvent.stopInmediatePropagation();
							}
						},
						stopPropagation: function stopPropagation() {
							if (originalEvent && originalEvent.stopPropagation) {
								originalEvent.stopPropagation();
							}
						},
						target: this,
						type: eventName
					};

					return facade;
				}
			}

			/**
    * Execute each of the listeners in order with te supplied arguments.
    * @param {string|object} event
    * @param {*} opt_args [arg1], [arg2], [...]
    * @return {boolean} Returns true if event had listeners, false otherwise.
    */

		}, {
			key: 'emit',
			value: function emit(event) {
				for (var _len3 = arguments.length, args = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
					args[_key3 - 1] = arguments[_key3];
				}

				var eventName = (0, _metal.isObject)(event) ? event.name : event;
				var facade = this.buildFacade_(eventName, event.data, event.originalEvent);

				args = (0, _metal.isObject)(event) ? [facade] : args;

				var listeners = this.getRawListeners_(eventName);

				if (listeners.length === 0) {
					return false;
				}

				this.runListeners_(listeners, args, facade);
				return true;
			}
		}]);

		return ClayComponent;
	}(_metalComponent2.default);

	/**
  * State definition.
  * @static
  * @type {!Object}
  */

	ClayComponent.STATE = {
		/**
   * Data to add to the element.
   * @default undefined
   * @instance
   * @memberof ClayComponent
   * @type {?object}
   */
		data: _metalState.Config.object()
	};

	exports.ClayComponent = ClayComponent;
	exports.default = ClayComponent;
	//# sourceMappingURL=ClayComponent.js.map
});
//# sourceMappingURL=ClayComponent.js.map