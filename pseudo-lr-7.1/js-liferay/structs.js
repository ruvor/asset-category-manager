'use strict';

Liferay.Loader.define('frontend-js-metal-web$metal-structs@1.0.1/lib/all/structs', ['module', 'exports', 'require', '../MultiMap', '../TreeNode'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.TreeNode = exports.MultiMap = undefined;

  var _MultiMap = require('../MultiMap');

  var _MultiMap2 = _interopRequireDefault(_MultiMap);

  var _TreeNode = require('../TreeNode');

  var _TreeNode2 = _interopRequireDefault(_TreeNode);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  exports.MultiMap = _MultiMap2.default;
  exports.TreeNode = _TreeNode2.default;
});
//# sourceMappingURL=structs.js.map