'use strict';

Liferay.Loader.define("frontend-js-spa-web@2.0.6/liferay/screen/EventScreen.es", ['module', 'exports', 'require', 'frontend-js-spa-web$senna/lib/screen/HtmlScreen', 'frontend-js-spa-web$senna/lib/globals/globals', 'frontend-js-metal-web$metal-promise/lib/promise/Promise'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _get = function get(object, property, receiver) {
		if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
			var parent = Object.getPrototypeOf(object);if (parent === null) {
				return undefined;
			} else {
				return get(parent, property, receiver);
			}
		} else if ("value" in desc) {
			return desc.value;
		} else {
			var getter = desc.get;if (getter === undefined) {
				return undefined;
			}return getter.call(receiver);
		}
	};

	var _HtmlScreen2 = require("frontend-js-spa-web$senna/lib/screen/HtmlScreen");

	var _HtmlScreen3 = _interopRequireDefault(_HtmlScreen2);

	var _globals = require("frontend-js-spa-web$senna/lib/globals/globals");

	var _globals2 = _interopRequireDefault(_globals);

	var _Promise = require("frontend-js-metal-web$metal-promise/lib/promise/Promise");

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	/**
  * EventScreen
  *
  * Inherits from Senna's `HtmlScreen`. It performs logic that is
  * common to both {@link ActionURLScreen|ActionURLScreen} and
  * {@link RenderURLScreen|RenderURLScreen}.
  */

	var EventScreen = function (_HtmlScreen) {
		_inherits(EventScreen, _HtmlScreen);

		/**
   * @inheritDoc
   */

		function EventScreen() {
			_classCallCheck(this, EventScreen);

			var _this = _possibleConstructorReturn(this, (EventScreen.__proto__ || Object.getPrototypeOf(EventScreen)).call(this));

			_this.cacheable = false;
			_this.timeout = Liferay.SPA.app.timeout;
			return _this;
		}

		/**
   * @inheritDoc
   * Exposes the `screenDispose` event to the Liferay global object
   */

		_createClass(EventScreen, [{
			key: 'dispose',
			value: function dispose() {
				_get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'dispose', this).call(this);

				Liferay.fire('screenDispose', {
					app: Liferay.SPA.app,
					screen: this
				});
			}

			/**
    * @inheritDoc
    * Exposes the `screenActivate` event to the Liferay global object
    */

		}, {
			key: 'activate',
			value: function activate() {
				_get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'activate', this).call(this);

				Liferay.fire('screenActivate', {
					app: Liferay.SPA.app,
					screen: this
				});
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'addCache',
			value: function addCache(content) {
				_get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'addCache', this).call(this, content);

				this.cacheLastModified = new Date().getTime();
			}

			/**
    * Attempts a regular navigation to the given path, if a form is not being
    * submitted and the redirect Path can't be matched to a known route
    * @param  {!String} redirectPath The path to check
    */

		}, {
			key: 'checkRedirectPath',
			value: function checkRedirectPath(redirectPath) {
				var app = Liferay.SPA.app;

				if (!_globals2.default.capturedFormElement && !app.findRoute(redirectPath)) {
					window.location.href = redirectPath;
				}
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'deactivate',
			value: function deactivate() {
				_get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'deactivate', this).call(this);

				Liferay.fire('screenDeactivate', {
					app: Liferay.SPA.app,
					screen: this
				});
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'beforeScreenFlip',
			value: function beforeScreenFlip() {
				Liferay.fire('beforeScreenFlip', {
					app: Liferay.SPA.app,
					screen: this
				});
			}

			/**
    * Copies the classes and onload event from the virtual document to the actual
    * document on the page
    */

		}, {
			key: 'copyBodyAttributes',
			value: function copyBodyAttributes() {
				var virtualBody = this.virtualDocument.querySelector('body');

				document.body.className = virtualBody.className;
				document.body.onload = virtualBody.onload;
			}

			/**
    * @inheritDoc
    * Temporarily makes all permanent styles temporary when a language change is
    * detected, so that they are disposed, re-downloaded, and re-parsed before
    * the screen flips. This is important because the content of the portal and
    * theme styles are dynamic and may depend on the displayed language.
    * Right-to-left (RTL) languages, for instance, have diffrent styles.
    * @param  {!Array} surfaces The surfaces to evaluate styles from
    */

		}, {
			key: 'evaluateStyles',
			value: function evaluateStyles(surfaces) {
				var currentLanguageId = document.querySelector('html').lang.replace('-', '_');
				var languageId = this.virtualDocument.lang.replace('-', '_');

				if (currentLanguageId !== languageId) {
					this.stylesPermanentSelector_ = _HtmlScreen3.default.selectors.stylesPermanent;
					this.stylesTemporarySelector_ = _HtmlScreen3.default.selectors.stylesTemporary;

					this.makePermanentSelectorsTemporary_(currentLanguageId, languageId);
				}

				return _get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'evaluateStyles', this).call(this, surfaces).then(this.restoreSelectors_.bind(this));
			}

			/**
    * @inheritDoc
    * Adds the `beforeScreenFlip` event to the lifecycle and exposes the
    * `screenFlip` event to the Liferay global object
    * @param  {!Array} surfaces The surfaces to flip
    */

		}, {
			key: 'flip',
			value: function flip(surfaces) {
				var _this2 = this;

				this.copyBodyAttributes();

				return _Promise.CancellablePromise.resolve(this.beforeScreenFlip()).then(_get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'flip', this).call(this, surfaces)).then(function () {
					_this2.runBodyOnLoad();

					Liferay.fire('screenFlip', {
						app: Liferay.SPA.app,
						screen: _this2
					});
				});
			}

			/**
    * @inheritDoc
    * Returns the cache if it's not expired or if the cache
    * feature is not disabled
    * @return {!String} The cache contents
    */

		}, {
			key: 'getCache',
			value: function getCache() {
				var cache = null;

				var app = Liferay.SPA.app;

				if (app.isCacheEnabled() && !app.isScreenCacheExpired(this)) {
					cache = _get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'getCache', this).call(this);
				}

				return cache;
			}

			/**
    * Returns the timestamp the cache was last modified
    * @return {!Number} `cacheLastModified` time
    */

		}, {
			key: 'getCacheLastModified',
			value: function getCacheLastModified() {
				return this.cacheLastModified;
			}

			/**
    * Returns whether a given status code is considered valid
    * @param  {!Number} The status code to check
    * @return {!Boolean} True if the given status code is valid
    */

		}, {
			key: 'isValidResponseStatusCode',
			value: function isValidResponseStatusCode(statusCode) {
				var validStatusCodes = Liferay.SPA.app.getValidStatusCodes();

				return statusCode >= 200 && statusCode <= 500 || validStatusCodes.indexOf(statusCode) > -1;
			}

			/**
    * @inheritDoc
    * @return {!String} The cache contents
    */

		}, {
			key: 'load',
			value: function load(path) {
				var _this3 = this;

				return _get(EventScreen.prototype.__proto__ || Object.getPrototypeOf(EventScreen.prototype), 'load', this).call(this, path).then(function (content) {
					var redirectPath = _this3.beforeUpdateHistoryPath(path);

					_this3.checkRedirectPath(redirectPath);

					Liferay.fire('screenLoad', {
						app: Liferay.SPA.app,
						content: content,
						screen: _this3
					});

					return content;
				});
			}

			/**
    * The method used by {@link EventScreen#evaluateStyles|evaluateStyles}. This
    * changes the static properties `HtmlScreen.selectors.stylesTemporary` and
    * `HtmlScreen.selectors.stylesPermanent` temporarily. The action can be
    * undone by {@link EventScreen#restoreSelectors_|restoreSelectors_}
    * @param  {!String} currentLanguageId
    * @param  {!String} languageId
    */

		}, {
			key: 'makePermanentSelectorsTemporary_',
			value: function makePermanentSelectorsTemporary_(currentLanguageId, languageId) {
				_HtmlScreen3.default.selectors.stylesTemporary = _HtmlScreen3.default.selectors.stylesTemporary.split(',').concat(_HtmlScreen3.default.selectors.stylesPermanent.split(',').map(function (item) {
					return item + '[href*="' + currentLanguageId + '"]';
				})).join();

				_HtmlScreen3.default.selectors.stylesPermanent = _HtmlScreen3.default.selectors.stylesPermanent.split(',').map(function (item) {
					return item + '[href*="' + languageId + '"]';
				}).join();
			}

			/**
    * The method used by {@link EventScreen#evaluateStyles|evaluateStyles}. This
    * restores the permanent and temporary selectors changed by
    * {@link EventScreen#makePermanentSelectorsTemporary_|makePermanentSelectorsTemporary_}.
    */

		}, {
			key: 'restoreSelectors_',
			value: function restoreSelectors_() {
				_HtmlScreen3.default.selectors.stylesPermanent = this.stylesPermanentSelector_ || _HtmlScreen3.default.selectors.stylesPermanent;
				_HtmlScreen3.default.selectors.stylesTemporary = this.stylesTemporarySelector_ || _HtmlScreen3.default.selectors.stylesTemporary;
			}

			/**
    * Executes the `document.body.onload` event every time a navigation occurs
    */

		}, {
			key: 'runBodyOnLoad',
			value: function runBodyOnLoad() {
				var onLoad = document.body.onload;

				if (onLoad) {
					onLoad();
				}
			}
		}]);

		return EventScreen;
	}(_HtmlScreen3.default);

	exports.default = EventScreen;
});
//# sourceMappingURL=EventScreen.es.js.map