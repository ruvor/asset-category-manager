'use strict';

Liferay.Loader.define('frontend-taglib-clay$clay-button@2.1.12/lib/ClayButton', ['module', 'exports', 'require', 'frontend-taglib-clay$clay-icon', 'frontend-taglib-clay$clay-component', 'frontend-js-metal-web$metal-web-component', 'frontend-js-metal-web$metal-soy', 'frontend-js-metal-web$metal-state', './ClayButton.soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ClayButton = undefined;

  require('frontend-taglib-clay$clay-icon');

  var _clayComponent = require('frontend-taglib-clay$clay-component');

  var _clayComponent2 = _interopRequireDefault(_clayComponent);

  var _metalWebComponent = require('frontend-js-metal-web$metal-web-component');

  var _metalWebComponent2 = _interopRequireDefault(_metalWebComponent);

  var _metalSoy = require('frontend-js-metal-web$metal-soy');

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  var _metalState = require('frontend-js-metal-web$metal-state');

  var _ClayButtonSoy = require('./ClayButton.soy');

  var _ClayButtonSoy2 = _interopRequireDefault(_ClayButtonSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  /**
   * Metal Clay Button component.
   * @extends ClayComponent
   */
  var ClayButton = function (_ClayComponent) {
    _inherits(ClayButton, _ClayComponent);

    function ClayButton() {
      _classCallCheck(this, ClayButton);

      return _possibleConstructorReturn(this, (ClayButton.__proto__ || Object.getPrototypeOf(ClayButton)).apply(this, arguments));
    }

    return ClayButton;
  }(_clayComponent2.default);

  /**
   * State definition.
   * @static
   * @type {!Object}
   */

  ClayButton.STATE = {
    /**
     * Aria label attribute for the button element.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    ariaLabel: _metalState.Config.string(),

    /**
     * Renders the button as a block element.
     * @default false
     * @instance
     * @memberof ClayButton
     * @type {?boolean}
     */
    block: _metalState.Config.bool().value(false),

    /**
     * Data to add to the element.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?object}
     */
    data: _metalState.Config.object(),

    /**
     * The button disabled attribute.
     * @default false
     * @instance
     * @memberof ClayButton
     * @type {?boolean}
     */
    disabled: _metalState.Config.bool().value(false),

    /**
     * CSS classes to be applied to the element.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    elementClasses: _metalState.Config.string(),

    /**
     * Icon to be rendered in the button.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    icon: _metalState.Config.string(),

    /**
     * Render ClayIcon in the ClayButton element.
     * @default left
     * @instance
     * @memberof ClayButton
     * @type {?string}
     */
    iconAlignment: _metalState.Config.oneOf(['left', 'right']).value('left'),

    /**
     * Id to be applied to the element.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    id: _metalState.Config.string(),

    /**
     * The label of the button content.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(html|string|undefined)}
     */
    label: _metalState.Config.any(),

    /**
     * Flag to indicate if button should be monospaced.
     * @default false
     * @instance
     * @memberof ClayButton
     * @type {?bool}
     */
    monospaced: _metalState.Config.bool().value(false),

    /**
     * The name attribute value of the element.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    name: _metalState.Config.string(),

    /**
     * The name that will make the CSS class name that will represent the
     * button size.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    size: _metalState.Config.oneOf(['sm']),

    /**
     * The path to the SVG spritemap file containing the icons.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    spritemap: _metalState.Config.string(),

    /**
     * The css class that colors the button. Style `unstyled` is only for internal
     * purposes.
     * @default primary
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    style: _metalState.Config.oneOf(['link', 'primary', 'secondary', 'unstyled']).value('primary'),

    /**
     * The title attribute of the element.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    title: _metalState.Config.string(),

    /**
     * The type attribute value of the element.
     * @default button
     * @instance
     * @memberof ClayButton
     * @type {?string}
     */
    type: _metalState.Config.oneOf(['button', 'reset', 'submit']).value('button'),

    /**
     * The value attribute value of the element.
     * @default undefined
     * @instance
     * @memberof ClayButton
     * @type {?(string|undefined)}
     */
    value: _metalState.Config.string()
  };

  (0, _metalWebComponent2.default)('clay-button', ClayButton);

  _metalSoy2.default.register(ClayButton, _ClayButtonSoy2.default);

  exports.ClayButton = ClayButton;
  exports.default = ClayButton;
  //# sourceMappingURL=ClayButton.js.map
});
//# sourceMappingURL=ClayButton.js.map