'use strict';

Liferay.Loader.define("frontend-js-spa-web@2.0.6/liferay/app/App.es", ['module', 'exports', 'require', 'frontend-js-spa-web$senna/lib/app/App', 'frontend-js-metal-web$metal/lib/core', 'frontend-js-metal-web$metal-dom/lib/dom', 'frontend-js-metal-web$metal-promise/lib/promise/Promise', 'frontend-js-web/liferay/toast/commands/OpenToast.es', '../surface/Surface.es', '../util/Utils.es'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	}();

	var _get = function get(object, property, receiver) {
		if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
			var parent = Object.getPrototypeOf(object);if (parent === null) {
				return undefined;
			} else {
				return get(parent, property, receiver);
			}
		} else if ("value" in desc) {
			return desc.value;
		} else {
			var getter = desc.get;if (getter === undefined) {
				return undefined;
			}return getter.call(receiver);
		}
	};

	var _App2 = require("frontend-js-spa-web$senna/lib/app/App");

	var _App3 = _interopRequireDefault(_App2);

	var _core = require("frontend-js-metal-web$metal/lib/core");

	var _core2 = _interopRequireDefault(_core);

	var _dom = require("frontend-js-metal-web$metal-dom/lib/dom");

	var _dom2 = _interopRequireDefault(_dom);

	var _Promise = require("frontend-js-metal-web$metal-promise/lib/promise/Promise");

	var _OpenToast = require('frontend-js-web/liferay/toast/commands/OpenToast.es');

	var _Surface = require('../surface/Surface.es');

	var _Surface2 = _interopRequireDefault(_Surface);

	var _Utils = require('../util/Utils.es');

	var _Utils2 = _interopRequireDefault(_Utils);

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	/**
  * LiferayApp
  *
  * Inherits from `senna/src/app/App` and adds the following Liferay specific
  * behavior to Senna's default App:
  * <ul>
  *   <li> Makes cache expiration time configurable from System Settings</li>
  *   <li>Lets you set valid status codes (Liferay's default valid status codes
  *   are listed in {@link https://docs.liferay.com/portal/7.1/javadocs/portal-kernel/com/liferay/portal/kernel/servlet/ServletResponseConstants.html|ServletResponseConstants.java})
  *   <li>Shows alert notifications when requests take too long or when they fail</li>
  *   <li>Adds a portletBlacklist option that lets you exclude specific portlets
  *   from the SPA lifecycle.</li>
  * </ul>
  */

	var LiferayApp = function (_App) {
		_inherits(LiferayApp, _App);

		/**
   * @inheritDoc
   */

		function LiferayApp() {
			_classCallCheck(this, LiferayApp);

			var _this = _possibleConstructorReturn(this, (LiferayApp.__proto__ || Object.getPrototypeOf(LiferayApp)).call(this));

			_this.portletsBlacklist = {};
			_this.validStatusCodes = [];

			_this.setShouldUseFacade(true);

			_this.timeout = Math.max(Liferay.SPA.requestTimeout, 0) || _Utils2.default.getMaxTimeout();
			_this.timeoutAlert = null;

			var exceptionsSelector = Liferay.SPA.navigationExceptionSelectors;

			_this.setFormSelector('form' + exceptionsSelector);
			_this.setLinkSelector('a' + exceptionsSelector);
			_this.setLoadingCssClass('lfr-spa-loading');

			_this.on('beforeNavigate', _this.onBeforeNavigate);
			_this.on('endNavigate', _this.onEndNavigate);
			_this.on('navigationError', _this.onNavigationError);
			_this.on('startNavigate', _this.onStartNavigate);

			Liferay.on('beforeScreenFlip', _Utils2.default.resetAllPortlets);
			Liferay.on('io:complete', _this.onLiferayIOComplete, _this);

			var body = document.body;

			if (!body.id) {
				body.id = 'senna_surface' + _core2.default.getUid();
			}

			_this.addSurfaces(new _Surface2.default(body.id));

			_dom2.default.append(body, '<div class="lfr-spa-loading-bar"></div>');
			return _this;
		}

		/**
   * Returns the cache expiration time configuration. This value comes from
   * System Settings. The configuration is set upon App initialization
   * @See {@link https://github.com/liferay/liferay-portal/blob/7.1.x/modules/apps/frontend-js/frontend-js-spa-web/src/main/resources/META-INF/resources/init.tmpl|init.tmpl}
   * @return {!Number} The `cacheExpirationTime` value
   */

		_createClass(LiferayApp, [{
			key: 'getCacheExpirationTime',
			value: function getCacheExpirationTime() {
				return Liferay.SPA.cacheExpirationTime;
			}

			/**
    * Returns the valid status codes accepted by Liferay. These values
    * come from {@link https://docs.liferay.com/portal/7.1/javadocs/portal-kernel/com/liferay/portal/kernel/servlet/ServletResponseConstants.html|ServletResponseConstants.java}.
    * @return {!Array} The `validStatusCodes` property
    */

		}, {
			key: 'getValidStatusCodes',
			value: function getValidStatusCodes() {
				return this.validStatusCodes;
			}

			/**
    * Returns whether the cache is enabled. Cache is considered enabled
    * when {@link LiferayApp#getCacheExpirationTime|getCacheExpirationTime} is
    * greater than zero.
    * @return {!Boolean} True if cache is enabled
    */

		}, {
			key: 'isCacheEnabled',
			value: function isCacheEnabled() {
				return this.getCacheExpirationTime() > -1;
			}

			/**
    * Returns whether a given portlet element is in a blacklisted portlet
    * that should not behave like a SPA
    * @param  {!String} element The portlet boundary DOM node
    * @return {!Boolean} True if portlet element is blacklisted
    */

		}, {
			key: 'isInPortletBlacklist',
			value: function isInPortletBlacklist(element) {
				return Object.keys(this.portletsBlacklist).some(function (portletId) {
					var boundaryId = _Utils2.default.getPortletBoundaryId(portletId);

					var portlets = document.querySelectorAll('[id^="' + boundaryId + '"]');

					return Array.prototype.slice.call(portlets).some(function (portlet) {
						return _dom2.default.contains(portlet, element);
					});
				});
			}

			/**
    * Returns whether a given Screen's cache is expired. The expiration timeframe
    * is based on the value returned by {@link LiferayApp#getCacheExpirationTime|getCacheExpirationTime}.
    * @param  {!Screen} screen The Senna Screen
    * @return {!Boolean} True if the cache has expired
    */

		}, {
			key: 'isScreenCacheExpired',
			value: function isScreenCacheExpired(screen) {
				var screenCacheExpired = false;

				if (this.getCacheExpirationTime() !== 0) {
					var lastModifiedInterval = new Date().getTime() - screen.getCacheLastModified();

					screenCacheExpired = lastModifiedInterval > this.getCacheExpirationTime();
				}

				return screenCacheExpired;
			}

			/**
    * A callback for Senna's `beforeNavigate` event. The cache is cleared
    * for all screens when the flag `Liferay.SPA.clearScreensCache`
    * is set or when a form submission is about to occur. This method also
    * exposes the `beforeNavigate` event to the Liferay global object so
    * anyone can listen to it.
    * @param  {!Object} data Data about the event
    * @param  {!Event} event The event object
    */

		}, {
			key: 'onBeforeNavigate',
			value: function onBeforeNavigate(data, event) {
				if (Liferay.SPA.clearScreensCache || data.form) {
					this.clearScreensCache();
				}

				this._clearLayoutData();

				Liferay.fire('beforeNavigate', {
					app: this,
					originalEvent: event,
					path: data.path
				});
			}

			/**
    * A private event handler function, called when the
    * `dataLayoutConfigReady` event is fired on the Liferay object,
    * that initializes `Liferay.Layout`
    * @param  {!Event} event The event object
    */

		}, {
			key: 'onDataLayoutConfigReady_',
			value: function onDataLayoutConfigReady_(event) {
				if (Liferay.Layout) {
					Liferay.Layout.init(Liferay.Data.layoutConfig);
				}
			}

			/**
    * @inheritDoc
    * Overrides Senna's default `onDocClickDelegate_ handler` and
    * halts SPA behavior if the click target is inside a blacklisted
    * portlet
    * @param  {!Event} event The event object
    */

		}, {
			key: 'onDocClickDelegate_',
			value: function onDocClickDelegate_(event) {
				if (this.isInPortletBlacklist(event.delegateTarget)) {
					return;
				}

				_get(LiferayApp.prototype.__proto__ || Object.getPrototypeOf(LiferayApp.prototype), 'onDocClickDelegate_', this).call(this, event);
			}

			/**
    * @inheritDoc
    * Overrides Senna's default `onDocSubmitDelegate_ handler` and
    * halts SPA behavior if the form is inside a blacklisted
    * portlet
    * @param  {!Event} event The event object
    */

		}, {
			key: 'onDocSubmitDelegate_',
			value: function onDocSubmitDelegate_(event) {
				if (this.isInPortletBlacklist(event.delegateTarget)) {
					return;
				}

				_get(LiferayApp.prototype.__proto__ || Object.getPrototypeOf(LiferayApp.prototype), 'onDocSubmitDelegate_', this).call(this, event);
			}

			/**
    * Callback for Senna's `endNavigate` event that exposes it
    * to the Liferay global object
    * @param  {!Event} event The event object
    */

		}, {
			key: 'onEndNavigate',
			value: function onEndNavigate(event) {
				Liferay.fire('endNavigate', {
					app: this,
					error: event.error,
					path: event.path
				});

				if (!this.pendingNavigate) {
					this._clearRequestTimer();
					this._hideTimeoutAlert();
				}

				if (!event.error) {
					this.dataLayoutConfigReadyHandle_ = Liferay.once('dataLayoutConfigReady', this.onDataLayoutConfigReady_);
				}

				AUI().Get._insertCache = {};

				Liferay.DOMTaskRunner.reset();
			}

			/**
    * Callback for Liferay's `io:complete` event that clears screens cache when
    * an async request occurs
    */

		}, {
			key: 'onLiferayIOComplete',
			value: function onLiferayIOComplete() {
				this.clearScreensCache();
			}

			/**
    * Callback for Senna's `navigationError` event that displays
    * an alert message to the user with information about the error
    * @param  {!Event} event The event object
    */

		}, {
			key: 'onNavigationError',
			value: function onNavigationError(event) {
				if (event.error.requestPrematureTermination) {
					window.location.href = event.path;
				} else if (event.error.invalidStatus || event.error.requestError || event.error.timeout) {
					var message = 'There\x20was\x20an\x20unexpected\x20error\x2e\x20Please\x20refresh\x20the\x20current\x20page\x2e';

					if (Liferay.SPA.debugEnabled) {
						console.error(event.error);

						if (event.error.invalidStatus) {
							message = 'the-spa-navigation-request-received-an-invalid-http-status-code';
						}
						if (event.error.requestError) {
							message = 'there-was-an-unexpected-error-in-the-spa-request';
						}
						if (event.error.timeout) {
							message = 'the-spa-request-timed-out';
						}
					}

					Liferay.Data.layoutConfig = this.dataLayoutConfig_;

					this._createNotification({
						message: message,
						title: 'Error',
						type: 'danger'
					});
				}
			}

			/**
    * Callback for Senna's `startNavigate` event that exposes it
    * to the Liferay global object
    * @param  {!Event} event The event object
    */

		}, {
			key: 'onStartNavigate',
			value: function onStartNavigate(event) {
				Liferay.fire('startNavigate', {
					app: this,
					path: event.path
				});

				this._startRequestTimer(event.path);
			}

			/**
    * Sets the `portletsBlacklist` property
    * @param  {!Object} portletsBlacklist
    */

		}, {
			key: 'setPortletsBlacklist',
			value: function setPortletsBlacklist(portletsBlacklist) {
				this.portletsBlacklist = portletsBlacklist;
			}

			/**
    * Sets the `validStatusCodes` property
    * @param  {!Array} validStatusCodes
    */

		}, {
			key: 'setValidStatusCodes',
			value: function setValidStatusCodes(validStatusCodes) {
				this.validStatusCodes = validStatusCodes;
			}

			/**
    * Clears and detaches event handlers for Liferay's `dataLayoutConfigReady`
    * event
    */

		}, {
			key: '_clearLayoutData',
			value: function _clearLayoutData() {
				this.dataLayoutConfig_ = Liferay.Data.layoutConfig;

				Liferay.Data.layoutConfig = null;

				if (this.dataLayoutConfigReadyHandle_) {
					this.dataLayoutConfigReadyHandle_.detach();
					this.dataLayoutConfigReadyHandle_ = null;
				}
			}

			/**
    * Clears the timer that notifies the user when the SPA request
    * takes longer than the thresshold time configured in the
    * `Liferay.SPA.userNotification.timeout` System Settings property
    */

		}, {
			key: '_clearRequestTimer',
			value: function _clearRequestTimer() {
				if (this.requestTimer) {
					clearTimeout(this.requestTimer);
				}
			}

			/**
    * Creates a user notification
    * @param  {!Object} configuration object that's passed to `Liferay.Notification`
    * @return {!CancellablePromise} A promise that renders a notification when
    * resolved
    */

		}, {
			key: '_createNotification',
			value: function _createNotification(config) {
				return new _Promise.CancellablePromise(function (resolve) {
					resolve((0, _OpenToast.openToast)(Object.assign({
						type: 'warning'
					}, config)));
				});
			}

			/**
    * Hides the request timeout alert
    */

		}, {
			key: '_hideTimeoutAlert',
			value: function _hideTimeoutAlert() {
				if (this.timeoutAlert) {
					this.timeoutAlert.dispose();
				}
			}

			/**
    * Starts the timer that shows the user a notification when the SPA
    * request takes longer than the threshold time configured in the
    * `Liferay.SPA.userNotification.timeout` System Settings property
    * @param  {!String} path The path that may time out
    */

		}, {
			key: '_startRequestTimer',
			value: function _startRequestTimer(path) {
				var _this2 = this;

				this._clearRequestTimer();

				if (Liferay.SPA.userNotification.timeout > 0) {
					this.requestTimer = setTimeout(function () {
						Liferay.fire('spaRequestTimeout', {
							path: path
						});

						_this2._hideTimeoutAlert();

						_this2._createNotification({
							message: Liferay.SPA.userNotification.message,
							title: Liferay.SPA.userNotification.title,
							type: 'warning'
						}).then(function (alert) {
							_this2.timeoutAlert = alert;
						});
					}, Liferay.SPA.userNotification.timeout);
				}
			}

			/**
    * @inheritDoc
    */

		}, {
			key: 'updateHistory_',
			value: function updateHistory_(title, path, state, opt_replaceHistory) {
				if (state && state.redirectPath) {
					state.path = state.redirectPath;
				}

				_get(LiferayApp.prototype.__proto__ || Object.getPrototypeOf(LiferayApp.prototype), 'updateHistory_', this).call(this, title, path, state, opt_replaceHistory);
			}
		}]);

		return LiferayApp;
	}(_App3.default);

	exports.default = LiferayApp;
});
//# sourceMappingURL=App.es.js.map