'use strict';

Liferay.Loader.define('frontend-js-web@2.0.6/liferay/toast/commands/OpenToast.es', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-dom', 'frontend-taglib-clay$clay-alert'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.openToast = undefined;

	var _metalDom = require('frontend-js-metal-web$metal-dom');

	var _metalDom2 = _interopRequireDefault(_metalDom);

	var _clayAlert = require('frontend-taglib-clay$clay-alert');

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { default: obj };
	}

	/**
  * Function that implements the Toast pattern, which allows to present feedback
  * to user actions as a toast message in the lower left corner of the page
  *
  * @param {string} message The message to show in the toast notification
  * @param {string} title The title associated with the message
  * @param {string} type The type of notification to show. It can be one of the
  * following: 'danger', 'info', 'success', 'warning'
  * @return {ClayToast} The Alert toast created
  * @review
  */

	function openToast(_ref) {
		var _ref$message = _ref.message,
		    message = _ref$message === undefined ? '' : _ref$message,
		    _ref$title = _ref.title,
		    title = _ref$title === undefined ? 'Success' : _ref$title,
		    _ref$type = _ref.type,
		    type = _ref$type === undefined ? 'success' : _ref$type;

		var alertContainer = document.getElementById('alertContainer');

		if (!alertContainer) {
			alertContainer = document.createElement('div');
			alertContainer.id = 'alertContainer';

			_metalDom2.default.addClasses(alertContainer, 'alert-notifications alert-notifications-fixed');
			_metalDom2.default.enterDocument(alertContainer);
		} else {
			_metalDom2.default.removeChildren(alertContainer);
		}

		var clayToast = new _clayAlert.ClayToast({
			autoClose: true,
			destroyOnHide: true,
			events: {
				'disposed': function disposed(event) {
					if (!alertContainer.hasChildNodes()) {
						_metalDom2.default.exitDocument(alertContainer);
					}
				}
			},
			message: message,
			spritemap: themeDisplay.getPathThemeImages() + '/lexicon/icons.svg',
			style: type,
			title: title
		}, alertContainer);

		_metalDom2.default.removeClasses(clayToast.element, 'show');

		requestAnimationFrame(function () {
			_metalDom2.default.addClasses(clayToast.element, 'show');
		});

		return clayToast;
	}

	exports.openToast = openToast;
	exports.default = openToast;
});
//# sourceMappingURL=OpenToast.es.js.map