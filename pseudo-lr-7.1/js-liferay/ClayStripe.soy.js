'use strict';

Liferay.Loader.define('frontend-taglib-clay$clay-alert@2.1.12/lib/ClayStripe.soy', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.templates = exports.ClayStripe = undefined;

  var _metalComponent = require('frontend-js-metal-web$metal-component');

  var _metalComponent2 = _interopRequireDefault(_metalComponent);

  var _metalSoy = require('frontend-js-metal-web$metal-soy');

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  } /* jshint ignore:start */

  var templates;
  goog.loadModule(function (exports) {
    var soy = goog.require('soy');
    var soydata = goog.require('soydata');
    // This file was automatically generated from ClayStripe.soy.
    // Please don't edit this file by hand.

    /**
     * @fileoverview Templates in namespace ClayStripe.
     * @public
     */

    goog.module('ClayStripe.incrementaldom');

    goog.require('goog.soy.data.SanitizedContent');
    goog.require('soy.asserts');
    var soyIdom = goog.require('soy.idom');

    var $templateAlias1 = _metalSoy2.default.getTemplate('ClayAlertBase.incrementaldom', 'render');

    /**
     * @param {$render.Params} opt_data
     * @param {Object<string, *>=} opt_ijData
     * @param {Object<string, *>=} opt_ijData_deprecated
     * @return {void}
     * @suppress {checkTypes|uselessCode}
     */
    var $render = function $render(opt_data, opt_ijData, opt_ijData_deprecated) {
      opt_ijData = opt_ijData_deprecated || opt_ijData;
      /** @type {!goog.soy.data.SanitizedContent|function()|string} */
      var message = soy.asserts.assertType(goog.isFunction(opt_data.message) || goog.isString(opt_data.message) || opt_data.message instanceof goog.soy.data.SanitizedContent, 'message', opt_data.message, '!goog.soy.data.SanitizedContent|function()|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var spritemap = soy.asserts.assertType(goog.isString(opt_data.spritemap) || opt_data.spritemap instanceof goog.soy.data.SanitizedContent, 'spritemap', opt_data.spritemap, '!goog.soy.data.SanitizedContent|string');
      /** @type {!goog.soy.data.SanitizedContent|string} */
      var title = soy.asserts.assertType(goog.isString(opt_data.title) || opt_data.title instanceof goog.soy.data.SanitizedContent, 'title', opt_data.title, '!goog.soy.data.SanitizedContent|string');
      /** @type {*|null|undefined} */
      var _handleHide = opt_data._handleHide;
      /** @type {boolean|null|number|undefined} */
      var autoClose = soy.asserts.assertType(opt_data.autoClose == null || goog.isBoolean(opt_data.autoClose) || opt_data.autoClose === 1 || opt_data.autoClose === 0 || goog.isNumber(opt_data.autoClose), 'autoClose', opt_data.autoClose, 'boolean|null|number|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var elementClasses = soy.asserts.assertType(opt_data.elementClasses == null || goog.isString(opt_data.elementClasses) || opt_data.elementClasses instanceof goog.soy.data.SanitizedContent, 'elementClasses', opt_data.elementClasses, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var id = soy.asserts.assertType(opt_data.id == null || goog.isString(opt_data.id) || opt_data.id instanceof goog.soy.data.SanitizedContent, 'id', opt_data.id, '!goog.soy.data.SanitizedContent|null|string|undefined');
      /** @type {!goog.soy.data.SanitizedContent|null|string|undefined} */
      var style = soy.asserts.assertType(opt_data.style == null || goog.isString(opt_data.style) || opt_data.style instanceof goog.soy.data.SanitizedContent, 'style', opt_data.style, '!goog.soy.data.SanitizedContent|null|string|undefined');
      var timeToDisappear__soy206 = autoClose == true ? 5 : autoClose == false ? null : autoClose;
      $templateAlias1({ autoClose: timeToDisappear__soy206, closeable: true, elementClasses: elementClasses, events: { hide: _handleHide }, id: id, message: message, ref: 'alertBase', spritemap: spritemap, style: style, title: title, type: 'stripe' }, opt_ijData);
    };
    exports.render = $render;
    /**
     * @typedef {{
     *  message: (!goog.soy.data.SanitizedContent|function()|string),
     *  spritemap: (!goog.soy.data.SanitizedContent|string),
     *  title: (!goog.soy.data.SanitizedContent|string),
     *  _handleHide: (*|null|undefined),
     *  autoClose: (boolean|null|number|undefined),
     *  elementClasses: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  id: (!goog.soy.data.SanitizedContent|null|string|undefined),
     *  style: (!goog.soy.data.SanitizedContent|null|string|undefined),
     * }}
     */
    $render.Params;
    if (goog.DEBUG) {
      $render.soyTemplateName = 'ClayStripe.render';
    }

    exports.render.params = ["message", "spritemap", "title", "_handleHide", "autoClose", "elementClasses", "id", "style"];
    exports.render.types = { "message": "html|string", "spritemap": "string", "title": "string", "_handleHide": "any", "autoClose": "bool|int", "elementClasses": "string", "id": "string", "style": "string" };
    exports.templates = templates = exports;
    return exports;
  });

  var ClayStripe = function (_Component) {
    _inherits(ClayStripe, _Component);

    function ClayStripe() {
      _classCallCheck(this, ClayStripe);

      return _possibleConstructorReturn(this, (ClayStripe.__proto__ || Object.getPrototypeOf(ClayStripe)).apply(this, arguments));
    }

    return ClayStripe;
  }(_metalComponent2.default);

  _metalSoy2.default.register(ClayStripe, templates);
  exports.ClayStripe = ClayStripe;
  exports.templates = templates;
  exports.default = templates;
  /* jshint ignore:end */
  //# sourceMappingURL=ClayStripe.soy.js.map
});
//# sourceMappingURL=ClayStripe.soy.js.map