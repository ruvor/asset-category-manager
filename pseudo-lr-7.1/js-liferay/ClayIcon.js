'use strict';

Liferay.Loader.define('frontend-taglib-clay$clay-icon@2.1.12/lib/ClayIcon', ['module', 'exports', 'require', 'frontend-js-metal-web$metal-component', 'frontend-js-metal-web$metal-web-component', 'frontend-js-metal-web$metal-soy', 'frontend-js-metal-web$metal-state', './ClayIcon.soy'], function (module, exports, require) {
  var define = undefined;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ClayIcon = undefined;

  var _metalComponent = require('frontend-js-metal-web$metal-component');

  var _metalComponent2 = _interopRequireDefault(_metalComponent);

  var _metalWebComponent = require('frontend-js-metal-web$metal-web-component');

  var _metalWebComponent2 = _interopRequireDefault(_metalWebComponent);

  var _metalSoy = require('frontend-js-metal-web$metal-soy');

  var _metalSoy2 = _interopRequireDefault(_metalSoy);

  var _metalState = require('frontend-js-metal-web$metal-state');

  var _ClayIconSoy = require('./ClayIcon.soy');

  var _ClayIconSoy2 = _interopRequireDefault(_ClayIconSoy);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  /**
   * Implementation of the Metal Clay Icon.
   * @extends Component
   */
  var ClayIcon = function (_Component) {
    _inherits(ClayIcon, _Component);

    function ClayIcon() {
      _classCallCheck(this, ClayIcon);

      return _possibleConstructorReturn(this, (ClayIcon.__proto__ || Object.getPrototypeOf(ClayIcon)).apply(this, arguments));
    }

    return ClayIcon;
  }(_metalComponent2.default);

  /**
   * State definition.
   * @static
   * @type {!Object}
   */

  ClayIcon.STATE = {
    /**
     * CSS classes to be applied to the element.
     * @default undefined
     * @instance
     * @memberof ClayIcon
     * @type {?(string|undefined)}
     */
    elementClasses: _metalState.Config.string(),

    /**
     * Flag to indicate if the svg is focusable or not
     * @default false
     * @instance
     * @memberof ClayIcon
     * @type {?bool}
     */
    focusable: _metalState.Config.bool().value(false),

    /**
     * Id to be applied to the element.
     * @default undefined
     * @instance
     * @memberof ClayIcon
     * @type {?(string|undefined)}
     */
    id: _metalState.Config.string(),

    /**
     * The path to the SVG spritemap file containing the icons.
     * @default undefined
     * @instance
     * @memberof ClayIcon
     * @type {!string}
     */
    spritemap: _metalState.Config.string().required(),

    /**
     * The name of the Clay SVG Icon e.g. `plus`.
     * @default undefined
     * @instance
     * @memberof ClayIcon
     * @type {!string}
     */
    symbol: _metalState.Config.string().required(),

    /**
     * The title of the Clay SVG Icon.
     * @default undefined
     * @instance
     * @memberof ClayIcon
     * @type {?(string|undefined)}
     */
    title: _metalState.Config.string()
  };

  (0, _metalWebComponent2.default)('clay-icon', ClayIcon);

  _metalSoy2.default.register(ClayIcon, _ClayIconSoy2.default);

  exports.ClayIcon = ClayIcon;
  exports.default = ClayIcon;
  //# sourceMappingURL=ClayIcon.js.map
});
//# sourceMappingURL=ClayIcon.js.map