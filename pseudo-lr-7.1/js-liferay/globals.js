'use strict';

Liferay.Loader.define('frontend-js-spa-web$senna@2.5.6/lib/globals/globals', ['module', 'exports', 'require'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var globals = globals || {};

	if (typeof window !== 'undefined') {
		globals.window = window;
	}

	if (typeof document !== 'undefined') {
		globals.document = document;
	}

	exports.default = globals;
});
//# sourceMappingURL=globals.js.map