'use strict';

/**
  * Debounces function execution.
  * @param {!function()} fn
  * @param {number} delay
  * @return {!function()}
  */

Liferay.Loader.define("frontend-js-metal-web$metal-debounce@2.0.0/lib/debounce", ['module', 'exports', 'require'], function (module, exports, require) {
	var define = undefined;
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	function debounce(fn, delay) {
		return function debounced() {
			var args = arguments;
			cancelDebounce(debounced);
			debounced.id = setTimeout(function () {
				fn.apply(null, args);
			}, delay);
		};
	}

	/**
  * Cancels the scheduled debounced function.
  */
	function cancelDebounce(debounced) {
		clearTimeout(debounced.id);
	}

	exports.default = debounce;
	exports.cancelDebounce = cancelDebounce;
	exports.debounce = debounce;
});
//# sourceMappingURL=debounce.js.map