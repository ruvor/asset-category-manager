# Субпроект по "портированию" портлета Asset Category Manager на седьмой Лайфрей

По сути, надо сделать новый портлет для редактирования категорий на Ангуляре.

После почти года простоя вернулись к этой поделке. Но теперь код портлета и сервисов будет жить не в своей отдельной репе, а в репе "коробки", для которой управлятор предназначен: [БЗ InKnowledge](https://bitbucket.org/emdev-limited/inknowledge).

## О структуре

В папке `pseudo-lr-7.1` лежит заготовка, выдранная из лайфрея специально для создания интерфейсов без участия самого лайфрея.

О такой хитромудрой системе разработки см. репозиторий `liferay-base`, там папку `liferay-7-ui-boilerplate`.

docs/ЧТЗ\_этап2\_МиграцияБЗКЦ\_v04.docx — филе из проекта, в рамках которого портлет первоначально делался, вроде бы

docs/Менеджер категорий - интерфейс v2.odt — а это уже непосредственно по новому портлету

## Развёртывание окружения

    npm install

Соответствующий тимный репозиторий расположить в папке ../../Team/inknowledge (а этот репозиторий рекомендуется расположить в папке Work/My/asset-category-manager).
Затем наделать джункшонов. К сожалению, наделать симлинков на либы в папке портлета не получится, потому что градл отказывается собирать с ними. Да и в репу кладутся сами симлинки.

    mklink /j pseudo-lr-7.1\asset-category-manager-app ..\..\Team\inknowledge\inknowledge-team\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\asset-category-manager-app
    mklink /j pseudo-lr-7.1\node_modules node_modules

    mklink /j ..\..\Team\inknowledge\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\libs\angular node_modules\angular
    mklink /j ..\..\Team\inknowledge\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\libs\angular-animate node_modules\angular-animate
    mklink /j ..\..\Team\inknowledge\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\libs\angular-translate node_modules\angular-translate
    mklink /j ..\..\Team\inknowledge\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\libs\angular-ui-tree node_modules\angular-ui-tree
    mklink /j ..\..\Team\inknowledge\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\libs\angular-simple-configurator node_modules\angular-simple-configurator
    mklink /j ..\..\Team\inknowledge\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\libs\liferay-7-angularjs-ui-tools node_modules\liferay-7-angularjs-ui-tools
    mklink /j ..\..\Team\inknowledge\modules\asset-category-manager\asset-category-manager-web\src\main\resources\META-INF\resources\libs\angular-simple-autocompl node_modules\angular-simple-autocompl

В `.git/info` тимной репы положить файл `exclude`. Этот исключительный файл нужен, чтобы не отсвечивали многочисленные файлы, которые не нужны в тимной репе, но которые вынужденно там лежат из-за джункшонов на библиотечные папки в node_modules.

## Запуск локального сервера

`gulp serve`

(Или `serve.cmd` — файл с этой командой.)

Также имеется удобная пусковая конфигурация для Visual Studio Code. Можно запускать кнопкой F5.

## Про дерево

Как я понял по [документации на Angular UI Tree](https://github.com/angular-ui-tree/angular-ui-tree#angular-ui-tree), сворачивать/разворачивать всё нужно примерно вот так:

    catsTreeScope.$broadcast('angular-ui-tree:expand-all');
    catsTreeScope.$broadcast('angular-ui-tree:collapse-all');

Но в нашем случае не обеспечивается сохранение признака развёрнутости в объектах категорий, а это необходимо для работы "приложения".

А сворачивать/разворачивать отдельные узлы надо так:

    categoryScope.expand();
    categoryScope.collapse();

Но из-за привязки к модели признака коллапснутости (типа атрибута `collapsed="category.collapsed"`) это не нужно, достаточно сменить значение `category.collapsed`. Не знаю, заюзал ли я таким образом случайную недокументированную возможность или автор на это и рассчитывал.

В документации [написано](https://github.com/angular-ui-tree/angular-ui-tree#droppedevent), что коллбэк `dropped` вызывается только при изменении положения узла:

> If a node moves it's position after dropped, the nodeDropped callback will be called.

Но опытным путём выяснено, что вызывается он всегда. А чтобы он не вызывался, коллбэк `beforeDrop` должен вернуть явное `false`.

## TODO

Отправлять при поиске по букве категорию, дочерних из которой не предлагать (потому что её удаляют, например).
