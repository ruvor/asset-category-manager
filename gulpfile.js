const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync");
// const transhproxy = require("transhproxy");

const staticServerInstance = browserSync.create();
const proxyServerInstance = browserSync.create();

gulp.task("sass", function () {
    return gulp.src(["pseudo-lr-7.1/asset-category-manager-app/asset-category-manager.scss"])
            .pipe(sass().on("error", sass.logError))
            .pipe(gulp.dest("pseudo-lr-7.1/css-junk/"))
            .pipe(staticServerInstance.stream());
});

gulp.task("serve", ["sass"], function () {
    staticServerInstance.init({
        server: "pseudo-lr-7.1",
        port: 3000,
        ui: {
            port: 3001
        }
    });
    proxyServerInstance.init({
        proxy: {
            target: "https://liferay71dev.emdev.ru",
            proxyRes: [
                function (proxyRes, req, res) {
                    res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
                    if (req.method === 'OPTIONS' && proxyRes.statusCode === 401) {
                        // При preflight-запросе бровзерсинк добросовестно пересылает его лайфрею, а тот отказывает,
                        // но мне для отладки нужно, чтобы вернулось 200
                        proxyRes.statusCode = 200; // Такое присвоение помогает
                    }
                }
            ]
        },
        port: 4000,
        ui: {
            port: 4001
        },
        cors: true
    });
    // transhproxy("127.0.0.1:4000", "192.168.1.39:8080", "cookies.txt");
    gulp.watch(["pseudo-lr-7.1/asset-category-manager-app/*.scss"], ["sass"]);
});
