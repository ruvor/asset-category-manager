// Это большие информативные комментарии, которые не потерпел в коде Сонаркуб. Пусть полежат отдельно.

// Варианты передачи категории:
// 1) категория не имеет подкатегорий
//    {
//        id: ...
//        hasChildren: false,
//    }
// 2) категория имеет подкатегории, которые можно запросить
//    {
//        id: ...
//        hasChildren: true,
//    }
// 3) категория имеет подкатегории и сервер отдаёт их сразу же
//    {
//        id: ...
//        hasChildren: ... не имеет значения, может не быть совсем
//        categories: [
//            {
//                id: ...
//            },
//        ],
//    }

// Вариант состояния дерева в процессе работы:
/* this.vocabularies = [
    {
        id: "0",
        name: "Словарь первый",
        collapsed: true,
        categories: [
            {
                id: "0",
                name: "Категория первая",
                collapsed: true,
                hasChildren: false,
                categories: [],
            },
            {
                id: "1",
                name: "Категория вторая",
                collapsed: false,
                categories: [
                    {
                        id: "2",
                        name: "Категория третья",
                        collapsed: true,
                        archived: true,
                        hasChildren: true,
                        categories: [],
                    },
                    {
                        id: "3",
                        name: "Категория четвёртая",
                        collapsed: true,
                        categories: [],
                    },
                    {
                        id: "4",
                        name: "Категория пятая",
                        collapsed: true,
                        hasChildren: true,
                        categories: [],
                    },
                ],
            },
            {
                id: "5",
                name: "Категория шестая",
                collapsed: true,
                categories: [
                    {
                        id: "6",
                        name: "Категория седьмая",
                        collapsed: true,
                        hasChildren: false,
                        categories: [],
                    },
                ],
            },
        ],
    },
    {
        id: "1",
        name: "Словарь второй",
        collapsed: false,
        categories: [
            {
                id: "7",
                name: "Категория восьмая",
                collapsed: true,
                categories: [],
            },
            {
                id: "8",
                name: "Категория девятая",
                collapsed: true,
                categories: [
                    {
                        id: "9",
                        name: "Категория десятая",
                        collapsed: true,
                        categories: [
                            {
                                id: "10",
                                name: "Категория одиннадцатая",
                                collapsed: true,
                                hasChildren: false,
                                categories: [],
                            },
                        ],
                    },
                ],
            },
        ],
    },
    {
        id: "2",
        name: "Словарь третий",
        collapsed: true,
        categories: [],
    },
]; */